<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldUrlfilesToJawabans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jawabans', function (Blueprint $table) {
            $table->string('urlfile_koreksi')->after('file_koreksi')->nullable();
            $table->string('urlfile_konfirmasi')->after('file_konfirmasi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jawabans', function (Blueprint $table) {
            $table->dropIfExists('urlfile_koreksi');
            $table->dropIfExists('urlfile_konfirmasi');
        });
    }
}
