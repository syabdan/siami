<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToPeriodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('periodes', function (Blueprint $table) {
            $table->renameColumn('tanggal_awal_isi_prodi', 'tanggal_awal');
            $table->renameColumn('tanggal_akhir_isi_prodi', 'tanggal_akhir');
            $table->date('batas_isi_prodi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('periodes', function (Blueprint $table) {
            $table->dropIfExists('batas_isi_prodi');

        });
    }
}
