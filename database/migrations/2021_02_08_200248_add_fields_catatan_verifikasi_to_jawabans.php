<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsCatatanVerifikasiToJawabans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jawabans', function (Blueprint $table) {
            $table->string('file_abr')->after('file_konfirmasi')->nullable();
            $table->string('catatan_verifikasi')->after('catatan_pencegahan')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jawabans', function (Blueprint $table) {
            $table->dropIfExists('file_abr');
            $table->dropIfExists('catatan_verifikasi');

        });
    }
}
