<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJawabansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jawabans', function (Blueprint $table) {
            $table->id();
            $table->integer('pertanyaan_id');
            $table->integer('user_id');
            $table->integer('auditor_id')->nullable();
            $table->integer('unit_id');
            $table->integer('periode_id');
            $table->text('jawaban');
            $table->string('file_jawaban')->nullable();
            $table->string('file_koreksi')->nullable();
            $table->string('nilai')->nullable();
            $table->string('nilai_auditor')->nullable();
            $table->string('temuan')->nullable();
            $table->Text('catatan_penyebab')->nullable();
            $table->Text('catatan_koreksi')->nullable();
            $table->Text('catatan_pencegahan')->nullable();
            $table->date('tgl_tindakan_koreksi')->nullable();
            $table->date('tgl_ttd_auditor')->nullable();
            $table->date('tgl_verifikasi')->nullable();
            $table->string('aspect')->nullable();
            $table->string('impact')->nullable();
            $table->string('likelihood', 15)->nullable();
            $table->string('severity', 15)->nullable();
            $table->string('assesment', 15)->nullable();
            $table->string('mitigation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jawabans');
    }
}
