<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class NilaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nilais')->truncate();

        DB::table('nilais')->insert([
            ['value' => 0],
            ['value' => 1],
            ['value' => 2],
            ['value' => 3],
            ['value' => 4],
        ]);
    }
}
