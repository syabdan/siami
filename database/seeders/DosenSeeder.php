<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Dosen;
use DB;

class DosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = api_dosen();
        $sumber = $data;
        $list = json_decode($sumber, true);
        // print_r($list);

		for($a=0; $a < count($list); $a++){
			Dosen::updateOrCreate(['nama_dosen' => $list[$a]['nama']],[
                'nidn'              => $list[$a]['nidn'],
                'nama_dosen'        => $list[$a]['nama'],
                'gelar_depan'       => $list[$a]['gelar_depan'],
                'gelar_belakang'    => $list[$a]['gelar_belakang'],
                'prodi'             => $list[$a]['nama_prodi'],
                'fakultas'          => $list[$a]['nama_fakultas']
            ]);
		}
    }
}
