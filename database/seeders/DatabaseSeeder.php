<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([UserSeeder::class]);
        $this->call([MenuSeeder::class]);
        $this->call([PermissionSeeder::class]);
        $this->call([DosenSeeder::class]);
        $this->call([UnitSeeder::class]);
        $this->call([IconSeeder::class]);
        $this->call([PeriodeSeeder::class]);
        $this->call([NilaiSeeder::class]);
        $this->call([StandarSeeder::class]);
    }
}
