<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Unit;
use DB;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('units')->truncate();

		// Insert fakultas
		$api_fakultas = api_fakultas();
        $list = json_decode($api_fakultas, true);

		for($a=0; $a < count($list); $a++){

			Unit::updateOrCreate([
                'id'               			=> $list[$a]['kode_fakultas'],
                'nama_unit'         		=> $list[$a]['nama_fakultas'],
                'jenjang_pendidikan'      	=> 'Fakultas'
            ]);
		}

		// Insert Prodi
		$api_prodi = api_prodi();
        $list = json_decode($api_prodi, true);

		for($a=0; $a < count($list); $a++){

			Unit::updateOrCreate([
                'id'               			=> $list[$a]['kode_prodi'],
                'nama_unit'         		=> $list[$a]['nama_prodi'],
                'jenjang_pendidikan'      	=> $list[$a]['jenjang_pendidikan'],
                'parent_id'   	 			=> $list[$a]['kode_fakultas']
            ]);
		}



    }
}
