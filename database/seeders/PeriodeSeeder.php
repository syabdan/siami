<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PeriodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('periodes')->truncate();

        DB::table('periodes')->insert([
            ['nama_periode' => 'periode 2021/2022', 'tanggal_awal' => '2021-01-01', 'tanggal_akhir' => '2022-01-01', 'status' => '1'],
        ]);
    }
}
