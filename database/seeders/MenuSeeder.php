<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->truncate();

        // 1
        $menus = new Menu();
        $menus->posisi = 1;
        $menus->nama = "Dashboard";
        $menus->link = "dashboard";
        $menus->icon = "ti-home";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = NULL;
        $menus->label = 'Main';
        $menus->save();

        // 2
        $menus = new Menu();
        $menus->posisi = 2;
        $menus->nama = "Setting";
        $menus->link = "#";
        $menus->icon = "ti-settings";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = NULL;
        $menus->label = 'Master';
        $menus->save();

        // 3
        $menus = new Menu();
        $menus->posisi = 3;
        $menus->nama = "Data User";
        $menus->link = "users";
        $menus->icon = "ti-user";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2; // setting
        $menus->label = NULL;
        $menus->save();

        // 3
        $menus = new Menu();
        $menus->posisi = 3;
        $menus->nama = "Data Instrumen";
        $menus->link = "instrumen";
        $menus->icon = "ti-files";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2; // setting
        $menus->label = NULL;
        $menus->save();

        // 3
        $menus = new Menu();
        $menus->posisi = 3;
        $menus->nama = "Data Periode";
        $menus->link = "periode";
        $menus->icon = "ti-files";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2; // setting
        $menus->label = NULL;
        $menus->save();

        // 4
        $menus = new Menu();
        $menus->posisi = 3;
        $menus->nama = "Data Menu";
        $menus->link = "menu";
        $menus->icon = "ti-files";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2; // setting
        $menus->label = NULL;
        $menus->save();

        // 5
        $menus = new Menu();
        $menus->posisi = 3;
        $menus->nama = "Data Permission";
        $menus->link = "permission";
        $menus->icon = "ti-files";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2; // setting
        $menus->label = NULL;
        $menus->save();

        // 6
        $menus = new Menu();
        $menus->posisi = 3;
        $menus->nama = "Extra JS";
        $menus->link = "adan";
        $menus->icon = "ti-files";
        $menus->status = 1;
        $menus->tampil = 0;
        $menus->parent_menu_id = NULL;
        $menus->label = NULL;
        $menus->save();
    }
}
