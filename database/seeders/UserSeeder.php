<?php

namespace Database\Seeders;
use App\Models\User;
use DB;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        $newuser = new User();
        $newuser->nama_user = "Admin LPM";
        $newuser->email = "lpm@uir.ac.id";
        $newuser->password = bcrypt('secret');
        $newuser->permission_id = 1;
        $newuser->tanggal_awal = NULL;
        $newuser->tanggal_akhir = NULL;
        $newuser->status = 1;
        $newuser->created_by = 1;
        $newuser->save();

        $newuser = new User();
        $newuser->nama_user = "Pimpinan UIR";
        $newuser->email = "pimpinan@uir.ac.id";
        $newuser->password = bcrypt('secret');
        $newuser->permission_id = 2;
        $newuser->tanggal_awal = NULL;
        $newuser->tanggal_akhir = NULL;
        $newuser->status = 1;
        $newuser->created_by = 1;
        $newuser->save();
    }
}
