<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class StandarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('standars')->truncate();

        DB::table('standars')->insert([
            ['nama_standar' => 'Standar 1', 'keterangan' => 'Standar Mahasiswa'],
            ['nama_standar' => 'Standar 2', 'keterangan' => 'Standar SDM'],
            ['nama_standar' => 'Standar 3', 'keterangan' => 'Standar Pendidikan'],
            ['nama_standar' => 'Standar 4', 'keterangan' => 'Standar Penelitian'],
            ['nama_standar' => 'Standar 5', 'keterangan' => 'Standar Sarana dan Prasarana'],
            ['nama_standar' => 'Standar 6', 'keterangan' => 'Standar Kelulusan'],
        ]);
    }
}
