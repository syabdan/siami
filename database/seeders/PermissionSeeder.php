<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\models\Permission;
use DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('permissions')->truncate();

        $Permissions = new Permission();
        $Permissions->permission_name = "LPM";
        $Permissions->view_status = false;
        $Permissions->add_status = true;
        $Permissions->edit_status = true;
        $Permissions->delete_status = true;

        $Permissions->data_menus = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23";
        $Permissions->status = true;
        $Permissions->created_by = 1;
        $Permissions->save();


        $Permissions = new Permission();
        $Permissions->permission_name = "Pimpinan";
        $Permissions->view_status = false;
        $Permissions->add_status = true;
        $Permissions->edit_status = true;
        $Permissions->delete_status = true;

        $Permissions->data_menus = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16";
        $Permissions->status = true;
        $Permissions->created_by = 1;
        $Permissions->save();

        $Permissions = new Permission();
        $Permissions->permission_name = "Auditor";
        $Permissions->view_status = false;
        $Permissions->add_status = true;
        $Permissions->edit_status = true;
        $Permissions->delete_status = true;

        $Permissions->data_menus = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16";
        $Permissions->status = true;
        $Permissions->created_by = 1;
        $Permissions->save();

        $Permissions = new Permission();
        $Permissions->permission_name = "Prodi";
        $Permissions->view_status = false;
        $Permissions->add_status = true;
        $Permissions->edit_status = true;
        $Permissions->delete_status = true;

        $Permissions->data_menus = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16";
        $Permissions->status = true;
        $Permissions->created_by = 1;
        $Permissions->save();

    }
}
