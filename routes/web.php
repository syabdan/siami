<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\AksesMenu;
use App\Http\Middleware\Authenticate;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\JsController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\PeriodeController;
use App\Http\Controllers\StandarController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\JawabanController;
use App\Http\Controllers\NcrController;
use App\Http\Controllers\AbrController;
use App\Http\Controllers\PenugasanAuditorController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\BeritaAcaraController;
use App\Http\Controllers\UnggahController;

// Frontend
Route::get('/', [FrontendController::class, 'index'])->name('frontend');
Route::get('/frontend/lihat/{id_unit}', [FrontendController::class, 'lihat']);
// #end Frontend

Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::get('logout', [LoginController::class, 'logout'])->name('logout');

Route::get('adan/{folder}/{file}', [JsController::class, 'backend']);
Route::get('adan/{folder}/{id}/{file}', [JsController::class, 'backendWithId']);

Auth::routes();
Route::get('edit_profile/{id}', [UsersController::class, 'editProfile'])->name('editProfile');
Route::put('update_profile/{id}', [UsersController::class, 'update_profile'])->name('update_profile');
Route::put('update_unit_auditor/{id}', [UsersController::class, 'update_unit_auditor'])->name('update_unit_auditor');
Route::get('reset_auditee', [UsersController::class, 'reset_auditee'])->name('reset_auditee');
Route::middleware([Authenticate::class, AksesMenu::class])->group(function () {
    #dashboard
	Route::group([ 'prefix' => 'dashboard', 'as' => 'dashboard.'], function () {
        Route::get('/', [HomeController::class, 'index']);
        Route::any('data_monitoring/{periode?}', [HomeController::class, 'data_monitoring'])->name('data_monitoring');
        Route::any('data_monitoring_prodi', [HomeController::class, 'data_monitoring_prodi'])->name('data_monitoring_prodi');
        Route::post('cetak', [BeritaAcaraController::class, 'cetak'])->name('berita_acara');

	});

    #users
	Route::group([ 'prefix' => 'users', 'as' => 'users.'], function () {
		Route::get('hapus/{id}', [UsersController::class, 'hapus'])->name('hapus');
		Route::any('data', [UsersController::class, 'data'])->name('data');
	});
    Route::resource('users', UsersController::class);

    #permission
	Route::group([ 'prefix' => 'permission', 'as' => 'permission.'], function () {
		Route::get('hapus/{id}', [PermissionController::class, 'hapus'])->name('hapus');
		Route::any('data', [PermissionController::class, 'data'])->name('data');
	});
    Route::resource('permission', PermissionController::class);

    #menu
	Route::group([ 'prefix' => 'menu', 'as' => 'menu.'], function () {
		Route::get('hapus/{id}', [MenuController::class, 'hapus'])->name('hapus');
		Route::any('data', [MenuController::class, 'data'])->name('data');
	});
    Route::resource('menu', MenuController::class);

    #standar
	Route::group([ 'prefix' => 'standar', 'as' => 'standar.'], function () {
		Route::get('hapus/{id}', [StandarController::class, 'hapus'])->name('hapus');
		Route::any('data', [StandarController::class, 'data'])->name('data');
	});
    Route::resource('standar', StandarController::class);

    #penugasan
	Route::group([ 'prefix' => 'penugasan', 'as' => 'penugasan.'], function () {
		Route::get('hapus/{id}', [PenugasanAuditorController::class, 'hapus'])->name('hapus');
		Route::any('data', [PenugasanAuditorController::class, 'data'])->name('data');
	});
    Route::resource('penugasan', PenugasanAuditorController::class);

    #pertanyaan
	Route::group([ 'prefix' => 'pertanyaan', 'as' => 'pertanyaan.'], function () {
		Route::get('hapus/{id}', [PertanyaanController::class, 'hapus'])->name('hapus');
		Route::any('data', [PertanyaanController::class, 'data'])->name('data');
	});
    Route::resource('pertanyaan', PertanyaanController::class);

    #periode
	Route::group([ 'prefix' => 'periode', 'as' => 'periode.'], function () {
		Route::get('hapus/{id}', [PeriodeController::class, 'hapus'])->name('hapus');
		Route::get('pesan', [PeriodeController::class, 'pesan'])->name('pesan');
		Route::post('kirim_pesan', [PeriodeController::class, 'kirim_pesan'])->name('kirim_pesan');
		Route::any('data', [PeriodeController::class, 'data'])->name('data');
	});
    Route::resource('periode', PeriodeController::class);

    #instrumen
	Route::group([ 'prefix' => 'instrumen', 'as' => 'instrumen.'], function () {
		Route::get('standar/{id}', [JawabanController::class, 'standar'])->name('standar');
		Route::post('simpan_jawaban', [JawabanController::class, 'simpan_jawaban'])->name('simpan');
		Route::get('disable/{id?}', [JawabanController::class, 'nonaktif'])->name('disable');
		Route::post('ambil_pilihan', [JawabanController::class, 'ambil_pilihan'])->name('ambil_pilihan');
		Route::get('ambil_file/{file}', [JawabanController::class, 'ambil_file'])->name('ambil_file');
		Route::post('publish', [JawabanController::class, 'publis'])->name('publish');
    });

    #ncr
	Route::group([ 'prefix' => 'ncr', 'as' => 'ncr.'], function () {
        Route::get('ambil_file/{file}', [NcrController::class, 'ambil_file'])->name('ambil_file');
        Route::get('ambil_file_konfirmasi/{file}', [NcrController::class, 'ambil_file_konfirmasi'])->name('ambil_file_konfirmasi');
        Route::get('ambil_file_ncr/{file}', [NcrController::class, 'ambil_file_ncr'])->name('ambil_file_ncr');
        Route::get('standar/{id}/{periode?}', [NcrController::class, 'standar'])->name('standar');
        Route::get('tindakan/{id}', [NcrController::class, 'tindakan'])->name('tindakan');
        Route::get('penyebab/{id}', [NcrController::class, 'penyebab'])->name('penyebab');
        Route::get('catatan-verifikasi/{id}', [NcrController::class, 'catatan_verifikasi'])->name('catatan_verifikasi');
        Route::get('pencegahan/{id}', [NcrController::class, 'pencegahan'])->name('pencegahan');
        Route::get('prodikoreksi/{id}', [NcrController::class, 'prodikoreksi'])->name('prodikoreksi');
        Route::get('reupload/{id}', [NcrController::class, 'reupload'])->name('reupload');
        Route::get('verifikasi/{id}', [NcrController::class, 'verifikasi'])->name('verifikasi');
        Route::get('nilai/{id}', [NcrController::class, 'nilai'])->name('nilai');
        Route::get('unggah/{id}', [NcrController::class, 'unggah'])->name('unggah');
        Route::any('unggah_file/{id}', [NcrController::class, 'unggah_file'])->name('unggah_file');
        Route::any('cetak/{id}/{periode?}', [NcrController::class, 'cetak'])->name('cetak');
	});
	Route::resource('ncr', NcrController::class);

    #abr
	Route::group([ 'prefix' => 'abr', 'as' => 'abr.'], function () {
        Route::get('/{periode?}', [AbrController::class, 'index']);
        Route::get('ambil_file/{id}', [AbrController::class, 'ambil_file'])->name('ambil_file');
        Route::get('standar/{id}', [AbrController::class, 'standar'])->name('standar');
        Route::any('cetak/{periode?}', [AbrController::class, 'cetak'])->name('cetak');
        Route::any('ditandatangani/{periode?}', [AbrController::class, 'ditandatangani'])->name('ditandatangani');
        Route::any('tindakan/{id}', [AbrController::class, 'tindakan'])->name('tindakan');
        Route::get('modal_unggah/{id}', [AbrController::class, 'modal_unggah'])->name('modal_unggah');
        Route::any('unggah', [AbrController::class, 'unggah'])->name('unggah');
	});
	Route::resource('abr', AbrController::class);

    #unggah
	Route::group([ 'prefix' => 'unggah', 'as' => 'unggah.'], function () {
        Route::any('data', [UnggahController::class, 'data'])->name('data');
        Route::get('ambil_file/{id}', [UnggahController::class, 'ambil_file'])->name('ambil_file');
        Route::get('modal_unggah/{id}', [UnggahController::class, 'modal_unggah'])->name('modal_unggah');
        Route::any('unggah', [UnggahController::class, 'unggah'])->name('unggah');
		Route::get('hapus/{id}', [UnggahController::class, 'hapus'])->name('hapus');
	});
	Route::resource('unggah', UnggahController::class);


});


Route::get('/{page?}', [AdminController::class, 'index']);
