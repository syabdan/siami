<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('backend/login') }}/fonts/icomoon/style.css">

    <link rel="stylesheet" href="{{ asset('backend/login') }}/css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('backend/login') }}/css/bootstrap.min.css">

    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('backend/login') }}/css/style.css">

    <title>SIAMI UIR</title>
  </head>
  <body>



  <div class="content">
    <div class="container">
      <div class="row">

        <div class="col-md-6 contents">
            <div class="row justify-content-center">
              <div class="col-md-8">
              <div class="mb-4">
                    {{-- <h3>SIAMI UIR</h3> --}}
                    <img src="{{ asset('/') }}/frontend/images/siami6.png" alt="alternative" width="50%">
                    <p class="mb-4" style="color:#4d8fac">Sistem Informasi Audit Mutu Internal Universitas Islam Riau</p>
              </div>
              <form action="{{ route('login') }}"  method="POST">
                 @csrf
                <div class="form-group first">
                  <label for="email">Email</label>
                  <input id="email" type="email" class="input100 form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group last mb-4">
                  <label for="password">Password</label>
                  <input  id="password" type="password" class="input100 form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                  @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
                <button type="submit" class="btn btn-block" style="background-color:#00b5cc; color:#fff">
                    Masuk
                </button>

              </form>
              <br>
              <a href="{{ url('/') }}" style="text-decoration: none; color: #00b5cc; margin-top:50px"><img src="{{ asset('assets/images/svgs/arrow_back-24px.svg') }}" alt=""> Kembali</a>

              </div>

            </div>

          </div>
        <div class="col-md-6">
          <img src="{{ asset('backend/login') }}/images/undraw_data_trends_b0wg.svg" alt="Image" class="img-fluid">
        </div>


      </div>
    </div>
  </div>


    <script src="{{ asset('backend/login') }}/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('backend/login') }}/js/popper.min.js"></script>
    <script src="{{ asset('backend/login') }}/js/bootstrap.min.js"></script>
    <script src="{{ asset('backend/login') }}/js/main.js"></script>
  </body>
</html>
