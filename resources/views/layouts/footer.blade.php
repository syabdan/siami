<!-- FOOTER -->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-md-12 col-sm-12 text-center">
							Copyright © {{ date('Y') }} <a href="#">SIAMI</a>. Developed by <a href="https://www.instagram.com/syabdandalimunthe/"> Universitas Islam Riau </a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
<!-- FOOTER END -->
