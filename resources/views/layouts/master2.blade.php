<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <!-- META DATA -->
        <meta charset="UTF-8">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Sistem Informasi Audit Manajemen Mutu Internal Universitas Islam Riau">
        <meta name="author" content="Syabdan Dalimunthe">
        <meta name="keywords" content="siami, uir, sistem informasi audit mutu internal, sisfo uir, sikad uir">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @include('layouts.head')
    </head>

    <body class="login-img">
        @yield('content')
        @include('layouts.footer-scripts')
    </body>
</html>
