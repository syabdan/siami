<script>
    $(document).on("click",".ubah-user",function() {
    adanLoadingFadeIn();
        var id = $(this).attr('user-id');
        $.loadmodal({
            url: "{{ url($url_admin.'/edit_profile') }}/"+ id ,
            id: 'responsive',
            dlgClass: 'fade',
            bgClass: 'warning',
            title: 'Ubah User',
            width: 'whatever',
            modal: {
                keyboard: true,
                // any other options from the regular $().modal call (see Bootstrap docs)
                },
        ajax: {
                dataType: 'html',
                method: 'GET',
                success: function(data, status, xhr){
                adanLoadingFadeOut();
                },
            },
        });
    });


</script>
