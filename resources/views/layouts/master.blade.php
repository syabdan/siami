<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>

        <!-- META DATA -->
        <meta charset="UTF-8">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Sistem Informasi Audit Manajemen Mutu Internal Universitas Islam Riau">
        <meta name="author" content="Syabdan Dalimunthe">
        <meta name="keywords" content="siami, uir, sistem informasi audit mutu internal, sisfo uir, sikad uir">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @include('layouts.head')
        <style>
            .blink {
                animation: blink-animation 1s steps(2, start) infinite;
                -webkit-animation: blink-animation 1s steps(2, start) infinite;
            }
            @keyframes blink-animation {
                to {
                    visibility: hidden;
                }
            }
            @-webkit-keyframes blink-animation {
                to {
                    visibility: hidden;
                }
            }
        </style>
    </head>

    <body class="app sidebar-mini">
        <!-- GLOBAL-LOADER -->
        <div id="global-loader" class="global-loader">
            <img src="{{URL::asset('assets/images/loader.svg')}}" class="loader-img" alt="Loader">
        </div>
        <!-- /GLOBAL-LOADER -->
        <!-- PAGE -->
         <div class="page">
         <div class="page-main">
            @include('layouts.app-sidebar')
            @include('layouts.mobile-header')
        <div class="app-content">
        <div class="side-app">
        <div class="page-header">
            @yield('page-header')
            @include('layouts.notification')
        </div>
            @yield('content')

            @include('layouts.sidebar')
            @include('layouts.footer')
        </div>
        </div>

            @include('layouts.footer-scripts')
            @include('layouts.edituser-scripts')

            @if(\Auth::user()->permission_id == 3)
            @include('backend.auditor.b_acara')
            <script>
                $(document).on("click",".beritaAcara",function() {
                   $('#modalBeritaAcara').modal('show');
               });
           </script>
            @endif


    </body>
</html>
