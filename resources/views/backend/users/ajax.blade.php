$(document).on("click",".submit-tambah",function() {
	$('#frmAdan').submit();
});

$( ".submit-ubah" ).click(function() {
	$('#frmAdan-ubah').submit();

});

$( ".submit-ubah-user" ).click(function() {
	$('#frmAdan-profile').submit();

});

$( ".submit-hapus" ).click(function() {
	var url					= "{{ url($url_admin.'/users') }}/"+$("#id").val();
	var dataString			= $('#frmAdan').serializeArray();
	goAjax(url, dataString);
});

$('#frmAdan').on('submit', function(event){
	event.preventDefault();
	$.ajax({
	url:"{{ url($url_admin.'/users') }}",
	method:"POST",
	enctype: "multipart/form-data",
	data:new FormData(this),
	dataType:'JSON',
	contentType: false,
	cache: false,
	processData: false,
	beforeSend: function(){
				sebelumKirim();
		},
		success: function(data){
				successMsg(data);
		},
		error: function(x, e){
			//	errorMsg(x.status);
		}
	})
});

$('#frmAdan-ubah').on('submit', function(event){
	event.preventDefault();
	$.ajax({
	url:"{{ url($url_admin.'/users') }}/"+$("#id").val(),
	method:"POST",
	enctype: "multipart/form-data",
	data:new FormData(this),
	dataType:'JSON',
	contentType: false,
	cache: false,
	processData: false,
	beforeSend: function(){
				sebelumKirim();
		},
		success: function(data){
				successMsg(data);
		},
		error: function(x, e){
			//	errorMsg(x.status);
		}
	})
});

$('#frmAdan-profile').on('submit', function(event){
	event.preventDefault();
	$.ajax({
	url:"{{ url($url_admin.'/update_profile') }}/"+$("#id").val(),
	method:"POST",
	enctype: "multipart/form-data",
	data:new FormData(this),
	dataType:'JSON',
	contentType: false,
	cache: false,
	processData: false,
	beforeSend: function(){
				sebelumKirim();
		},
		success: function(data){
				successMsg(data);
		},
		error: function(x, e){
			//	errorMsg(x.status);
		}
	})
});
