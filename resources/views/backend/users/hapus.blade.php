{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'DELETE')) !!}
<div class="row">
	<div class="col-md-12">
		<p>
			<label class="control-label">Hapus data <strong>{{ $user->nama_user }} ?</strong></label>
			{!! Form::hidden('id', $user->id, array('id' => 'id')) !!}
			{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
		</p>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
        <span class="pesan"></span>
    </div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('assets/js/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/users/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>
