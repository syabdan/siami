<link href="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
<style>
    .ui-datepicker{
        z-index: 9999999 !important;
    }
</style>
{!! Form::open(array('id' => 'frmAdan-profile', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
<div class="row">
	<div class="col-md-12">
        @if(\Auth::user()->permission_id == 1)
        <div class="form-group row">
			{!! Form::label('Level', 'Level *', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-12">
  			{!! Form::select('permission_id', $permission, $data->permission_id, array('id' => 'permission_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
        </div>
        @endif
		<div class="form-group row" id="namaUser" style="display:none">
			{!! Form::label('Nama', 'Nama', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			{!! Form::text('nama_user', $data->nama_user, array('id' => 'nama_user', 'class' => 'form-control', 'placeholder' => 'eg. Dekan Fakultas Teknik', 'readonly' => 'true')) !!}
			</div>
		</div>
		<div class="form-group row">
			{!! Form::label('Email', 'Email', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			{!! Form::text('email', $data->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => 'eg. eng.uir.ac.id', 'readonly' => 'true')) !!}
			</div>
		</div>
		<div class="form-group row">
			{!! Form::label('Password', 'Password', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			<input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
			<span class="required"><i> Kosongkan jika tidak perlu </i></span>
			</div>
		</div>

		<div class="form-group row">
			{!! Form::label('Foto', 'Foto', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-12">
			{!! Form::file('pic', array('id' => 'pic', 'class' => 'dropify', 'data-height' => '300')) !!}
			</div>
		</div>
		{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
		{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
		<div class="form-group row">
			<div class="col-md-12">
				<span class="pesan"></span>
			</div>
		</div>

	</div>
</div>

{!! Form::close() !!}
<script src="{{ URL::asset('adan/users/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/fileupload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/file-upload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js') }}"></script>
<script>

    $('.select2').select2();

    $('#permission_id').on('change', function() {
	(this.value == 3) ? $('#lulus_auditor').show(500) : $('#lulus_auditor').hide(500);
    ((this.value == 1) || (this.value == 2) || (this.value == 4)) ? $('#namaUser').show(500) : $('#namaUser').hide(500);
	((this.value == 3) || (this.value == 4)) ? $('#prodi').show(500) : $('#prodi').hide(500);
	});

</script>


