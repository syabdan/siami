<link href="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
<style>
    .ui-datepicker{
        z-index: 9999999 !important;
    }
</style>
{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
<div class="row">
	<div class="col-md-12">
        <div class="form-group row">
			{!! Form::label('Level', 'Level *', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-12">
  			{!! Form::select('permission_id', $permission, $data->permission_id, array('id' => 'permission_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
        </div>
		<div class="form-group row" id="namaUser" style="display:none">
			{!! Form::label('Nama', 'Nama', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			{!! Form::text('nama_user', $data->nama_user, array('id' => 'nama_user', 'class' => 'form-control', 'placeholder' => 'eg. Dekan Fakultas Teknik')) !!}
			</div>
		</div>
		<div class="form-group row">
			{!! Form::label('Email', 'Email', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			{!! Form::text('email', $data->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => 'eg. eng.uir.ac.id')) !!}
			</div>
        </div>
        <div class="form-group row">
			{!! Form::label('No Handphone', 'No Handphone', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			{!! Form::text('hp', $data->hp, array('id' => 'hp', 'class' => 'form-control', 'placeholder' => 'eg. 082388865234')) !!}
			</div>
        </div>
		<div class="form-group row">
			{!! Form::label('Password', 'Password', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			<input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
			<span class="required"><i> Kosongkan jika tidak perlu </i></span>
			</div>
		</div>

    <div id="dataDosen" style="display:none">
        <div class="form-group row">
            {!! Form::label('NIDN', 'NIDN', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-12">
            {!! Form::text('nidn', $dosen->nidn ?? null, array('id' => 'nidn', 'class' => 'form-control', 'placeholder' => 'eg. 1029078701')) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('Nama Dosen', 'Nama Dosen', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-12">
            {!! Form::text('nama_dosen', $dosen->nama_dosen ?? null, array('id' => 'nama_dosen', 'class' => 'form-control', 'placeholder' => 'eg. Nesi Syafitri')) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('Gelar Depan', 'Gelar Depan', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-12">
            {!! Form::text('gelar_depan', $dosen->gelar_depan ?? null, array('id' => 'gelar_depan', 'class' => 'form-control', 'placeholder' => 'eg. Prof.Dr ')) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('Gelar Belakang', 'Gelar Belakang', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-12">
            {!! Form::text('gelar_belakang', $dosen->gelar_belakang ?? null, array('id' => 'gelar_belakang', 'class' => 'form-control', 'placeholder' => 'eg. S.Kom., M.Cs ')) !!}
            </div>
        </div>
    </div>
        <div id="lulus_auditor" @if($data->permission_id != 3) style="display:none" @endif>
            <div class="form-group row">
				<label class="col-md-4 control-label">Tanggal Lulus Auditor
				</label>
				<div class="col-md-12">
					<div class="input-group">
                        <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                            <input class="form-control" type="text"  name="tgl_lulus_auditor" value="{{ $data->tgl_lulus_auditor }}">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                        </div>
                    </div>
				</div>
			</div>
        </div>
        <div class="form-group row" id="prodi" style="display:none">
            <div class="form-group row">
                {!! Form::label('Program Studi', 'Program Studi *', array('class' => 'col-md-4 form-label')) !!}
                <div class="col-md-12">
                {!! Form::select('unit_id', $unit, $data->unit_id, array('id' => 'unit_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
                </div>
            </div>
        </div>
		<div class="form-group row">
			{!! Form::label('Foto', 'Foto', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-12">
			{!! Form::file('pic', array('id' => 'pic', 'class' => 'dropify', 'data-height' => '300')) !!}
			</div>
		</div>
		{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
		{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
		<div class="form-group row">
			<div class="col-md-12">
				<span class="pesan"></span>
			</div>
		</div>

	</div>
</div>

{!! Form::close() !!}
<script src="{{ URL::asset('adan/users/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/fileupload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/file-upload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js') }}"></script>
<script>

    $('.select2').select2();

    $('#permission_id').on('change', function() {
        (this.value == 3) ? $('#lulus_auditor').show(500) : $('#lulus_auditor').hide(500);
        ((this.value == 1) || (this.value == 2) || (this.value == 4)) ? $('#namaUser').show(500) : $('#namaUser').hide(500);
        ((this.value == 3) || (this.value == 4)) ? $('#prodi').show(500) : $('#prodi').hide(500);
        ((this.value == 3) || (this.value == 4)) ? ($('#dataDosen').show(500)) : $('#dataDosen').hide(500);
	});
    $(document).ready(function(){
        ({{ $data->permission_id }} == 3) ? $('#lulus_auditor').show(500) : $('#lulus_auditor').hide(500);
        (({{ $data->permission_id }} == 1) || ({{ $data->permission_id }} == 2) || ({{ $data->permission_id }} == 4)) ? $('#namaUser').show(500) : $('#namaUser').hide(500);
        (({{ $data->permission_id }} == 3) || ({{ $data->permission_id }} == 4)) ? $('#prodi').show(500) : $('#prodi').hide(500);
        (({{ $data->permission_id }} == 3) || ({{ $data->permission_id }} == 4)) ? ($('#dataDosen').show(500)) : $('#dataDosen').hide(500);
    });
</script>


