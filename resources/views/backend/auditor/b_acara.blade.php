
{{-- Modal Cetak Berita Acara --}}
<div id="modalBeritaAcara" class="modal hide fade" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog" role="document">
        @php
            $id = \Auth::user()->id;
        @endphp
        <div class="modal-content">
            {!! Form::open(array('id' => 'frmAdan-beritaAcara',  'route' => 'dashboard.berita_acara','class' => 'form account-form', 'target' => '_blank', 'method' => 'POST')) !!}
            {{ csrf_field() }}
            <div class="modal-header">
                <h4>Cetak Berita Acara</h4>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                    {!! Form::label('Nama Dekan', 'Nama Dekan', array('class' => 'col-md-4 form-label')) !!}
                    <div class="col-md-12">
                    {!! Form::text('dekan', NULL, array('id' => 'dekan', 'class' => 'form-control', 'placeholder' => 'eg. Nesi Syafitri, S.Kom., M.Cs')) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('Siklus', 'Siklus', array('class' => 'col-md-4 form-label')) !!}
                    <div class="col-md-12">
                    {!! Form::number('siklus', NULL, array('id' => 'siklus', 'min' => '1', 'class' => 'form-control', 'placeholder' => 'eg. 4')) !!}
                    </div>
                </div>
            </div><!-- MODAL-BODY -->
            <div class="modal-footer" >
                <a type="button" href="#" class="btn btn-outline-secondary" data-dismiss="modal"><i class="ti-arrow-left"></i> Kembali</a>
                <input type="submit" class="btn btn-outline-primary" value="Cetak">

            </div>
            {!! Form::close() !!}
        </div>
    </div><!-- MODAL-DIALOG -->
</div>
{{-- End Modal Cetak Berita Acara --}}

