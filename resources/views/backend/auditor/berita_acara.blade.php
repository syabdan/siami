<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Cetak Berita Acara AMI</title>
<style>
.left    { text-align: left;}
.right   { text-align: right;}
.center   { text-align: center;}
.center-uir  {
     text-align: center;
     font-size: 22pt;
     font-weight: bold;
}

.center-pekanbaru  {
	text-align: center;
    font-size: 10pt;
    font-weight: bold;
}

.judul {
    margin-top: 180px;
    text-align: center;
    font-size: 12pt;
    font-weight: bold;
    line-height: 1.2;
}

.judul2 {
    font-size: 10pt;
    font-weight: bold;
    line-height: 1.2;
}

.nama_user{
    font-size: 9pt;
}
/* Reset chapter and figure counters on the body */
body {
    font-family: "Trebuchet MS", "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Tahoma, sans-serif;
    line-height: 1.5;
    font-size: 11pt;
    margin-left: 1.5cm;
    margin-right: 1cm;
  }

  /* Get the title of the current chapter, which will be the content of the h1.
  Reset figure counter because figures start from 1 in each chapter. */
  h1 {
    line-height: 1.3;
  }

  table, tr, td {
      margin-left: 10px;
  }

  td{
    padding: 5px 5px 0px;
    text-align: left;
  }


</style>
</head>
<body>
    {{-- <table width="100%" border="0" style="border-bottom:3px solid #000; margin-top: -20px; margin-bottom: 30px">
            <tr>
            <th rowspan="3" width="80" ><center><img src="{{ asset('assets/images/brand/uir.png') }}" width="90%"></center></th>
            <th class="center-uir">UNIVERSITAS ISLAM RIAU</th>
            </tr>
            <tr >
            <th class="center-pekanbaru" >
                Jl. Kaharudin Nasution No. 113 Marpoyan Pekanbaru, Riau, Indonesia
                <br>
                Telp. +62761674674 Fax. +62761674834 Website: www.uir.ac.id Email: info@uir.ac.id
            </th>
            </tr>
            <tr style="margin-top: -200px"></tr>
    </table> --}}
    <br>

   <div class="judul">
    BERITA ACARA <br>
    Audit Mutu Internal (AMI) <br>
    Program Studi
   </div>

   <p>
    Pada Hari ini {{ hariIni() }}, Tanggal {{ date('d') }} Bulan {{ bulan(date('Y-m-d')) }} Tahun {{ date('Y') }} Pukul {{ date('H:i') }} WIB, di Kampus Universitas Islam Riau telah dilakukan Audit Mutu Internal (AMI) Siklus ke {{ $siklus ?? null }} Periode Audit {{ $ketua->nama_periode }}, oleh {{ $user->count() ?? null }} Orang Tim Audit Mutu Internal Universitas Islam Riau kepada:
   </p>
   <br><br>
   <div class="judul2">
       AUDITEE
   </div>

   <table style="width: 100%">
       <tr>
           <td>Fakultas</td>
           <td colspan="2">: {{ $unit->parent->nama_unit }}</td>
       </tr>
       <tr>
           <td>Program Studi</td>
           <td colspan="2" >: {{ $unit->nama_unit }}</td>
       </tr>
       <tr>
           <td style="width: 135px">Nama Ka.Prodi</td>
           <td class="" >: {{ $auditee->dosen->gelar_depan ?? null }} {{ $auditee->dosen->nama_dosen }} {{ $auditee->dosen->gelar_belakang }}</td>
           <td class="right" >Tanda Tangan ....................</td>
       </tr>
   </table>

   <br>
   <div class="judul2">
       TIM AUDITOR
   </div>
   <table style="width: 100%">
    <tr>
        <td style="width: 5px">1</td>
        <td style="width: 115px">Ketua </td>
        <td class="">: {{ $ketua->nama_user }}</td>
        <td class="right" >Tanda Tangan ....................</td>
    </tr>
    @php
        $no=2;
    @endphp
    @foreach ($user->where('posisi', '===','Anggota') as $anggota )

    <tr>
        <td style="width: 5px">{{ $no }}</td>
        <td> Anggota</td>
        <td class="">: {{ $anggota->nama_user }}</td>
        <td class="right" >Tanda Tangan ....................</td>
    </tr>
    @php
        $no++;
    @endphp
    @endforeach

</table>
<br><br>
<p>
    Dengan hasil audit yang telah dipahami oleh Auditee disetujui oleh Auditor. Demikian Berita Acara ini dibuat dengan sebenarnya.
</p>
<br><br>
<table style="width: 100%">
    <tr>
        <td class="center judul2">Mengetahui Pimpinan Fakultas <br> DEKAN</td>
        <td class="center judul2">Ketua Auditor,</td>
    </tr>
    <tr>
        <td style="height: 80px"></td>
        <td></td>
    </tr>
    <tr>
        <td class="center judul2">( {{ $dekan }} )</td>
        <td class="center judul2">( {{ $ketua->nama_user }} )</td>
    </tr>
</table>

</body>
</html>
