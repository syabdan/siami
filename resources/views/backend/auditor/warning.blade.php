<link href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
{{-- Modal belum ditempatkan --}}
<div id="modalAudit" class="modal hide fade" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Pemberitahuan</h4>
            </div>
            <div class="modal-body">
                <h3>Anda Belum ditugaskan !</h3>
                <p>Anda belum ditugaskan di Auditee manapun. Silahkan hubungi administrator untuk tindakan lebih lanjut. terimakasih</p>
                <h6 ><i class="ti-email"></i> : lpm@uir.ac.id</h6>
            </div><!-- MODAL-BODY -->
            <div class="modal-footer" >


                <a type="button" href="{{ route('logout') }}" class="btn btn-secondary"><i class="ti-power-off"></i> Keluar</a>
            </div>
        </div>
    </div><!-- MODAL-DIALOG -->
</div>
{{-- End Modal belum ditempatkan --}}


{{-- Modal pilih auditee --}}
<div id="modalPilih_auditee" class="modal hide fade" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog" role="document">
        @php
            $id = \Auth::user()->id;
        @endphp
        <div class="modal-content">
            {!! Form::open(array('id' => 'frmAdan-ubah',  'url' => 'update_unit_auditor/'.$id,'class' => 'form account-form', 'method' => 'PUT')) !!}
            {{ csrf_field() }}
            <div class="modal-header">
                <h4>Pilih Auditee</h4>
            </div>
            <div class="modal-body">
                @if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2)
                <div class="form-group row">
                    {!! Form::label('Pilih Periode', 'Pilih Periode *', array('class' => 'col-md-4 form-label')) !!}
                    <div class="col-md-12">
                    {!! Form::select('periode_id', $pilihPeriode, NULL, array('id' => 'periode_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'required' => 'true', 'style' => 'width:100%')) !!}
                    </div>
                </div>
                @endif
                <div class="form-group row">
                    {!! Form::label('Unit Auditee', 'Unit Auditee *', array('class' => 'col-md-4 form-label')) !!}
                    <div class="col-md-12">
                    {!! Form::select('unit_id', $pilihUnitAuditee, NULL, array('id' => 'unit_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'required' => 'true', 'style' => 'width:100%')) !!}
                    </div>
                </div>
            </div><!-- MODAL-BODY -->
            <div class="modal-footer" >
                @if(\Auth::user()->permission_id == 3)
                <a type="button" href="{{ route('logout') }}" class="btn btn-secondary"><i class="ti-power-off"></i> Keluar</a>
                @else
                <a type="button" href="" class="btn btn-secondary" data-dismiss="modal"><i class="ti-close"></i> Kembali</a>
                @endif
                {!! Form::submit('Submit',  array('class' => 'btn btn-primary')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div><!-- MODAL-DIALOG -->
</div>
{{-- End Modal pilih auditee --}}


<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js') }}"></script>
<script>
    $('.select2').select2();
</script>
