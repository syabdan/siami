var nextUrl=$("#url").val();
function errorMsg(pesan){
		$(".pesan").html('<div class="alert alert-danger" role="alert"><i class="fa fa-ban"></i> Terjadi kesalahan. Error '+ pesan +'</div>');
		$("button").prop("disabled", false);
		$("input").prop('disabled', false);
		$(".loading").hide();
		$(".fa-floppy-o").show();
}

function successMsg(data){
		if(data.status == true){
            swal("Selamat!", "Berhasil diproses.", "success");
            {{-- $.growl.notice({
                title: "Selamat !",
                message: "Berhasil diproses."
              }); --}}
            {{-- $('.modal').modal('hide');
            $("button").prop("disabled", false);
            $("input").prop('disabled', false);
            $(".loading").hide();
            $(".fa-floppy-o").show()
            table.ajax.reload();
            $(".bersihkan").prop('disabled', false); --}}
			location.href= nextUrl;
		}else{
			swal("Oppss!", "Gagal diproses.", "error");
			$("button").prop("disabled", false);
			$("input").prop('disabled', false);
			$.each(data.pesan, function(i, item) {
				$('#'+i).closest('.form-group').addClass('has-error');
				$('#'+i).focus();
				$(".pesan").html('<div class="alert alert-danger" role="alert"><i class="fa fa-ban"></i> '+ item +'</div>');
				return false;
			});
		}
		$(".loading").hide();
		$(".fa-floppy-o").show();

}
function sebelumKirim(){
		$("button").prop("disabled", true);
		$("input").prop('disabled', true);
		$(".loading").show();
		$(".fa-floppy-o").hide();
}

function goAjax(targetUrl, dataString, methodType = 'POST'){
		$.ajax({
			type: methodType,
			url: targetUrl,
			data: dataString,
			enctype: 'multipart/form-data',
			dataType: 'json',
			cache: false,
			beforeSend: function(){
					sebelumKirim();
			},
			success: function(data){
					successMsg(data);
			},
			error: function(x, e){
					errorMsg(x.status);
			}
		});
}

function goAjaxFile(targetUrl, targetForm ){
		$.ajax({
			type: 'POST',
			url: targetUrl,
			data: new FormData(targetForm),
			enctype: 'multipart/form-data',
			dataType: 'json',
			cache: false,
			beforeSend: function(){
					sebelumKirim();
			},
			success: function(data){
					successMsg(data);
			},
			error: function(x, e){
					errorMsg(x.status);
			}
		});
}

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}
