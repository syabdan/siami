@extends('layouts.master')
@section('css')
<link href="{{ URL::asset('assets/plugins/datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/date-picker/spectrum.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/multipleselect/multiple-select.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/time-picker/jquery.timepicker.css')}}" rel="stylesheet" />
@endsection

@section('page-header')
<!-- PAGE-HEADER -->
    <div>
        <h1 class="page-title">{{$halaman->nama}}</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">{{$parent->nama}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$halaman->nama}}</li>
        </ol>
    </div>
<!-- PAGE-HEADER END -->
@endsection
@section('content')
<!-- ROW-1 OPEN -->
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row col-md-12">
                    <div class="col-md-10">
                        <h3 class="card-title">{{$halaman->nama}}</h3>
                    </div>
                    <div class="col-md-2 col-sm-2 col-2 text-right">
                        <div class="btn-group">
                            <a class="btn btn-secondary-light btn-sm tambah mr-2">Tambah <i class="fa fa-plus"></i></a>

                            {{-- <a class="btn btn-info-light btn-sm">Kirim Pesan <i class="fa fa-envelope"></i></a> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table id="table" class="table table-striped table-bordered text-nowrap w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Auditor</th>
                                <th>Unit Auditee</th>
                                <th>Posisi</th>
                                <th>Periode</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- TABLE WRAPPER -->
        </div>
        <!-- SECTION WRAPPER -->
    </div>
</div>
<!-- ROW-1 CLOSED -->

</div>
</div>
<!-- CONTAINER CLOSED -->
</div>
@endsection

@section('js')
<script src="{{ URL::asset('assets/plugins/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/datatable.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/datatable-2.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::to('/') }}/adan/penugasan/jquery.js"></script>
<script>
    var table;
    $(document).ready( function () {
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
        table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
            url: '{!! route('penugasan.data') !!}',
            type: 'POST'
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                { data: 'nama_auditor' },
                { data: 'unit_auditee'},
                { data: 'posisi'},
                { data: 'periode.nama_periode'},
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ]
        });
        table.on( 'draw', function () {
            $('.livicon').each(function(){
                $(this).updateLivicon();
            });
        } );
    });
    </script>


@endsection
