<link href="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
<div class="form-group row" >
    {!! Form::label('Nama Auditor', 'Nama Auditor *', array('class' => 'col-md-4 form-label')) !!}
    <div class="col-md-12">
    {!! Form::select('user_id', $auditor, $data->user_id, array('id' => 'user_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('Unit Auditee', 'Unit Auditee *', array('class' => 'col-md-4 form-label')) !!}
    <div class="col-md-12">
    {!! Form::select('unit_audit_id', $unit, $data->unit_audit_id, array('id' => 'unit_audit_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('Periode', 'Periode *', array('class' => 'col-md-4 form-label')) !!}
    <div class="col-md-12">
    {!! Form::select('periode_id', $periode, $data->periode_id, array('id' => 'periode_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('Posisi', 'Posisi *', array('class' => 'col-md-4 form-label')) !!}
    <div class="col-md-12">
    {!! Form::select('posisi', ['Ketua' => 'Ketua', 'Anggota' => 'Anggota'], $data->posisi, array('id' => 'posisi', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
    </div>
</div>

	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/penugasan/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js') }}"></script>

<script>
	$('.select2').select2();

</script>
