<script>
    @foreach($units2 as $unit)
        Highcharts.chart("standar_{!! $unit->id  !!}", {
            chart: {
            polar: true
        },
        title: {
            text: "{!! $unit->nama_unit !!}"
        },
        subtitle: {
            // text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },
        pane: {
            startAngle: 0,
            endAngle: 360
        },
        xAxis: {
            categories: {!! json_encode($chart_ncr[$unit->id]['category'])  !!},

        },
        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },
        tooltip: {
            shared: true,
            // pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Prodi',
            data: {!! json_encode($chart_ncr[$unit->id]['rata_nilai_prodi'])  !!},
            pointPlacement: 'on'
        },
        {
            name: 'Auditor',
            data: {!! json_encode($chart_ncr[$unit->id]['rata_nilai_auditor'])  !!},
            pointPlacement: 'on'
        }],
        colors: ['#F62459', '#19B5FE'],

    });

    new Morris.Donut({
		element: "abr_unit_{!! $unit->id  !!}",
		data: {!! $chart_abr[$unit->id] !!},
		backgroundColor: 'rgba(119, 119, 142, 0.2)',
		labelColor: '#77778e',
		colors: ['#48929b', '#ffb61e', '#1f4788'],
		formatter: function(x) {
			return x + "%"
		}
	}).on('click', function(i, row) {
		console.log(i, row);
	});


    @endforeach

    // table monitoring
    var table;
    $(document).ready( function () {
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            bDestroy: true,
            ajax: {
            url: '{!! route('dashboard.data_monitoring') !!}',
            type: 'POST'
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                { data: 'nama_unit' },
                { data: 'pengisian' },
                { data: 'unggah_proses' },
                { data: 'mayor' },
                { data: 'minor'},
                { data: 'sesuai'},
                { data: 'high'},
                { data: 'medium'},
                { data: 'low'},
            ],
                    initComplete: function() {
                        $('.buttons-excel').html('<i class="fa fa-file" /> Unduh');
                        $('.buttons-print').html('<i class="fa fa-print" /> Cetak');
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        'pageLength',
                        {
                            extend: 'excelHtml5',
                            text: 'Unduh ke Excel',
                            title: 'Laporan Monitoring Sistem Audit Nutu Internal',
                            messageTop: ' ',
                        },
                        {
                            extend: 'print',
                            text: 'Cetak',
                            title: 'Laporan Monitoring Sistem Audit Nutu Internal',
                            orientation: 'landscape',
                            pageSize: 'LEGAL' ,

                        }

                    ],

                    lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],

        });
        table.on( 'draw', function () {
            $('.livicon').each(function(){
                $(this).updateLivicon();
            });
        });



    });


    $('#periode').on('change',function(){
        periode = this.value;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            bDestroy: true,
            ajax: {
            url: '{!! url('dashboard/data_monitoring') !!}'+'/'+periode,
            type: 'POST'
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                { data: 'nama_unit' },
                { data: 'pengisian' },
                { data: 'unggah_proses' },
                { data: 'mayor' },
                { data: 'minor'},
                { data: 'sesuai'},
                { data: 'high'},
                { data: 'medium'},
                { data: 'low'},
            ],
                    initComplete: function() {
                        $('.buttons-excel').html('<i class="fa fa-file" /> Unduh');
                        $('.buttons-print').html('<i class="fa fa-print" /> Cetak');
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        'pageLength',
                        {
                            extend: 'excelHtml5',
                            text: 'Unduh ke Excel',
                            title: 'Laporan Monitoring Sistem Audit Nutu Internal',
                            messageTop: ' ',
                        },
                        {
                            extend: 'print',
                            text: 'Cetak',
                            title: 'Laporan Monitoring Sistem Audit Nutu Internal',
                            orientation: 'landscape',
                            pageSize: 'LEGAL' ,

                        }

                    ],

                    lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],

        });
        table.on( 'draw', function () {
            $('.livicon').each(function(){
                $(this).updateLivicon();
            });
        });
    });
    // #end tabel monitoring


    // barchart ncr

/*chart-assessment*/
var chart = c3.generate({
 		bindto: '#chart-bar-ncr', // id of chart wrapper
 		data: {
 			columns: [
 				// each columns data
 				['data1',  @foreach($chart_ncr_per_prodi['mayor']  as $mayor)  {{ $mayor.',' }} @endforeach],
 				['data2', @foreach($chart_ncr_per_prodi['minor']  as $minor)  {{ $minor.',' }} @endforeach],
 				['data3', @foreach($chart_ncr_per_prodi['sesuai']  as $sesuai)  {{ $sesuai.',' }} @endforeach],
 			],
 			type: 'bar', // default type of chart
 			groups: [
 				['data1', 'data2', 'data3']
 			],
 			colors: {
                data1: '#f3c13a',
                data2: '#2abb9b',
                data3: '#4d8fac'
 			},
 			names: {
 				// name of each serie
 				'data1': 'Mayor',
 				'data2': 'Minor',
 				'data3': 'Sesuai',
 			}
 		},
 		axis: {
 			x: {
 				type: 'category',
 				// name of each category
 				categories: {!! json_encode($chart_ncr_per_prodi['category']) !!}
 			},
 		},
 		bar: {
 			width: 16
 		},
 		legend: {
 			show: true, //hide legend
 		},
 		padding: {
 			bottom: 0,
 			top: 0
 		},
 	});

    // #endbarchart ncr





</script>


