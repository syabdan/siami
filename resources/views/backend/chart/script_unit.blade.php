<script>
     // table monitoring
    var table;
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    table = $('#table_prodi').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: {
        url: '{!! route('dashboard.data_monitoring_prodi') !!}',
        type: 'POST'
        },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            { data: 'nama_periode' },
            { data: 'pengisian' },
            { data: 'unggah_proses' },
        ],
                initComplete: function() {
                    $('.buttons-excel').html('<i class="fa fa-file" /> Unduh');
                    $('.buttons-print').html('<i class="fa fa-print" /> Cetak');
                },
                dom: 'Bfrtip',
                buttons: [
                    'pageLength',
                    {
                        extend: 'excelHtml5',
                        text: 'Unduh ke Excel',
                        title: 'Laporan Monitoring Sistem Audit Nutu Internal',
                        messageTop: ' ',
                    },
                    {
                        extend: 'print',
                        text: 'Cetak',
                        title: 'Laporan Monitoring Sistem Audit Nutu Internal',
                        orientation: 'landscape',
                        pageSize: 'LEGAL' ,

                    }

                ],

                lengthMenu: [
                [ 10, 25, 50, -1 ],
                [ '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],

    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    });

</script>


<script>

    new Morris.Donut({
		element: 'abr_unit',
		data: {!! $abr_array !!},
		backgroundColor: 'rgba(119, 119, 142, 0.2)',
		labelColor: '#77778e',
		colors: ['#48929b', '#ffb61e', '#1f4788'],
		formatter: function(x) {
			return x + "%"
		}
	}).on('click', function(i, row) {
		console.log(i, row);
    });


/*chart-assessment*/
var chart = c3.generate({
 		bindto: '#chart-bar-stacked', // id of chart wrapper
 		data: {
 			columns: [
 				// each columns data
 				['data1',  @foreach($chart_ncr_per_standar['mayor'] as $mayor)  {{ $mayor.',' }} @endforeach],
 				['data2', @foreach($chart_ncr_per_standar['minor'] as $minor)  {{ $minor.',' }} @endforeach],
 				['data3', @foreach($chart_ncr_per_standar['sesuai'] as $sesuai)  {{ $sesuai.',' }} @endforeach],
 			],
 			type: 'bar', // default type of chart
 			groups: [
 				['data1', 'data2', 'data3']
 			],
 			colors: {
                data1: '#f3c13a',
                data2: '#2abb9b',
                data3: '#4d8fac',
 			},
 			names: {
 				// name of each serie
 				'data1': 'Mayor',
 				'data2': 'Minor',
 				'data3': 'Sesuai',
 			}
 		},
 		axis: {
 			x: {
 				type: 'category',
 				// name of each category
 				categories: {!! json_encode($chart_ncr_per_standar['category']) !!}
 			},
 		},
 		bar: {
 			width: 16
 		},
 		legend: {
 			show: true, //hide legend
 		},
 		padding: {
 			bottom: 0,
 			top: 0
 		},
 	});



</script>


