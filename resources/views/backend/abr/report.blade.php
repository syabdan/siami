<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- TITLE -->
    <title>SIAMI UIR</title>

    <!-- BOOTSTRAP CSS -->
    <link href="{{ URL::asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

    <style>
        body {
        font-family: tahoma, sans-serif;
        font-size: 10pt;
        /* margin-right: 2cm; */
        }

        table {
            border-collapse: collapse;

        }



        .label {
            padding: 5px 5px 2px 5px;

        }

        .isi{
            padding: 5px;
            font-size: 90%;
            vertical-align: text-top;

        }

        .nama_ttd{
            font-size: 8pt;
            width: 200px;
            padding: 2px 0px 5px 5px;

        }

        .table_abr {
            width: 100%;
            table-layout: fixed;
        }
    </style>
</head>
<body>


<!-- ROW-1 OPEN -->
<div class="row" >
    <div class="col-md-12 col-lg-12">
        <div class="card">

            <div class="card-body ">


                <div class="text-center" style="margin-top: -15px">
                    <p>
                        <img src="{{ asset('assets/images/brand/uir.png') }}" width="5%" style="margin-bottom: -20px">
                        <h4 style="margin-bottom: 0px">FORM AUDIT BERBASIS RISIKO TERKAIT KETIDAKSESUAIAN</h4>
                        <h6>No. Dokumen: UIR/LPM/F/05.03 - 10 Maret 2019</h6>
                    </p>

                </div>

                <table class="table_abr">
                    <tr>
                        <td style="width: 10%">Tanggal Audit</td>
                        <td>: {{ tanggal_indonesia(date('Y-m-d')) }}</td>
                    </tr>
                    <tr>
                        <td>Lokasi Audit</td>
                        <td>: {{ $data->nama_unit ?? null }}</td>
                    </tr>
                    <tr>
                        <td>Auditor</td>
                        <td>: @php
                            $no=1;
                            @endphp
                            @foreach ($auditors as $auditor)
                                {{ $auditor->nama_dosen }} @if($auditors->count() > $no) / @endif
                                @php
                                    $no++
                                @endphp
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>Auditee</td>
                        <td>: {{ $data->nama_unit ?? null }}</td>
                    </tr>
                </table>


            </div>
            <!-- TABLE WRAPPER -->
        </div>
        <!-- SECTION WRAPPER -->
    </div>
</div>
<!-- ROW-1 CLOSED -->

<table border="1" class="table_abr">
    <tr class="text-center">
        <th rowspan="2" style="width:25px;" class="label">No</th>
        <th rowspan="2" class="label" style="width: 250px">Ketidaksesuaian</th>
        <th rowspan="2" style="width:70px;" class="label">Kategori</th>
        <th colspan="2" class="label" style="width:500px;">Identifikasi Risiko Terkait Ketidaksesuaian</th>
        <th colspan="2" class="label" style="width:70px;">Risk Evaluasi</th>
        <th rowspan="2" class="label" style="width:85px;">Assesment <br> (H/M/L)</th>
        <th rowspan="2" class="label" style="width: 250px">Risk Mitigation </th>
    </tr>
    <tr class="text-center">
        <th style="width:50%;">Aspect / Penyebab (Risk)</th>
        <th style="width:50%;">Impact / Akibat (Risk)</th>
        <th width="30px">Li</th>
        <th width="30px">S</th>
    </tr>

    @php
        $no = 1;
    @endphp
    @foreach ($djawaban as $jawab)
    <tr class="isi">
        <td class="isi" style="text-align: center">{{  $no }}</td>
        <td class="isi" >{!! strip_tags($jawab->uraian) !!}</td>
        <td class="isi" style="text-align: center">{{  $jawab->temuan }}</td>
        <td class="isi" >{!!  strip_tags($jawab->aspect) !!}</td>
        <td class="isi" >{!!  strip_tags($jawab->impact) !!}</td>
        <td class="isi text-center">{!!  $jawab->likelihood !!}</td>
        <td class="isi text-center">{!!  $jawab->severity !!}</td>
        <td class="isi text-center">{!!  $jawab->assesment !!}</td>
        <td class="isi" >{!!  strip_tags($jawab->mitigation) !!}</td>

    </tr>
    @php
        $no++;
    @endphp
    @endforeach
</table>

<table style="margin-top:20px" style="table-layout: fixed">
    <tr>
        <td colspan="3" style="width: 1000px"><b> Legend: </b></td>
    </tr>
    <tr>
        <td colspan="3">likelihood (tingkat keseringan kemungkinan yang terjadi) dan Saverity (Keparahan dari Resiko)</td>
    </tr>
    <tr>
        <td style="width: 50px">Li</td>
        <td style="width: 100px">Likelihood</td>
        <td>(L : tidak pernah; M: Pernah terjadi; H: Sering terjadi)</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>(L : tidak mungkin; M: mungkin terjadi; H: sangat mungkin terjadi)</td>
    </tr>
    <tr>
        <td>S</td>
        <td>Severity</td>
        <td>(L : tidak menimbulkan efek mutu; M: menimbulkan tapi tidak significant; H: significant)</td>
    </tr>
    <tr>
        <td colspan="3">Nilai</td>
    </tr>
    <tr>
        <td></td>
        <td>Low</td>
        <td>Pegendalian dengan IK cukup</td>
    </tr>
    <tr>
        <td></td>
        <td>Mid</td>
        <td>Pegendalian dengan IK + monitoring</td>
    </tr>
    <tr>
        <td></td>
        <td>High</td>
        <td>Dibuatkan program</td>
    </tr>
</table>

<table>
<tr>
    <td>
        <table style="margin-top: 30px;" border="1" >
            <tr class="text-center" >
                <td>Auditor</td>
                <td>Auditee</td>
                <td>Verifikasi Auditor</td>
                <td>TTD</td>

            </tr>
            <tr>
                <td style="padding:70px"></td>
                <td style="padding:70px"></td>
                <td style="padding:70px"></td>
                <td style="padding:70px"></td>
            </tr>
            <tr>
                <td  class="nama_ttd">{{ $ketua->nama_dosen }}</td>
                <td  class="nama_ttd">{{ $auditee->nama_dosen }}</td>
                <td  class="nama_ttd">{{ $ketua->nama_dosen }}</td>
                <td rowspan="2">Tgl: </td>
            </tr>
            <tr>
                <td  class="nama_ttd">{{ $ketua->nidn }}</td>
                <td class="nama_ttd">{{ $auditee->nidn }}</td>
                <td  class="nama_ttd">{{ $ketua->nidn }}</td>
            </tr>
        </table>
    </td>
    <td width="50px">

    </td>
    <td >
        <table>
            <tr>
                <td style="padding: 0 10px 0 10px;" style="border:0">Likelihood</td>
                <td ><img src="{{ asset('assets/images/media/likelihood.png') }}" width="250px"></td>
            </tr>
            <tr>
                <td colspan="2" class="text-center" style="border:0 solid #fff">Severity</td>
            </tr>
        </table>
    </td>
</tr>
</table>
</body>
</html>
