<link href="{{ URL::asset('assets/plugins/wysiwyag/richtext.css')}}" rel="stylesheet">
{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
    <div class="form-group row">
        <h4> <b>Ketidaksesuaian:</b>  </h4>
        <h5>{!! strip_tags($data->uraian) !!}</h5>
    </div>
    <br>
    <div class="form-group row">
        {!! Form::label('ASPECT / (PENYEBAB RISK)', 'ASPECT / (PENYEBAB RISK)', array('class' => 'col-md-4 control-label')) !!}
        <div class="col-md-12">
            <textarea name="aspect" id="aspect" class="content" cols="30" rows="2" required="required">{{ $data->catatan_penyebab }}</textarea>
        </div>
    </div>
    <hr>

    <div class="form-group row">
        {!! Form::label('IMPACT / (AKIBAT RISK)', 'IMPACT / (AKIBAT RISK)', array('class' => 'col-md-4 control-label')) !!}
        <div class="col-md-12">
            <textarea name="impact"  id="impact" class="content" cols="30" rows="3" required="required">{{ $data->impact }}</textarea>
        </div>
    </div>
    <hr>

    <div class="form-group row">
    {!! Form::label('Likelihood', 'Likelihood', array('class' => 'col-md-4 control-label')) !!}
        <div class="col-md-12">
            {!! Form::select('likelihood', ['H' => 'H', 'M' => 'M', 'L' => 'L'], $data->likelihood, array('id' => 'likelihood', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
        </div>
    </div>
    <hr>

    <div class="form-group row">
    {!! Form::label('Severity', 'Severity', array('class' => 'col-md-4 control-label')) !!}
        <div class="col-md-12">
            {!! Form::select('severity', ['H' => 'H', 'M' => 'M', 'L' => 'L'], $data->severity, array('id' => 'severity', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
        </div>
    </div>
    <hr>

    <div class="form-group row">
        {!! Form::label('RISK MITIGATION', 'RISK MITIGATION', array('class' => 'col-md-4 control-label')) !!}
        <div class="col-md-12">
            <textarea name="mitigation" id="mitigation" class="content" cols="30" rows="3" required="required">{{ $data->catatan_pencegahan }}</textarea>
        </div>
    </div>
    <hr>


	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/abr/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::asset('assets/plugins/wysiwyag/jquery.richtext.js') }}"></script>

<script>
    $('#aspect').richText();
    $('#impact').richText();
    $('#mitigation').richText();
</script>
