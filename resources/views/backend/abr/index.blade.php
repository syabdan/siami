@extends('layouts.master')
@section('css')
<link href="{{ URL::asset('assets/plugins/datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/date-picker/spectrum.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/multipleselect/multiple-select.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/time-picker/jquery.timepicker.css')}}" rel="stylesheet" />

<style>
    .box_table{
        border:1px solid #ccc;
        padding:10px;
        width:400px;
        max-height:500px;
        overflow:scroll;
        page-break-inside: avoid;
    }

</style>
@endsection

@section('page-header')
<!-- PAGE-HEADER -->
    <div>
        <h1 class="page-title">Audit Berbasis Risiko (ABR)</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active"><a href="#">Audit Berbasis Risiko (ABR)</a></li>
        </ol>
    </div>
<!-- PAGE-HEADER END -->
@endsection
@section('content')
<!-- ROW-1 OPEN -->
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row col-md-12">
                    <div class="col-md-8">
                        <h3 class="card-title">Audit Berbasis Risiko (ABR) {{ $periodeTerpilih->nama_periode }} @if(\Auth::user()->permission_id != 4) {{ auditee() ?? null }} @endif {!! ( periode() ?? null) ? periode() : '' !!}</h3>
                        @if(\Auth::user()->permission_id == 3 || \Auth::user()->permission_id == 4)
                        <div class="form-group row">
                            <div class="col-md-6 mt-5 text-right">
                            {!! Form::select('periode', $pilihPeriode, NULL, array('id' => 'periode', 'class' => 'form-control select2', 'placeholder' => 'Pilih Periode', 'style' => 'width:100%')) !!}
                            </div>
                            <span class="col-md-6"><i>NB* Jika Anda Ingin Melihat Periode Sebelumnya</i></span>
                        </div>
                        @endif

                    </div>
                    <div class="col-md-4 text-right">
                        <div class="btn-group">
                            @if(\Auth::user()->permission_id == 3)
                                <a href="{{ url('abr/cetak/'.($data->periode_id ?? null) ) }}" target="_blank"  class="btn btn-info-light btn-sm cetak"><i class="fa fa-download"></i> Cetak ABR </a>
                                <a href="#" data-id="{{ \Auth::user()->id }}" class="btn btn-secondary-light btn-sm unggah ml-2 mr-2"><i class="fa fa-upload"></i> Unggah ABR </a>
                            @endif

                            @if(cekDokumen())
                            {{-- <a href="{{ url('abr/ditandatangani/'.($data->periode_id ?? null) ) }}" target="_blank"  class="btn btn-primary-light btn-sm cetak"><i class="fa fa-search"></i> Lihat ABR </a> --}}
                            <a href="{!!cekDokumen()->urlfile!!}" target="_blank"  class="btn btn-primary-light btn-sm cetak"><i class="fa fa-search"></i> Lihat ABR </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body ">
                <div class="text-center pb-6">
                    <h3>FORM AUDIT BERBASIS RISIKO TERKAIT KETIDAKSESUAIAN</h3>
                    <h6>No. Dokumen: UIR/LPM/F/05.03 - 10 Maret 2019</h6>
                </div>

                <div class="row col-md-12">
                    <div class="col-md-2"><h6>Tanggal Audit</h6></div>
                    <div class="col-md-10"><h6>: {{ tanggal_indonesia(date('Y-m-d')) }}</h6></div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-2"><h6>Lokasi Audit</h6></div>
                    <div class="col-md-10"><h6>: {{ $data->nama_unit ?? null }}</h6></div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-2"><h6>Auditor</h6></div>
                    <div class="col-md-10"><h6>:
                        @php
                            $no=1;
                        @endphp
                         @foreach ($auditors as $auditor)
                            {{ $auditor->nama_dosen }} @if($auditors->count() > $no) / @endif
                            @php
                                $no++
                            @endphp
                         @endforeach
                    </h6></div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-2"><h6>Auditee</h6></div>
                    <div class="col-md-10"><h6>: {{ $data->nama_unit ?? null }}</h6></div>
                </div>
                <div class="table-responsive">
                    <table id="table" class="table table-striped table-bordered text-nowrap w-100">
                        <thead>
                            <tr>
                                <th rowspan="2">No</th>
                                @if(\Auth::user()->permission_id == 3)
                                    <th rowspan="2">Aksi </th>
                                @endif
                                <th rowspan="2">Ketidaksesuaian</th>
                                <th rowspan="2">Kategori</th>
                                <th colspan="2">Identifikasi Risiko Terkait Ketidaksesuaian</th>
                                <th colspan="2">Risk Evaluasi</th>
                                <th rowspan="2">Assesment <br> (H/M/L)</th>
                                <th rowspan="2">Risk Mitigation </th>

                            </tr>
                            <tr>
                                <th>Aspect / Penyebab (Risk)</th>
                                <th>Impact / Akibat (Risk)</th>
                                <th>Li</th>
                                <th>S</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($djawaban as $jawab)
                            <tr>
                                <td>{{  $no }}</td>
                                @if(\Auth::user()->permission_id == 3)
                                <td>
                                    <a href="#" data-id="{{ $jawab->id }}"  class="btn btn-outline-secondary btn-sm tindakan"><i class="fa fa-pencil"></i> Isi Tindakan </a>
                                </td>

                                @endif
                                <td><div class="box_table">{!!  $jawab->uraian !!}</div></td>
                                <td>{{  $jawab->temuan }}</td>

                                <td><div class="box_table">{!!  $jawab->aspect !!}</div></td>
                                <td><div class="box_table">{!!  $jawab->impact !!}</div></td>
                                <td>{!!  $jawab->likelihood !!}</td>
                                <td>{!!  $jawab->severity !!}</td>
                                <td>{!!  $jawab->assesment !!}</td>
                                <td><div class="box_table">{!!  $jawab->mitigation !!}</div></td>
                            </tr>
                            @php
                                $no++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- TABLE WRAPPER -->
        </div>
        <!-- SECTION WRAPPER -->
    </div>
</div>
<!-- ROW-1 CLOSED -->

</div>
</div>
<!-- CONTAINER CLOSED -->
</div>



@endsection

@section('js')
<script src="{{ URL::asset('assets/plugins/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/datatable.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/datatable-2.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::to('/') }}/adan/abr/jquery.js"></script>

<script>
    $(document).ready(function(){
        $('#periode').on('change', function() {
            window.location = "{{ url($url_admin.'/abr') }}/"+$(this).val();
        });
    });
</script>
@if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2)
    @if (\Auth::user()->unit_id === NULL)
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modalPilih_auditee').modal('show');
        });
    </script>
    @else
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modalPilih_auditee').modal('hide');
        });
    </script>
    @endif
@endif


@if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2)
    @include('backend.auditor.warning')
@endif


@endsection

