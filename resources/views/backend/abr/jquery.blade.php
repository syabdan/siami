$(document).ready(function(){

	$(document).on("click",".tindakan",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/abr/tindakan') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Input Tindakan',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".unggah",function() {
    adanLoadingFadeIn();
    var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/abr/modal_unggah') }}/"+ id,
            id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'success',
			title: 'Unggah',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});


});
