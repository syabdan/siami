<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- TITLE -->
    <title>SIAMI UIR</title>

    <!-- BOOTSTRAP CSS -->
    <link href="{{ URL::asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

    <style>
        body {
        font-family: tahoma, sans-serif;
        font-size: 10pt;
        }

        table {
            border-collapse: collapse;
        }

        .isi{
            padding: 2px 0px 5px 5px;
            font-size: 90%;
        }

        .nama_ttd{
            font-size: 8pt;
            width: 150px;
            padding: 2px 0px 5px 5px;

        }
    </style>
</head>
<body>


<!-- ROW-1 OPEN -->
<div class="row" >
    <div class="col-md-12 col-lg-12">
        <div class="card">

            <div class="card-body ">


                <div class="text-center" style="margin-top: -15px">
                    <p>
                        <img src="{{ asset('assets/images/brand/uir.png') }}" width="5%" style="margin-bottom: -20px">
                        <h4 style="margin-bottom: 0px">FORM AUDIT BERBASIS RISIKO TERKAIT KETIDAKSESUAIAN</h4>
                        <h6>No. Dokumen: UIR/LPM/F/05.03 - {{ tanggal_indonesia(date('Y-m-d'), false) }}</h6>
                    </p>

                </div>

                <table>
                    <tr>
                        <td>Tanggal Audit</td>
                        <td>: {{ tanggal_indonesia(date('Y-m-d')) }}</td>
                    </tr>
                    <tr>
                        <td>Lokasi Audit</td>
                        <td>: {{ $data->nama_unit ?? null }}</td>
                    </tr>
                    <tr>
                        <td>Auditor</td>
                        <td>: @php
                            $no=1;
                            @endphp
                            @foreach ($auditors as $auditor)
                                {{ $auditor->nama_dosen }} @if($auditors->count() > $no) / @endif
                                @php
                                    $no++
                                @endphp
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>Auditee</td>
                        <td>: {{ $data->nama_unit ?? null }}</td>
                    </tr>
                </table>


            </div>
            <!-- TABLE WRAPPER -->
        </div>
        <!-- SECTION WRAPPER -->
    </div>
</div>
<!-- ROW-1 CLOSED -->

<table border="1" width="100%">
    <tr class="text-center">
        <th rowspan="2" style="width:2%;">No</th>
        <th rowspan="2">Ketidaksesuaian</th>
        <th rowspan="2" style="width:3%;">Kategori</th>
        <th colspan="2" style="width:25%;">Identifikasi Risiko Terkait Ketidaksesuaian</th>
        <th colspan="2" style="width:10%;">Risk Evaluasi</th>
        <th rowspan="2" style="width:10%;">Assesment <br> (H/M/L)</th>
        <th rowspan="2" style="width:20%;">Risk Mitigation </th>
    </tr>
    <tr class="text-center">
        <th>Aspect / Penyebab (Risk)</th>
        <th>Impact / Akibat (Risk)</th>
        <th width="30px">Li</th>
        <th width="30px">S</th>
    </tr>

    @php
        $no = 1;
    @endphp
    @foreach ($djawaban as $jawab)
    <tr class="isi">
        <td class="isi">{{  $no }}</td>
        <td class="isi">{!! $jawab->uraian !!}</td>
        <td class="isi">{{  $jawab->temuan }}</td>
        <td class="isi">{!!  $jawab->aspect !!}</td>
        <td class="isi">{!!  $jawab->impact !!}</td>
        <td class="isi text-center">{!!  $jawab->likelihood !!}</td>
        <td class="isi text-center">{!!  $jawab->severity !!}</td>
        <td class="isi text-center">{!!  $jawab->assesment !!}</td>
        <td class="isi">{!!  $jawab->mitigation !!}</td>

    </tr>
    @php
        $no++;
    @endphp
    @endforeach
</table>

<table style="margin-top:20px">
    <tr>
        <td colspan="3"><b> Legend: </b></td>
    </tr>
    <tr>
        <td colspan="3">likelihood (tingkat keseringan kemungkinan yang terjadi) dan Saverity (Keparahan dari Resiko)</td>
    </tr>
    <tr>
        <td>Li</td>
        <td >Likelihood</td>
        <td>(L : tidak pernah; M: Pernah terjadi; H: Sering terjadi)</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>(L : tidak mungkin; M: mungkin terjadi; H: sangat mungkin terjadi)</td>
    </tr>
    <tr>
        <td>S</td>
        <td>Severity</td>
        <td>(L : tidak menimbulkan efek mutu; M: menimbulkan tapi tidak significant; H: significant)</td>
    </tr>
    <tr>
        <td colspan="3">Nilai</td>
    </tr>
    <tr>
        <td></td>
        <td>Low</td>
        <td>Pegendalian dengan IK cukup</td>
    </tr>
    <tr>
        <td></td>
        <td>Mid</td>
        <td>Pegendalian dengan IK + monitoring</td>
    </tr>
    <tr>
        <td></td>
        <td>High</td>
        <td>Dibuatkan program</td>
    </tr>
</table>

<table>
<tr>
    <td>
        <table style="margin-top: 30px;" border="1" >
            <tr class="text-center" >
                <td>Auditor</td>
                <td>Auditee</td>
                <td>Verifikasi Auditor</td>
                <td>TTD</td>

            </tr>
            <tr>
                <td style="padding:70px"></td>
                <td style="padding:70px"></td>
                <td style="padding:70px"></td>
                <td style="padding:70px"></td>
            </tr>
            <tr>
                <td  class="nama_ttd">{{ $ketua->nama_dosen }}</td>
                <td  class="nama_ttd">{{ $auditee->nama_dosen }}</td>
                <td  class="nama_ttd">{{ $ketua->nama_dosen }}</td>
                <td rowspan="2">Tgl: </td>
            </tr>
            <tr>
                <td  class="nama_ttd">{{ $ketua->nidn }}</td>
                <td class="nama_ttd">{{ $auditee->nidn }}</td>
                <td  class="nama_ttd">{{ $ketua->nidn }}</td>
            </tr>
        </table>
    </td>
    <td width="50px">

    </td>
    <td >
        <table>
            <tr>
                <td style="padding: 0 10px 0 10px;" style="border:0">Likelihood</td>
                <td ><img src="{{ asset('assets/images/media/likelihood.png') }}" width="250px"></td>
            </tr>
            <tr>
                <td colspan="2" class="text-center" style="border:0 solid #fff">Severity</td>
            </tr>
        </table>
    </td>
</tr>
</table>
</body>
</html>
