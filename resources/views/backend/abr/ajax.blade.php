$( ".submit-input-tindakan" ).click(function() {
	var dataString			= $("#frmAdan-ubah").serializeArray();
	var url					= "{{ url($url_admin.'/abr') }}/"+$("#id").val();
	goAjax(url, dataString);
});

$( ".submit-unggah" ).click(function() {
	$('#frmAdan').submit();
});

$('#frmAdan').on('submit', function(event){
	event.preventDefault();
	$.ajax({
	url:"{{ url($url_admin.'/abr/unggah') }}",
	method:"POST",
	enctype: "multipart/form-data",
	data:new FormData(this),
	dataType:'JSON',
	contentType: false,
	cache: false,
	processData: false,
	beforeSend: function(){
				sebelumKirim();
		},
		success: function(data){
				successMsg(data);
		},
		error: function(x, e){
			//	errorMsg(x.status);
		}
	})
});
