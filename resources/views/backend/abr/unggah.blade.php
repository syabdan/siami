<link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" />

{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
<div class="row">
	<div class="col-md-12">

		{{-- <div class="form-group row">
			{!! Form::label('Dokumen ABR ditandatangani', 'Dokumen ABR ditandatangani', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-12">
			{!! Form::file('pic', array('id' => 'pic', 'class' => 'dropify', 'data-height' => '300')) !!}
			</div>
		</div> --}}
        <h4>Dokumen ABR ditandatangani</h4><br>
        <div class="form-group">
            <label class="form-label">Bagikan link file Google Drive</label>
            <input type="text" class="form-control" name="urlfile" id="urlfile" placeholder="https://drive.google.com/file/d/1prmuzkpeRoD7UeqTvQSmWV__yAhjp96C/view?usp=sharing">
        </div>
		{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
		<div class="form-group row">
			<div class="col-md-12">
				<span class="pesan"></span>
			</div>
		</div>

	</div>
</div>

{!! Form::close() !!}
<script src="{{ URL::asset('adan/abr/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/fileupload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/file-upload.js') }}"></script>
