<link href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/wysiwyag/richtext.css')}}" rel="stylesheet">
{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}


        <div class="form-group row">
            {!! Form::label('Kepada', 'Kepada', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-12">
            {!! Form::select('to', ['Auditor'=>'Auditor', 'Prodi'=>'Prodi'], NULL, array('id' => 'to', 'class' => 'form-control select2','placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
            </div>
        </div>

		<div class="form-group row">
        {!! Form::label('Pesan', 'Pesan', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-12">
                <textarea name="pesan" id="pesan" class="" cols="30" rows="3" ></textarea>
            </div>
        </div>

        <span id="total-characters"></span>

        <span style="color: red"><i> Maksimal 250 karakter. Jika lebih, pesan akan terpotong</i></span>
	    {!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

        <div class="row">
            <div class="col-md-12">
                <span class="pesan"></span>
            </div>
        </div>
{!! Form::close() !!}



<script src="{{ URL::asset('adan/periode/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/wysiwyag/jquery.richtext.js') }}"></script>
<script>
    $('.select2').select2();

    $('#pesan').richText();
</script>
