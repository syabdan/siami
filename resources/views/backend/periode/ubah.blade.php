<link href="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
	<div class="form-group row">
		{!! Form::label('Nama', 'Nama', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			{!! Form::text('nama_periode', $data->nama_periode, array('id' => 'nama_periode', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. Periode 2020')) !!}
			</div>
		</div>

		<div class="form-group row">
			<label class="col-md-4 control-label">Tanggal Awal
				<span class="required"> * </span>
			</label>
			<div class="col-md-12">
				<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
					<input class="form-control" type="text" value="{{ $data->tanggal_awal }}" name="tanggal_awal">
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Tanggal Akhir
				<span class="required"> * </span>
			</label>
			<div class="col-md-12">
				<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
					<input class="form-control" type="text" value="{{ $data->tanggal_akhir }}" name="tanggal_akhir">
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-md-4 control-label">Batas Pengisian Instrumen
				<span class="required"> * </span>
			</label>
			<div class="col-md-12">
				<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
					<input class="form-control" type="text" value="{{ $data->batas_isi_prodi }}" name="batas_isi_prodi">
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
				</div>
			</div>
		</div>


	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/periode/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
