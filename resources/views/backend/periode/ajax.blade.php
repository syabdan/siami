$(document).on("click",".submit-kirim-pesan",function() {
	var dataString			= $('#frmAdan').serializeArray();
	var url					= "{{ url($url_admin.'/periode/kirim_pesan') }}";
	goAjax(url, dataString);
});

$(document).on("click",".submit-tambah",function() {
	var dataString			= $('#frmAdan').serializeArray();
	var url					= "{{ url($url_admin.'/periode') }}";
	goAjax(url, dataString);
});

$( ".submit-ubah" ).click(function() {
	var dataString			= $("#frmAdan-ubah").serializeArray();
	var url					= "{{ url($url_admin.'/periode') }}/"+$("#id").val();
	goAjax(url, dataString);
});

$( ".submit-hapus" ).click(function() {
	var url					= "{{ url($url_admin.'/periode') }}/"+$("#id").val();
	var dataString			= $('#frmAdan').serializeArray();
	goAjax(url, dataString);
});

