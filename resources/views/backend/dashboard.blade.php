@extends('layouts.master')
@section('css')
<link href="{{ URL::asset('assets/plugins/morris/morris.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<style>
    p {
        margin-top: 1em;
    }
</style>
@endsection
@section('page-header')
<!-- PAGE-HEADER -->
    <div>
        <h1 class="page-title">Dashboard @if(\Auth::user()->permission_id == 3) {{ auditee() }} @endif</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
        </ol>
    </div>
<!-- PAGE-HEADER END -->
@endsection
@section('content')

@if(\Auth::user()->permission_id == 3 || \Auth::user()->permission_id == 4)

    <!-- ROW-1 NCR -->
    <div class="row row-cards">
        <div class="col-sm-12 col-lg-12 col-md-12 col-xl-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="text-center">Mayor</h4>
                    <div class="mt-3 text-center">
                        <div class="chart-circle mx-auto chart-dropshadow-warning" data-value="0.80" data-thickness="7" data-color="#ffb61e"><div class="chart-circle-value"><div class="font-weight-normal fs-20">{{ $mayor_persen[\Auth::user()->unit_id] ?? null }}</div></div></div>
                        <p class="mb-0 mt-2">Persentase Mayor</p>
                    </div>
                    <ul class="list-items mt-2">
                        @if(\Auth::user()->permission_id == 3)
                        <li class="p-1">
                            <span class="list-label"></span>Jumlah Program Studi
                            <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_prodi }}</span>
                        </li>
                        @endif
                        <li class="p-1">
                            <span class="list-label"></span>Jumlah Instrumen
                            <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_instrumen }}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-12 col-md-12 col-xl-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="text-center">Minor</h4>
                    <div class="mt-3 text-center">
                        <div class="chart-circle mx-auto chart-dropshadow-info" data-value="0.80" data-thickness="7" data-color="#48929b"><div class="chart-circle-value"><div class="font-weight-normal fs-20">{{  $minor_persen[\Auth::user()->unit_id] ?? null }}</div></div></div>
                        <p class="mb-0 mt-2">Persentase Minor</p>
                    </div>
                    <ul class="list-items mt-2">
                        @if(\Auth::user()->permission_id == 3)
                        <li class="p-1">
                            <span class="list-label"></span>Jumlah Program Studi
                            <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_prodi }}</span>
                        </li>
                        @endif
                        <li class="p-1">
                            <span class="list-label"></span>Jumlah Instrumen
                            <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_instrumen }}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-12 col-md-12 col-xl-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="text-center">Sesuai</h4>
                    <div class="mt-3 text-center">
                        <div class="chart-circle mx-auto chart-dropshadow-success" data-value="0.80" data-thickness="7" data-color="#1f4788"><div class="chart-circle-value"><div class="font-weight-normal fs-20">{{  $sesuai_persen[\Auth::user()->unit_id] ?? null }}</div></div></div>
                        <p class="mb-0 mt-2">Persentase Sesuai</p>
                    </div>
                    <ul class="list-items mt-2">
                        @if(\Auth::user()->permission_id == 3)
                        <li class="p-1">
                            <span class="list-label"></span>Jumlah Program Studi
                            <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_prodi }}</span>
                        </li>
                        @endif
                        <li class="p-1">
                            <span class="list-label"></span>Jumlah Instrumen
                            <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_instrumen }}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- ROW-1 CLOSED -->

    <!-- ROW-5 Monitoring ketidaksesuaian OPEN -->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Table Monitoring</h3>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover text-nowrap" id="table_prodi" style="width: 100%">
                            <thead class="text-center">
                                <tr>
                                    <th style="padding-bottom: 2em">No</th>
                                    <th style="padding-bottom: 2em">Periode</th>
                                    <th style="padding-bottom: 2em">Progres Pengisian (%)</th>
                                    <th style="padding-bottom: 2em">Unggah Dokumen (%)</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div><!-- COL END -->
    </div>
    <!-- ROW-5 Monitoring ketidaksesuaian CLOSED -->

    <div class="row row-cards">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xl-4">
            <div class="card overflow-hidden">
                <div class="card-header">
                    <h3 class="card-title">Persentase Audit Berbasis Risiko (ABR)</h3>
                </div>
                <div class="card-body">
                    <div id="abr_unit" class="h-270 donutShadow"></div>
                </div>
            </div>
        </div><!-- COL END -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xl-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Chart NCR Berdasarkan Instrumen</h3>
                </div>
                <div class="card-body">
                    <div id="chart-bar-stacked" class="chartsh"></div>
                </div>
            </div>
        </div>
    </div>

@endif

@if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2)

    <!-- ROW-5 Monitoring ketidaksesuaian OPEN -->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Table Monitoring</h3>
                </div>
                <div class="form-group">
                    <div class="col-md-3 mt-5 text-center">
                    {!! Form::select('periode', $pilihPeriode, NULL, array('id' => 'periode', 'class' => 'form-control select2', 'placeholder' => 'Pilih Periode', 'style' => 'width:100%')) !!}
                    </div>
                    <span class="col-md-3"><i>NB* Jika Anda Ingin Melihat Periode Sebelumnya</i></span>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover text-nowrap" id="table" style="width: 100%">
                            <thead class="text-center">
                                <tr>
                                    <th rowspan="2" style="padding-bottom: 2em">No</th>
                                    <th rowspan="2" style="padding-bottom: 2em">Nama Program Studi</th>
                                    <th rowspan="2" style="padding-bottom: 2em">Progres Pengisian (%)</th>
                                    <th rowspan="2" style="padding-bottom: 2em">Unggah Dokumen (%)</th>
                                    <th colspan="3" class="text-center">Ketidaksesuaian (NCR)</th>
                                    <th colspan="3">Risk Assessment</th>
                                </tr>
                                <tr>
                                    <th>Mayor </th>
                                    <th>Minor </th>
                                    <th>Sesuai </th>
                                    <th>High (%)</th>
                                    <th>Medium (%)</th>
                                    <th>Low (%)</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div><!-- COL END -->



    </div>
    <!-- ROW-5 Monitoring ketidaksesuaian CLOSED -->

    <!-- ROW spider standar OPEN -->
    <div class="row row-cards">
        <div class="card">
            <div class="card-body">
                <h5 class="page-title">Grafik NCR per Program Studi</h5>
            </div>
        </div>
        @foreach($units2 as $unit)
        <div class="col-xs-12 col-sm-12 col-lg-4 col-md-6">
            <div class="card">
                <div id="standar_{{ $unit->id }}" ></div>
            </div>
        </div><!-- COL END -->
        @endforeach
    </div>
    {{-- {!! $units->links('pagination::bootstrap-4') !!} --}}

    <!-- ROW spider standar CLOSED -->


    <!-- ROW Bar NCR OPEN -->
    {{-- <div class="row row-cards mt-5">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Chart NCR Program Studi</h3>
                </div>
                <div class="card-body">
                    <div id="chart-bar-ncr" class="chartsh"></div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- ROW Bar NCR CLOSED -->


     <!-- ROW donut ncr OPEN -->
    <div class="row row-cards mt-5">
        <div class="card">
            <div class="card-body">
                <h5 class="page-title">Grafik ABR per Program Studi</h5>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                    <!-- ROW spider standar OPEN -->
                    <div class="row">
                        @foreach($units2 as $unit)
                        <div class="col-xs-12 col-sm-12 col-lg-4 col-md-6">
                            <div class="card-header">
                                <h3 class="card-title">Persentase ABR {{ $unit->nama_unit }}</h3>
                            </div>
                            <div class="card-body">
                                <div id="abr_unit_{{ $unit->id }}" ></div>
                            </div>
                        </div><!-- COL END -->
                        @endforeach
                    </div>

                    {{-- {!! $units->render('pagination::bootstrap-4') !!} --}}
                    <!-- ROW abr assessment CLOSED -->
            </div>
        </div>
    </div>
     <!-- ROW donut ncr OPEN CLOSED-->

@endif






    </div>
</div>
<!-- CONTAINER CLOSED -->
</div>



@endsection
@section('js')

@if(\Auth::user()->permission_id == 3)
@if (!$unit)
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modalAudit').modal('show');
        });
    </script>
@elseif(!is_null($pilihUnitAuditee))
    @if (\Auth::user()->unit_id === NULL)
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modalPilih_auditee').modal('show');
        });
    </script>
    @else
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modalPilih_auditee').modal('hide');
        });
    </script>
    @endif


@endif

@endif
<script src="{{ URL::asset('assets/plugins/morris/raphael-min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/morris/morris.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/jquery.dataTables.min.js') }}" ></script>
<script src="{{ URL::asset('assets/plugins/datatable/dataTables.bootstrap4.min.js') }}" ></script>
<script src="{{ URL::asset('assets/plugins/datatable/datatable.js') }}" ></script>
{{-- <script src="{{ URL::asset('assets/plugins/datatable/datatable-2.js') }}" ></script> --}}
<script src="{{ URL::asset('assets/plugins/datatable/dataTables.responsive.min.js') }}" ></script>

<!-- Jquery DataTable Plugin Js -->
{{-- <script src="{{ URL::asset('assets/plugins/datatable/datatablescripts.bundle.js')}}"></script> --}}
<script src="{{ URL::asset('assets/plugins/datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/buttons/buttons.print.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/buttons/buttons.jszip.min.js')}}"></script>

    @if(\Auth::user()->permission_id == 3 || \Auth::user()->permission_id == 4)
    @include('backend.chart.script_unit')
    @endif

@if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2)
    <script src="{{ URL::to('/') }}/assets/plugins/highchart/highcharts.js" ></script>
    <script src="{{ URL::to('/') }}/assets/plugins/highchart/highcharts-more.js" ></script>
    <script src="{{ URL::to('/') }}/assets/plugins/highchart/exporting.js" ></script>
    <script src="{{ URL::to('/') }}/assets/plugins/highchart/export-data.js" ></script>
    <script src="{{ URL::to('/') }}/assets/plugins/highchart/accessibility.js" ></script>
    @include('backend.chart.script_all')
@endif
@if(\Auth::user()->permission_id == 3)
    @include('backend.auditor.warning')
@endif


@endsection

