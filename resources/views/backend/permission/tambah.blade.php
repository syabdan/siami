<link href="{{ URL::asset('assets/plugins/multipleselect/multiple-select.css')}}" rel="stylesheet" />
{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}
@csrf
		<div class="form-group row">
		{!! Form::label('Nama Permission', 'Nama Permission', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			{!! Form::text('permission_name', NULL, array('id' => 'permission_name', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. Root')) !!}
			</div>
		</div>

		<div class="form-group row">
			<label class="col-md-4 control-label">Menu
			</label>
			<div class="col-md-12 form-group">
				<select class="optmulti-select" name="menu[]" multiple="multiple">
						@foreach($menus as $menu)
							<option value="{{ $menu->id}}">{{ $menu->nama }}</option>
						@endforeach
				</select>
			</div>
		</div>
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/permission/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::asset('assets/plugins/multipleselect/multiple-select.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/multipleselect/multi-select.js') }}"></script>

