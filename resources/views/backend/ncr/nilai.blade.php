{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
    <div class="form-group row">
    {!! Form::label('Input Nilai Auditor', 'Input Nilai Auditor', array('class' => 'col-md-4 control-label')) !!}
        <div class="col-md-12">
            {!! Form::select('nilai_auditor', ['0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4'], $data->nilai_auditor, array('id' => 'nilai_auditor', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
        </div>
    </div>


	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/ncr/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

