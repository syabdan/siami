<link href="{{ URL::asset('assets/plugins/wysiwyag/richtext.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" />
{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}

    <center>
        <h4>Anda yakin telah mengkoreksi NCR ini ?</h4>
        <h6>Jika iya, silahkan klik tombol Konfirmasi dibawah ini !</h6>
    <hr>
    </center>
    {{-- <div class="form-group row mt-5">
        {!! Form::label('Unggah File Bukti Konfrimasi (Jika ada)', 'Unggah File Bukti Konfrimasi (Jika ada)', array('class' => 'col-md-12 form-label')) !!}
        <div class="col-md-12">
            {!! Form::file('file_konfirmasi', array('id' => 'file_konfirmasi', 'class' => 'dropify', 'data-height' => '300', 'required' => true)) !!}
        </div>
    </div> --}}
    <div class="form-group">
        <label class="form-label">Bagikan link file Google Drive</label>
        <input type="text" class="form-control" name="urlfile_konfirmasi" id="urlfile_konfirmasi" placeholder="https://drive.google.com/file/d/1prmuzkpeRoD7UeqTvQSmWV__yAhjp96C/view?usp=sharing">
    </div>

	{!! Form::hidden('tgl_tindakan_koreksi', date('Y-m-d'), array('id' => 'tgl_tindakan_koreksi')) !!}
	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
    <div class="col-md-12">
        <span class="pesan"></span>
    </div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/ncr/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::asset('assets/plugins/wysiwyag/jquery.richtext.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/fileupload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/file-upload.js') }}"></script>
<script>
    $('.content').richText();
</script>
