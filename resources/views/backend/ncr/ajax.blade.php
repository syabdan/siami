

$( ".submit-input-catatan" ).click(function() {
	var dataString			= $("#frmAdan-ubah").serializeArray();
	var url					= "{{ url($url_admin.'/ncr') }}/"+$("#id").val();
	goAjax(url, dataString);
});

$( ".submit-input-faktor-penyebab" ).click(function() {
	var dataString			= $("#frmAdan-ubah").serializeArray();
	var url					= "{{ url($url_admin.'/ncr') }}/"+$("#id").val();
	goAjax(url, dataString);
});

$( ".submit-input-tindakan-koreksi" ).click(function() {
	var dataString			= $("#frmAdan-ubah").serializeArray();
	var url					= "{{ url($url_admin.'/ncr') }}/"+$("#id").val();
	goAjax(url, dataString);
});

$( ".submit-input-tindakan-pencegahan" ).click(function() {
	var dataString			= $("#frmAdan-ubah").serializeArray();
	var url					= "{{ url($url_admin.'/ncr') }}/"+$("#id").val();
	goAjax(url, dataString);
});

$( ".submit-konfirmasi" ).click(function() {
    $("#frmAdan-ubah").submit();
});

$( ".submit-reupload" ).click(function() {
    $("#frmAdan-ubah").submit();
});

$( ".submit-verifikasi" ).click(function() {
	var dataString			= $("#frmAdan-ubah").serializeArray();
	var url					= "{{ url($url_admin.'/ncr') }}/"+$("#id").val();
	goAjax(url, dataString);
});

$( ".submit-catatan-verifikasi" ).click(function() {
	var dataString			= $("#frmAdan-ubah").serializeArray();
	var url					= "{{ url($url_admin.'/ncr') }}/"+$("#id").val();
	goAjax(url, dataString);
});

$( ".submit-input-nilai" ).click(function() {
	var dataString			= $("#frmAdan-ubah").serializeArray();
	var url					= "{{ url($url_admin.'/ncr') }}/"+$("#id").val();
	goAjax(url, dataString);
});

$( ".submit-unggah" ).click(function() {
    $("#frmAdan-unggah").submit();
});


$('#frmAdan-unggah').on('submit', function(event){
	event.preventDefault();
	$.ajax({
	url:"{{ url($url_admin.'/ncr/unggah_file') }}/"+$("#id").val(),
	method:"POST",
	enctype: "multipart/form-data",
	data:new FormData(this),
	dataType:'JSON',
	contentType: false,
	cache: false,
	processData: false,
	beforeSend: function(){
				sebelumKirim();
		},
		success: function(data){
				successMsg(data);
		},
		error: function(x, e){
			//	errorMsg(x.status);
		}
	})
});

$('#frmAdan-ubah').on('submit', function(event){
	event.preventDefault();
	$.ajax({
	url: "{{ url($url_admin.'/ncr') }}/"+$("#id").val(),
	method:"POST",
	enctype: "multipart/form-data",
	data:new FormData(this),
	dataType:'JSON',
	contentType: false,
	cache: false,
	processData: false,
	beforeSend: function(){
				sebelumKirim();
		},
		success: function(data){
				successMsg(data);
		},
		error: function(x, e){
			//	errorMsg(x.status);
		}
	})
});

