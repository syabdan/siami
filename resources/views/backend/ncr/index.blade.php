@extends('layouts.master')
@section('css')
<link href="{{ URL::asset('assets/plugins/datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/date-picker/spectrum.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/multipleselect/multiple-select.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/time-picker/jquery.timepicker.css')}}" rel="stylesheet" />
@endsection

@section('page-header')
<!-- PAGE-HEADER -->
    <div>
        <h1 class="page-title">Laporan Ketidaksesuaian (NCR)</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active"><a href="#">Laporan Ketidaksesuaian (NCR)</a></li>
        </ol>
    </div>
<!-- PAGE-HEADER END -->
@endsection
@section('content')
<!-- ROW-1 OPEN -->
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row col-md-12">
                    <div class="col-md-10">
                        <h3 class="card-title">Laporan Ketidaksesuaian (NCR) {{ $periodeTerpilih->nama_periode }} @if(\Auth::user()->permission_id != 4) {{ auditee() ?? null }} @endif {!! ( periode() ?? null) ? periode() : '' !!}</h3>
                        @if(\Auth::user()->permission_id == 3 || \Auth::user()->permission_id == 4)
                        <div class="form-group row">
                            <div class="col-md-6 mt-5 text-right">
                            {!! Form::select('periode', $pilihPeriode, NULL, array('id' => 'periode', 'class' => 'form-control select2', 'placeholder' => 'Pilih Periode', 'style' => 'width:100%')) !!}
                            </div>
                            <span class="col-md-6"><i>NB* Jika Anda Ingin Melihat Periode Sebelumnya</i></span>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-2 col-sm-2 col-2 text-right">
                        @if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2 || \Auth::user()->permission_id == 3)
                        <div class="btn-group">
                            <a href="{{ url('ncr/cetak/'.$id.'/'. ( \Request::segment(4) ?? null)) }}" target="blank"  class="btn btn-info btn-sm cetak"><i class="fa fa-download"></i> Cetak NCR </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table id="table" class="table table-striped table-bordered text-nowrap w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                @if(\Auth::user()->permission_id == 3 || \Auth::user()->permission_id == 4)
                                    <th>Aksi</th>
                                @endif
                                <th>Dokumen Instrumen</th>
                                <th>Ketidaksesuaian</th>
                                <th>Temuan</th>
                                <th>Uraian Faktor Penyebab</th>
                                <th>Tindakan Koreksi</th>
                                <th>Tindakan Pencegahan</th>
                                <th>Nilai Prodi</th>
                                <th>Nilai Auditor</th>
                                <th>Konfirmasi Koreksi</th>
                                <th>Verifikasi</th>
                                <th>NCR Ditandatangani</th>

                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($djawaban as $jawab)
                            @php
                                ($jawab->tgl_verifikasi !== NULL) ? $disable = 'disabled' : $disable='';
                            @endphp

                            <tr>
                                <td>{{  $no }}</td>
                                @if(\Auth::user()->permission_id == 3 || \Auth::user()->permission_id == 4)
                                <td>
                                    @if(\Auth::user()->permission_id == 3)

                                        @if($jawab->tgl_tindakan_koreksi !== NULL && $jawab->tgl_verifikasi == NULL)
                                            <a href="#" data-id="{{ $jawab->id }}"  class="btn btn-outline-danger btn-sm verifikasi {{ $disable }} blink"><i class="ti-clipboard"></i> Verifikasi </a>
                                        @endif
                                        <a href='#' data-id="{{ $jawab->id }}" class='btn btn-outline-primary btn-sm nilai {{ $disable }}'> <i class='ti-receipt'></i> Input Nilai</a>

                                        @if($jawab->nilai_auditor != 4)
                                            <a href="#" data-id="{{ $jawab->id }}"  class="btn btn-outline-secondary btn-sm tindakan {{ $disable }}"><i class="fa fa-pencil"></i> Isi Tindakan </a>
                                            <a href="#" data-id="{{ $jawab->id }}"  class="btn btn-outline-success btn-sm unggah {{ $disable }} "><i class="fa fa-upload"></i> Unggah </a>
                                        @endif
                                    @endif

                                    @if(\Auth::user()->permission_id == 4 && $jawab->tgl_verifikasi === NULL)
                                        @if($jawab->temuan != "Sesuai")
                                            <a href="#" data-id="{{ $jawab->id }}"  class="btn btn-outline-secondary btn-sm prodikoreksi"><i class="ti-files"></i> Konfirmasi </a>
                                        @endif
                                    @endif

                                </td>
                                @endif

                                <td>

                                      {{-- {!!  ($jawab->file_jawaban) ? "<a href=".url('/ncr/ambil_file/'.$jawab->file_jawaban)." target='blank' class='btn btn-outline-primary btn-sm'> <i class='fa fa-file'></i> Lihat Dokumen</a>" :  "<span> Belum Ada Dokumen</span>" !!} --}}
                                      {!!  ($jawab->urlfile) ? "<a href=".$jawab->urlfile." target='blank' class='btn btn-outline-primary btn-sm'> <i class='fa fa-file'></i> Lihat Dokumen</a>" :  "<span> Belum Ada Dokumen</span>" !!}

                                      <a href="#" data-id="{{ $jawab->id }}"  class="btn btn-outline-warning btn-sm reupload {{ $disable }}" ><i class="ti-upload"></i> reupload </a>
                                </td>
                                <td><div style='border:1px solid #ccc; padding:10px; max-height:200px; max-width:500px; overflow:scroll;'>{!!  $jawab->uraian !!}</div></td>
                                <td>{{  $jawab->temuan }}</td>

                                    <td><div style='border:1px solid #ccc; padding:10px; max-height:100px; max-width:500px; overflow:scroll;'>{!!  $jawab->catatan_penyebab !!}</div></td>
                                    <td><div style='border:1px solid #ccc; padding:10px; max-height:100px; max-width:500px; overflow:scroll;'>{!!  $jawab->catatan_koreksi !!}</div></td>
                                    <td><div style='border:1px solid #ccc; padding:10px; max-height:100px; max-width:500px; overflow:scroll;'>{!!  $jawab->catatan_pencegahan !!}</div></td>

                                <td>{!!  ($jawab->nilai) ?? "<span> Belum Ada Nilai</span>" !!}</td>
                                <td>{!!  (($jawab->nilai_auditor ) ?? "<span> Belum Ada Nilai</span>")  !!}</td>
                                <td>@if($jawab->tgl_tindakan_koreksi)
                                    <span>{{ tanggal_indonesia($jawab->tgl_tindakan_koreksi) }}<br>
                                        {{-- <a href="{!! url('ncr/ambil_file_konfirmasi/'.$jawab->file_konfirmasi) !!}" target="blank" class="btn btn-outline-primary btn-sm"> <i class="fa fa-file"></i> Lihat Dokumen</a></span> --}}
                                        <a href="{!! $jawab->urlfile_konfirmasi !!}" target='blank' class='btn btn-outline-primary btn-sm '> <i class="fa fa-file"></i> Lihat Dokumen</a></span>

                                        {{-- untuk koreksi kembali --}}
                                        @if (\Auth::user()->permission_id == 3)
                                            <a href="#" data-id="{{ $jawab->id }}"  class="btn btn-outline-danger btn-sm catatan-verifikasi {{ $disable }}"><i class="fa fa-pencil"></i> Catatan Verifikasi</a>
                                        @endif
                                        {{-- untuk koreksi kembali --}}

                                    @else
                                        <span> Belum Dikonfirmasi </span>
                                    @endif
                                </td>
                                <td>{!!  ($jawab->tgl_verifikasi) ? tanggal_indonesia($jawab->tgl_verifikasi) : "<span> Belum Diverifikasi </span>" !!} {!!  ($jawab->catatan_verifikasi) ? "<br><div style='border:1px solid #ccc; padding:10px; max-height:100px; max-width:500px; overflow:scroll;'><span style='color:red'> Catatan: ".$jawab->catatan_verifikasi."</span></div>" : "" !!}</td>
                                <td>
                                    {{-- {!!  ($jawab->file_koreksi) ? "<a href=".url('/ncr/ambil_file_ncr/'.$jawab->file_koreksi)." target='blank' class='btn btn-outline-primary btn-sm'> <i class='fa fa-file'></i> Lihat Dokumen</a>" :  "<span> Belum Ada Dokumen</span>" !!} --}}
                                    {!!  ($jawab->urlfile_koreksi) ? "<a href=".$jawab->urlfile_koreksi." target='blank' class='btn btn-outline-primary btn-sm'> <i class='fa fa-file'></i> Lihat Dokumen</a>"  :  "<span> Belum Ada Dokumen</span>" !!}

                                </td>

                            </tr>
                            @php
                                $no++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- TABLE WRAPPER -->
        </div>
        <!-- SECTION WRAPPER -->
    </div>
</div>
<!-- ROW-1 CLOSED -->

</div>
</div>
<!-- CONTAINER CLOSED -->
</div>



@endsection

@section('js')
<script src="{{ URL::asset('assets/plugins/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/datatable.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/datatable-2.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::to('/') }}/adan/ncr/jquery.js"></script>

<script>
$(document).ready(function(){
    $('#periode').on('change', function() {
        window.location = "{{ url($url_admin.'/ncr/standar/') }}/"+{{ Request::segment(3) }}+'/'+$(this).val();
    });
});
</script>

@if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2)
    @include('backend.auditor.warning')
@endif

@if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2)
    @if (\Auth::user()->unit_id === NULL)
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modalPilih_auditee').modal('show');
        });
    </script>
    @else
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modalPilih_auditee').modal('hide');
        });
    </script>
    @endif
@endif
@endsection
