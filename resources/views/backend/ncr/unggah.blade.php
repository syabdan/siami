<link href="{{ URL::asset('assets/plugins/wysiwyag/richtext.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" />
{!! Form::open(array('id' => 'frmAdan-unggah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}

{{-- <div class="form-group row">
    {!! Form::label('Unggah File NCR', 'Unggah File NCR', array('class' => 'col-md-4 form-label')) !!}
    <div class="col-md-12">
        {!! Form::file('file_koreksi', array('id' => 'file_koreksi', 'class' => 'dropify', 'data-height' => '300')) !!}
    </div>
</div> --}}

    <div class="form-group">
        <label class="form-label">Bagikan link file Google Drive</label>
        <input type="text" class="form-control" name="urlfile_koreksi" id="urlfile_koreksi" placeholder="https://drive.google.com/file/d/1prmuzkpeRoD7UeqTvQSmWV__yAhjp96C/view?usp=sharing">
    </div>

	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
    <div class="col-md-12">
        <span class="pesan"></span>
    </div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/ncr/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/fileupload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/file-upload.js') }}"></script>

