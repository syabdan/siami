$(document).ready(function(){

	$(document).on("click",".tindakan",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/ncr/tindakan') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Input Catatan',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".penyebab",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/ncr/penyebab') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Input Faktor Penyebab',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".prodikoreksi",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/ncr/prodikoreksi') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Konfirmasi',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".reupload",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/ncr/reupload') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Reupload',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".verifikasi",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/ncr/verifikasi') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Verifikasi',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".catatan-verifikasi",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/ncr/catatan-verifikasi') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Input Catatan',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".pencegahan",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/ncr/pencegahan') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Input Tindakan Pencegahan',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".unggah",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/ncr/unggah') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Unggah',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".unduh",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/ncr/unduh') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Unduh',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".nilai",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/ncr/nilai') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Input Nilai',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});


});
