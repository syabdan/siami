<link href="{{ URL::asset('assets/plugins/wysiwyag/richtext.css')}}" rel="stylesheet">
{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}

    <center>
        <h4>Anda yakin ingin memverifikasi NCR ini ?</h4>
        <h6>Jika iya, silahkan klik tombol Verifikasi dibawah ini !</h6>
    </center>

    <input type="hidden" name="tgl_verifikasi" value="{{ date('Y-m-d') }}">

	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
    <div class="col-md-12">
        <span class="pesan"></span>
    </div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/ncr/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::asset('assets/plugins/wysiwyag/jquery.richtext.js') }}"></script>

<script>
    $('.content').richText();
</script>
