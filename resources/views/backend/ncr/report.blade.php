<style>
    body {
        font-family: tahoma, sans-serif;
        font-size: 10pt;
    }

    table {
        border-collapse: collapse;
    }

    .ncr-header
    {
        text-align: center;
        font-size: 100%;
        font-weight: bold;
        width: 350px;

    }

    .title2 {
        font-size: 18px;
        text-align: center;

    }

    body .tablebody{
        font-size: 12px;
    }

    .label {
        width: 5%;
        padding: 5px 0 1px 2px;
    }

    .isi {
        font-size: 11px;
    }

    .label2 {
        font-size: 12px;
        width: 20%;
        padding: 5px 0 1px 5px;
        margin-top: 0px;
        border-bottom: 1px solid #fff;
    }

    .label3 {
        font-size: 12px;
        padding: 5px 0 100px 35px;
    }
    .label4 {

        padding: 5px 0 0px 5px;
    }
    .label5 {
        padding: 5px 0 100px 120px;
    }

    .label-header {
        font-size: 10pt;
        padding-left: 5px;
    }

    .isi2 {
        font-size: 12px;
        width: 20%;
        padding: 10px 0 10px 5px;
        border-top: 1px solid #fff;
        max-width: 350px;
    }

    .isitemuan {

        font-size: 12px;
        padding: 20px 0px 15px 10px;
        border-top: 1px solid #fff;
    }

    .break { page-break-after: always; }

</style>

@php
    $no = 1;
    $total = $datas->count();
@endphp
@foreach ($datas as $data)
@php
    $url_ikon = "asset('assets/images/pngs/check_icon.png')";
@endphp
<table width="100%" border="1" style="border:1px solid #000;tr:margin-top:-25px" >
    <tbody>
      <tr>
        <td rowspan="3" width="80"><center><img src="{{ asset('assets/images/brand/uir.png') }}" width="80%"></center></td>
        <td class="title2">FORMULIR</td>
        <td class="label-header">No Dokumen</td>
        <td class="label-header">: UIR/LPM/F/05.02</td>
      </tr>
      <tr>
        <td class="ncr-header" rowspan="2">LAPORAN KETIDAKSESUAIAN (NCR)</td>
        <td class="label-header">Tanggal Terbit</td>
        <td class="label-header">: 10 Maret 2019</td>
      </tr>
      <tr>
        <td class="label-header">No. Revisi</td>
        <td class="label-header">: 00</td>
      </tr>
    </tbody>
</table>
<br><br>
<br><br>

<table border="1" width="100%" class="tablebody">
    <tr>
        <td class="label">No. NCR</td>
        <td class="isi">: {{ $no }}</td>

        <td class="label">Tanggal</td>
        <td class="isi">: {{ tanggal_indonesia(date('Y-m-d'), false) }}</td>
    </tr>
    <tr>
        <td class="label">Klausul/Dokumen</td>
        <td class="isi">: {{ $data->standar_id }}.{{ $sub[$no-1] }}</td>

        <td class="label">Divisi/Lokasi</td>
        <td class="isi">: {{ $data->nama_unit }}</td>
    </tr>
    <tr>
        <td class="label">Auditor</td>
        <td class="isi">:
            @php
            $i=1;
            @endphp
            @foreach ($auditors as $auditor)
                {{ $auditor->nama_dosen }} @if($auditors->count() > $i) / @endif
                @php
                    $i++
                @endphp
            @endforeach
            </td>

        <td class="label">Auditee</td>
        <td class="isi">: {{ $data->nama_unit }}</td>
    </tr>


    <tr >
        <td colspan="2" class="label2">
            URAIAN KETIDAKSESUAIAN
        </td>
        <td colspan="2" class="label2">
            KATEGORI TEMUAN
        </td>
    </tr>
    <tr >
        <td colspan="2" max-width="350px" class="isi2">
            {!! $data->uraian !!}
        </td>
        <td colspan="2"  class="isi2">
            <table>
                <tr>
                    <td>
                        <ul>
                            <li class="isitemuan"> MAYOR </li>
                        </ul>
                    </td>
                    <td>
                        @if($data->temuan == 'Mayor') <img src="{{ asset('assets/images/pngs/check_icon.png') }}"  style="padding:0 0 0 10px;" width="20px" > @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul>
                            <li class="isitemuan"> MINOR </li>
                        </ul>
                    </td>
                    <td>
                        @if($data->temuan == 'Minor') <img src="{{ asset('assets/images/pngs/check_icon.png') }}"  style="margin:0 0 0 10px;" width="20px" > @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>


    <tr >
        <td colspan="2" class="label2">
            URAIAN FAKTOR PENYEBAB KETIDAKSESUAIAN
        </td>
        <td colspan="2" class="label2">
            TINDAKAN KOREKSI
        </td>
    </tr>
    <tr >
        <td colspan="2" class="isi2">
           {!! $data->catatan_penyebab !!}
        </td>
        <td colspan="2" class="isi2">
           {!! $data->catatan_koreksi !!}
        </td>
    </tr>


    <tr >
        <td colspan="2" >
            <table width="100%">
                <tr>
                    <td width="50%" class="label3">TTD Auditor</td>
                    <td width="50%" class="label3">TTD Auditee</td>
                </tr>


                <tr>
                    <td width="50%" class="label2">Tanggal Mulai: </td>
                    <td width="50%" class="label2">Tanggal: {{ tanggal_indonesia(date('Y-m-d'), false) }}</td>
                </tr>
                <tr>
                    <td width="50%" class="label2">Tanggal Selesai: </td>
                </tr>
            </table>
        </td>
        <td colspan="2" >

          <table width="100%" style="margin-top: 0px">
            <tr >
                <td class="label2" >
                    <p >TINDAKAN PENCEGAHAN</p>
                </td>
            </tr>


            <tr>
                <td  class="isi2">
                    {!! $data->catatan_pencegahan !!}
                </td>
            </tr>

        </table>
        </td>
    </tr>


    <tr >
        <td colspan="2" class="label5">
            TTD Auditor
        </td>
        <td colspan="2"  class="label2" >
           <p style="margin-top: -60px">VERIFIKASI PELAKSANAAN TINDAKAN KOREKSI DAN PENCEGAHAN</p>
        </td>
    </tr>


    <tr>
        <td colspan="2">
            Tanggal:
        </td>
        <td colspan="2" class="isi2">
            @if($data->tgl_verifikasi === NULL)
           <center><p style="margin-top: -60px">Belum diverifikasi</p></center>
           @else
            Diverifikasi pada tanggal <b>{{ tanggal_indonesia($data->tgl_verifikasi, false) }}</b>
           @endif
        </td>
    </tr>

</table>



@if ($total > $no)
<div class="break"></div>
@endif
@php
    $no++;
@endphp
@endforeach
