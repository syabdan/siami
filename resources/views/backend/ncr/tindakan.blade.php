<link href="{{ URL::asset('assets/plugins/wysiwyag/richtext.css')}}" rel="stylesheet">
{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
    <div class="form-group row">
        {!! Form::label('Uraian Ketidaksesuaian', 'Uraian Ketidaksesuaian', array('class' => 'col-md-4 control-label')) !!}
        <div class="col-md-12">
            <textarea name="uraian" id="uraian" class="content" cols="30" rows="2" >{{ $data->uraian }}</textarea>
        </div>
    </div>
    <hr>

    <div class="form-group row">
        {!! Form::label('Uraian Faktor Penyebab', 'Uraian Faktor Penyebab', array('class' => 'col-md-4 control-label')) !!}
        <div class="col-md-12">
            <textarea name="catatan_penyebab"  id="catatan_penyebab" class="content" cols="30" rows="3" >{{ $data->catatan_penyebab }}</textarea>
        </div>
    </div>
    <hr>

    <div class="form-group row">
        {!! Form::label('Catatan Koreksi', 'Catatan Koreksi', array('class' => 'col-md-4 control-label')) !!}
        <div class="col-md-12">
            <textarea name="catatan_koreksi" id="catatan_koreksi" class="content" cols="30" rows="3" >{{ $data->catatan_koreksi }}</textarea>
        </div>
    </div>
    <hr>

    <div class="form-group row">
        {!! Form::label('Catatan Pencegahan', 'Catatan Pencegahan', array('class' => 'col-md-4 control-label')) !!}
        <div class="col-md-12">
            <textarea name="catatan_pencegahan" id="catatan_pencegahan" class="content" cols="30" rows="3" >{{ $data->catatan_pencegahan }}</textarea>
        </div>
    </div>
    <hr>

	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/ncr/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::asset('assets/plugins/wysiwyag/jquery.richtext.js') }}"></script>

<script>
    $('#uraian ').richText();
    $('#catatan_penyebab ').richText();
    $('#catatan_koreksi ').richText();
    $('#catatan_pencegahan ').richText();
</script>
