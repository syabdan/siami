@extends('layouts.master')
@section('css')
<link href="{{ URL::asset('assets/plugins/formwizard/smart_wizard.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/date-picker/spectrum.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/multipleselect/multiple-select.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/notify/css/jquery.growl.css')}}" rel="stylesheet">
<style>
    p {
        display: inline;
    }
</style>
@endsection

@section('page-header')
<!-- PAGE-HEADER -->
    <div>
        <h1 class="page-title">Instrumen</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Instrumen Audit Mutu Internal</li>
        </ol>
    </div>
<!-- PAGE-HEADER END -->
@endsection
@section('content')
<!-- ROW-1 OPEN -->
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row col-md-12">
                    <div class="col-md-10">
                        <h3 class="card-title">Instrumen AMI Prodi</h3>
                    </div>
                    <div class="col-md-2 col-sm-2 col-2 text-right">
                        <div class="btn-group">
                            <a class="btn btn-secondary-light btn-sm mr-2" id="button-publish-data">Publish <i class="ti-save"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">

                <div id="notif_publish"></div>

                <!-- ROW-2 OPEN -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card accordion-wizard">
                            <div class="card-header">
                                <h3 class="card-title">{{ $standar->nama_standar }} : {{ $standar->keterangan }}</h3>
                            </div>
                            <div class="card-body">

                                <form id="form" class="form-kriteria">
                                    <div class="list-group">
                                        @foreach ($pertanyaan->where('standar_id', $id) as $ask)
                                        {{-- {!! Form::open(['class' => 'form-kriteria', 'files' => true ]) !!} --}}

                                        <input type="hidden" name="pertanyaan_id[]" value="{{ $ask->id }}">
                                        {{-- {!! Form::close() !!} --}}

                                        <input type="hidden" name="isi_pertanyaan[]" value="{{ $ask->isi_pertanyaan }}">

                                        <div class="list-group-item py-4" data-acc-step>
                                            <h5 class="mb-0" data-acc-title >{!! $ask->isi_pertanyaan !!}</h5>
                                            <div data-acc-content>
                                                <div class="my-3 custom-controls-stacked">
                                                    @for($i=4;$i>=0;$i--)
                                                    <label class="custom-control custom-radio" style="cursor: pointer;">
                                                        <input type="radio" class="custom-control-input" name="jawaban_nilai_{{$ask->id}}" value="{{ $ask->pilihan[$i]['nilai'] }}">
                                                        <input type="hidden" class="custom-control-input" name="jawaban_isi_{{$ask->id}}" value="{{ $ask->pilihan[$i]['pilihan'] }}">
                                                        <span class="custom-control-label">{{ $ask->pilihan[$i]['pilihan'] }}</span>
                                                    </label>
                                                    @endfor
                                                    <div class="mt-5 mb-5">
                                                    @if ($ask->catatan)
                                                        {!! $ask->catatan !!}
                                                    @endif
                                                    </div>
                                                    @if ($ask->s_unggah == 1)
                                                    <label for="dokumen"></label>

                                                        <div class="row">
                                                            {{-- {!! Form::label('Dokumen yang telah di unggah', 'Dokumen yang telah di unggah', array('class' => 'col-md-4 form-label')) !!} --}}
                                                            <div class="col-md-3">
                                                                <label for="Dokumen yang telah di unggah">Dokumen yang telah di unggah</label>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <span id="dokumen_jawaban_{{$ask->id}}" > </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            {!! Form::label('Unggah Dokumen', 'Unggah Dokumen', array('class' => 'col-md-4 form-label')) !!}
                                                            <div class="col-md-12">
                                                                <input type="file" class="dropify" name="dokumen_{{$ask->id}}" id="dokumen_{{$ask->id}}" data-default-file=""  data-height="300" />
                                                            </div>
                                                        </div>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ROW-2 CLOSED -->

            </div>
            <!-- TABLE WRAPPER -->

        </div>
        <!-- SECTION WRAPPER -->
    </div>
</div>
<!-- ROW-1 CLOSED -->

</div>
</div>
<!-- CONTAINER CLOSED -->
</div>
@endsection

@section('js')
<script src="{{ URL::asset('assets/plugins/accordion-Wizard-Form/jquery.accordion-wizard.min.js') }}"></script>
{{-- <script src="{{ URL::to('/') }}/adan/instrumen/jquery.js"></script> --}}
<script src="{{ URL::asset('assets/plugins/fileuploads/js/fileupload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/file-upload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/notify/js/rainbow.js') }}"></script>
<script>
$(document).ready(function (){


    if({{ cekPeriodeProdi()  }} <= 0){
        $('#modalHabisPeriode').modal('show');
    }

    // ambil jawaban
    var _token = "{{ csrf_token() }}";
    $.ajax({
        type: "POST",
        url: "{{ url($url_admin.'/instrumen/ambil_pilihan') }}",
        data: { '_token': _token},
        dataType: 'json',
        cache: false,

            success: function(data){
                $.each(data, function(key, value)
                {
                    var url = "{{ url('instrumen/ambil_file') }}"+"/"+value.file_jawaban;
                    $("input[name=jawaban_nilai_"+value.pertanyaan_id+"][value=" + value.nilai + "]").prop('checked', true);
                    $("#dokumen_jawaban_"+value.pertanyaan_id).html("<a target='_blank' href="+url+">"+value.file_jawaban+"</a>");

                });
            }
        });
    // #ambil
});

    //accordion-wizard
	var options = {
		mode: 'wizard',
		autoButtonsNextClass: 'btn btn-primary float-right',
		autoButtonsPrevClass: 'btn btn-info',
		stepNumberClass: 'badge badge-pill badge-primary mr-1',
		onSubmit: function() {

        swal({
            title: 'Apakah anda yakin untuk menyimpan data ini ?',
			text: "Silahkan klik tombol 'Ya' untuk menyimpan data",
			type: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#d33',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Ya, Simpan',
			cancelButtonText: 'Tidak',
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
                if (isConfirm) {
                    var url = "{{ url('instrumen/simpan_jawaban') }}";
                    var form = $('#form')[0];
                    var formData = new FormData(form);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url : url,
                        type : "POST",
                        enctype: 'multipart/form-data',
                        data : formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        dataType : 'JSON',
                        success : function(data) {
                                swal({
                                type: "success",
                                title: 'Berhasil...',
                                text: 'Data telah disimpan..!',
                                showConfirmButton: false
                                });
                                setTimeout(function(){
                                    window.location.reload();
                                }, 1500);
                        },
                        error : function(){
                            alert('gagal');
                        }
                    });

                }
            });
		}
	}
    $( "#form" ).accWizard(options);





    $('#button-publish-data').on("click", function(){
        swal({
            title: 'Apakah anda yakin untuk mempublish data ini ?',
			text: "Setelah Anda mempublish, data tidak akan dapat diubah kembali ...!",
			type: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#d33',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Ya, Publish',
			cancelButtonText: 'Tidak',
            closeOnConfirm: false,
            closeOnCancel: true
    }, function (isConfirm) {
            if (isConfirm) {
                var publish = "{{ $publish }}";
                console.log(publish);
				if(publish > 0){
					swal({
						title: 'Maaf...',
						text: 'Harap menginput data untuk semua pertanyaan pada Standar Instrumen ini !',
						type: 'error',
						showConfirmButton: true
					});
				} else {
					url = "{{ url('instrumen/publish') }}";
                    var _token = "{{ csrf_token() }}";
					$.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
						url : url,
						type : "POST",
                        // data: { '_token': _token, 'id_pertanyaan' : id_pertanyaan},
                        data: $(".form-kriteria").serializeArray(),
						success : function(data) {
							if(data.status == true){
								swal({
								type: "success",
								title: 'Berhasil...',
								text: 'Data telah dipublish..!',
								showConfirmButton: false
								});
								setTimeout(function(){
									window.location.reload();
								}, 1500);
							} else {
								swal({
								type: "error",
								title: 'Gagal...',
								text: 'Maaf, terjadi kesalahan..!',
								showConfirmButton: false
								});
								setTimeout(function(){
									window.location.reload();
								}, 1500);
							}

						},
						error : function(){
							alert('gagal');
						}
					});
				}
            }
        });
    });


    // Disable after publish
    // var _token = "{{ csrf_token() }}";
      $.ajax({
        type: "GET",
        url: "{{ url($url_admin.'/instrumen/disable') }}"+"/"+{{ $id }},
        // data: { '_token': _token},
        dataType: 'json',
        cache: false,

            success: function(data){
                $.each(data, function(key, value)
                {
                    if(value.publish == 1){

                        $('input').attr('disabled', 'disabled');
                        $('#notif_publish').html('<div class="alert alert-warning mb-4 col-sm-12 col-md-12"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Maaf ...</strong><hr class="message-inner-separator"><p>Data telah di Publish dan tidak dapat di ubah lagi</p></div></div>');
                    }



                });
            }
        });
    // #Disable after publish
</script>
<div id="modalHabisPeriode" class="modal hide fade" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Pemberitahuan</h4>
            </div>
            <div class="modal-body">
               <p> <h4>Maaf, Periode pengisian instrumen <strong> telah berakhir </strong>.</h4> <br>
                <h6> Untuk informasi lebih lanjut, silahkan hubungi LPM. Terimakasih</h6></p>

                <h6 ><i class="ti-email"></i> : lpm@uir.ac.id</h6>
            </div><!-- MODAL-BODY -->
            <div class="modal-footer" >
                <a type="button" href="{{ url('/dashboard') }}" class="btn btn-secondary"><i class="ti-close"></i> Kembali</a>
            </div>
        </div>
    </div><!-- MODAL-DIALOG -->
</div>


@endsection


