$(document).ready(function(){
   $('.tambah').click(function(){
		adanLoadingFadeIn();
		$.loadmodal({
			url: "{{ url($url_admin.'/instrumen/publish') }}",
			id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'primary',
			title: 'Tambah',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
                    adanLoadingFadeOut();
				},

			},
        });
  });

});
