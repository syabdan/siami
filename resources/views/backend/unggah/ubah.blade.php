<link href="{{ URL::asset('assets/plugins/fileuploads/css/fileupload.css')}}" rel="stylesheet" />

{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
<div class="row">
	<div class="col-md-12">
        <div class="form-group row">
            {!! Form::label('Jenis Dokumen', 'Jenis Dokumen *', array('class' => 'col-md-4 form-label')) !!}
            <div class="col-md-12">
            {!! Form::select('jenis', ['Berita Acara'=>'Berita Acara', 'Laporan'=>'Laporan'], $data->jenis, array('id' => 'jenis', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
            </div>
        </div>

		{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
	    {!! Form::hidden('id', $data->id, array('id' => 'id')) !!}

		<div class="form-group row">
			<div class="col-md-12">
				<span class="pesan"></span>
			</div>
		</div>

	</div>
</div>

{!! Form::close() !!}
<script src="{{ URL::asset('adan/unggah/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/fileupload.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/fileuploads/js/file-upload.js') }}"></script>
