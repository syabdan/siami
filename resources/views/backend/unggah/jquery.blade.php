$(document).ready(function(){
    $(document).on("click",".ubah",function() {
    adanLoadingFadeIn();
        var id = $(this).attr('data-id');
        $.loadmodal({
            url: "{{ url($url_admin.'/unggah') }}/"+ id +"/edit",
        id: 'responsive',
            dlgClass: 'fade',
            bgClass: 'warning',
            title: 'Ubah',
            width: '700px',
            modal: {
                keyboard: true,
                // any other options from the regular $().modal call (see Bootstrap docs)
                },
            ajax: {
                dataType: 'html',
                method: 'GET',
                success: function(data, status, xhr){
            adanLoadingFadeOut();
                },
            },
        });
    });

    $(document).on("click",".unggah",function() {
    adanLoadingFadeIn();
    var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/unggah/modal_unggah') }}/"+ id,
            id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'success',
			title: 'Unggah',
			width: '700px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".hapus",function() {
    adanLoadingFadeIn();
        var id = $(this).attr('data-id');
        $.loadmodal({
            url: "{{ url($url_admin.'/unggah/hapus') }}/"+ id,
        id: 'responsive',
            dlgClass: 'fade',
            bgClass: 'danger',
            title: 'Hapus',
            width: '300px',
            modal: {
                keyboard: true,
                // any other options from the regular $().modal call (see Bootstrap docs)
                },
            ajax: {
                dataType: 'html',
                method: 'GET',
                success: function(data, status, xhr){
            adanLoadingFadeOut();
                },
            },
        });
    });

});
