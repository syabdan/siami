<link href="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}

		<div class="form-group row">
		{!! Form::label('Nama Standar', 'Nama Standar', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			{!! Form::text('nama_standar', NULL, array('id' => 'nama_standar', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. Standar 9')) !!}
			</div>
		</div>

        <div class="form-group row">
		{!! Form::label('Keterangan', 'Keterangan', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			{!! Form::text('keterangan', NULL, array('id' => 'keterangan', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. Standar Visi Misi')) !!}
			</div>
		</div>



	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/standar/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
