{{-- <link href="{{ URL::asset('assets/plugins/summernote/summernote-bs4.css')}}" rel="stylesheet"> --}}
<link href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/plugins/wysiwyag/richtext.css')}}" rel="stylesheet">
{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}

		<div class="form-group row">
		{!! Form::label('Nama Standar', 'Nama Standar', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
            {!! Form::select('standar_id', $standar, NULL, array('id' => 'standar_id', 'class' => 'form-control select2','placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
		</div>

        <div class="form-group row">
		{!! Form::label('Group', 'Group', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
            {!! Form::select('s_unit', $jenis, NULL, array('id' => 's_unit', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
		</div>

        <div class="form-group row">
		{!! Form::label('Pertanyaan', 'Pertanyaan', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
                <textarea name="isi_pertanyaan" class="" id="isi_pertanyaan" cols="30" rows="2" ></textarea>

                {{-- {!! Form::text('isi_pertanyaan', NULL, array('id' => 'isi_pertanyaan', 'class' => 'form-control summernote', 'placeholder' => 'Isi pertanyaan')) !!} --}}
			</div>
        </div>

        <div class="form-group row">
		{!! Form::label('Pilihan Jawaban 4', 'Pilihan Jawaban 4', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
                <textarea name="pilihan4" class="content col-md-12" style="100%" rows="3" ></textarea>
			</div>
		</div>

        <div class="form-group row">
		{!! Form::label('Pilihan Jawaban 3', 'Pilihan Jawaban 3', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
                <textarea name="pilihan3" class="content col-md-12" style="100%" rows="3" ></textarea>
			</div>
        </div>

        <div class="form-group row">
		{!! Form::label('Pilihan Jawaban 2', 'Pilihan Jawaban 2', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
                <textarea name="pilihan2" class="content col-md-12" style="100%" rows="3" ></textarea>
			</div>
		</div>

        <div class="form-group row">
		{!! Form::label('Pilihan Jawaban 1', 'Pilihan Jawaban 1', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
                <textarea name="pilihan1" class="content col-md-12" style="100%"rows="3" ></textarea>
			</div>
		</div>

        <div class="form-group row">
		{!! Form::label('Pilihan Jawaban 0', 'Pilihan Jawaban 0', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
                <textarea name="pilihan0" class="content col-md-12" style="100%" rows="3" ></textarea>
			</div>
		</div>

        <div class="form-group row">
		{!! Form::label('Catatan', 'Catatan', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
                <textarea name="catatan" id="catatan" class="" cols="30" rows="2" ></textarea>
			</div>
        </div>

        <div class="form-group row">
            {!! Form::label('Status', 'Status', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-12">
            {!! Form::select('s_aktif', $status, NULL, array('id' => 's_aktif', 'class' => 'form-control select2', 'style' => 'width:100%')) !!}
            </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Upload File', 'Upload File', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-12">
            {!! Form::select('s_unggah', $status, NULL, array('id' => 's_unggah', 'class' => 'form-control select2', 'style' => 'width:100%')) !!}
            </div>
        </div>

	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/pertanyaan/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('/') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
{{-- <script src="{{ URL::asset('assets/plugins/summernote/summernote-bs4.js') }}"></script> --}}
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/wysiwyag/jquery.richtext.js') }}"></script>
<script>
    $('.select2').select2();

    $('#isi_pertanyaan').richText();
    // $('#isi_pertanyaan').summernote({
    //     placeholder: 'Isi pertanyaan',
    //     tabsize: 2,
    //     height: 200
    // });

    $('#catatan').richText();
    // $('#catatan').summernote({
    //     placeholder: 'Kosongkan bila tidak ada catatan.',
    //     tabsize: 2,
    //     height: 200
    //   });



</script>
