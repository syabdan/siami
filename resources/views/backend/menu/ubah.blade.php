<link href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
        <div class="form-group row">
		    {!! Form::label('Nama Menu', 'Nama Menu', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			{!! Form::text('nama', $data->nama, array('id' => 'nama', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. Master')) !!}
			</div>
		</div>

        <div class="form-group row">
            {!! Form::label('Label', 'Label', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-12">
            {!! Form::text('label', $data->label, array('id' => 'label', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. Di isi untuk Kelompok Menu')) !!}
            </div>
        </div>

		<div class="form-group row">
		    {!! Form::label('Link', 'Link', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
			{!! Form::text('link', $data->link, array('id' => 'link', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. master')) !!}
			</div>
		</div>

		<div class="form-group row">
		    {!! Form::label('Icon', 'Icon', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-12">
                {!! Form::select('icon', $icon, $data->icon , array('id' => 'icon', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}

			</div>
		</div>

		<div class="form-group row">
			{!! Form::label('Posisi', 'Posisi ', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-12">
			<select name="posisi" id="posisi" class="form-control select2" style="width:100%">
			<option value="">Pilih</option>
			<?php
				for($i=1;$i<=100;$i++){
					echo "<option value='".$i."'>$i</option>";
				}
			?>
			</select>
			</div>
		</div>

		<div class="form-group row">
			{!! Form::label('Parent', 'Parent Menu ', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-12">
  			{!! Form::select('parent_menu_id', $parent_menu, $data->parent_menu_id, array('id' => 'parent_menu_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
		</div>

		<div class="form-group row">
			{!! Form::label('Tampil', 'Tampil ', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-12">
  			{!! Form::select('tampil', $tampil, $data->tampil, array('id' => 'tampil', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
		</div>

	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('adan/menu/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/script/ajax.js') }}"></script>

<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js') }}"></script>
<script>
	$('.select2').select2();
</script>
