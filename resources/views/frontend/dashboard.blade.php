@extends('frontend/layout')

@section('content')

    <!-- About -->
    <div id="about" class="basic-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Program Studi</h2>
                    <p class="p-heading p-large">Daftar Program Studi yang diaudit untuk menunjang Mutu Internal dilingkungan Universitas Islam Riau</p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">

                    @foreach ($users as $user)
                    <!-- Team Member -->
                    <div class="team-member">
                        <a href="{{ url('frontend/lihat/' . $user->id) }}">
                        <div class="image-wrapper">
                            <img class="img-fluid" src="{{ asset('frontend') }}/images/team-member-2.svg" alt="alternative">
                        </div> <!-- end of image wrapper -->
                        </a>
                        <p class="p-large"><strong>{{ $user->nama_unit }}</strong></p>
                        <p class="job-title">{{ $user->jenjang_pendidikan }}</p>
                        <span class="social-icons">
                            <span class="fa-stack">
                                <a href="#your-link">
                                    <i class="fas fa-circle fa-stack-2x facebook"></i>
                                    <i class="fab fa-facebook-f fa-stack-1x"></i>
                                </a>
                            </span>
                            <span class="fa-stack">
                                <a href="#your-link">
                                    <i class="fas fa-circle fa-stack-2x twitter"></i>
                                    <i class="fab fa-twitter fa-stack-1x"></i>
                                </a>
                            </span>
                        </span> <!-- end of social-icons -->

                    </div> <!-- end of team-member -->
                    <!-- end of team member -->
                    @endforeach

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-4 -->
    <!-- end of about -->
@endsection
