@extends('frontend.layout')
@section('css')
<link href="{{ URL::asset('assets/plugins/morris/morris.css')}}" rel="stylesheet">
<!--C3.JS CHARTS PLUGIN -->
<link href="{{ URL::asset('assets/plugins/charts-c3/c3-chart.css')}}" rel="stylesheet"/>
@endsection
@section('content')
<div id="about" class="basic-4">
    <div class="container">

        <!-- ROW-1 NCR -->
        {{-- <div class="row row-cards">
            <div class="col-sm-12 col-lg-12 col-md-12 col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-center">Mayor</h4>
                        <div class="mt-3 text-center">
                            <div class="chart-circle mx-auto chart-dropshadow-warning" data-value="0.80" data-thickness="7" data-color="#763568"><div class="chart-circle-value"><div class="font-weight-normal fs-20">{{ $mayor_persen ?? null }}</div></div></div>
                            <p class="mb-0 mt-2">Persentase Mayor</p>
                        </div>
                        <ul class="list-items mt-2">
                            <li class="p-1">
                                <span class="list-label"></span>Jumlah Program Studi
                                <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_prodi }}</span>
                            </li>
                            <li class="p-1">
                                <span class="list-label"></span>Jumlah Instrumen
                                <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_instrumen }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-12 col-md-12 col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-center">Minor</h4>
                        <div class="mt-3 text-center">
                            <div class="chart-circle mx-auto chart-dropshadow-info" data-value="0.80" data-thickness="7" data-color="#26c2f7"><div class="chart-circle-value"><div class="font-weight-normal fs-20">{{  $minor_persen ?? null }}</div></div></div>
                            <p class="mb-0 mt-2">Persentase Minor</p>
                        </div>
                        <ul class="list-items mt-2">
                            <li class="p-1">
                                <span class="list-label"></span>Jumlah Program Studi
                                <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_prodi }}</span>
                            </li>
                            <li class="p-1">
                                <span class="list-label"></span>Jumlah Instrumen
                                <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_instrumen }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-12 col-md-12 col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-center">Sesuai</h4>
                        <div class="mt-3 text-center">
                            <div class="chart-circle mx-auto chart-dropshadow-success" data-value="0.80" data-thickness="7" data-color="#33ce7b"><div class="chart-circle-value"><div class="font-weight-normal fs-20">{{  $sesuai_persen ?? null }}</div></div></div>
                            <p class="mb-0 mt-2">Persentase Sesuai</p>
                        </div>
                        <ul class="list-items mt-2">
                            <li class="p-1">
                                <span class="list-label"></span>Jumlah Program Studi
                                <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_prodi }}</span>
                            </li>
                            <li class="p-1">
                                <span class="list-label"></span>Jumlah Instrumen
                                <span class="list-items-percentage float-right font-weight-semibold mr-4">{{ $jumlah_instrumen }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- ROW-1 CLOSED -->

        <figure class="highcharts-figure">
            <div id="container"></div>

        </figure>

        <div class="row row-cards">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-4">
                <div class="card overflow-hidden">
                    <div class="card-header">
                        <h3 class="card-title">Persentase Audit Berbasis Risiko (ABR)</h3>
                    </div>
                    <div class="card-body">
                        <div id="abr_unit" class="h-270 donutShadow"></div>
                    </div>
                </div>
            </div><!-- COL END -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Chart NCR Berdasarkan Instrumen</h3>
                    </div>
                    <div class="card-body">
                        <div id="chart-bar-stacked" class="chartsh"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
<script src="{{ URL::asset('assets/plugins/morris/raphael-min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/morris/morris.js') }}"></script>
    <script>
        new Morris.Donut({
            element: 'abr_unit',
            data: {!! $abr_array !!},
            backgroundColor: 'rgba(119, 119, 142, 0.2)',
            labelColor: '#77778e',
            colors: ['#ffb61e', '#48929b', '#1f4788'],
            formatter: function(x) {
                return x + "%"
            }
        }).on('click', function(i, row) {
            console.log(i, row);
        });



    /*chart-assessment*/
    var chart = c3.generate({
             bindto: '#chart-bar-stacked', // id of chart wrapper
             data: {
                 columns: [
                     // each columns data
                     ['data1',  @foreach($chart_ncr_per_standar['mayor'] as $mayor)  {{ $mayor.',' }} @endforeach],
                     ['data2', @foreach($chart_ncr_per_standar['minor'] as $minor)  {{ $minor.',' }} @endforeach],
                     ['data3', @foreach($chart_ncr_per_standar['sesuai'] as $sesuai)  {{ $sesuai.',' }} @endforeach],
                 ],
                 type: 'bar', // default type of chart
                 groups: [
                     ['data1', 'data2', 'data3']
                 ],
                 colors: {
                     data1: '#f3c13a',
                     data2: '#2abb9b',
                     data3: '#4d8fac',
                 },
                 names: {
                     // name of each serie
                     'data1': 'Mayor',
                     'data2': 'Minor',
                     'data3': 'Sesuai',
                 }
             },
             axis: {
                 x: {
                     type: 'category',
                     // name of each category
                     categories: {!! json_encode($chart_ncr_per_standar['category']) !!}
                 },
             },
             bar: {
                 width: 16
             },
             legend: {
                 show: true, //hide legend
             },
             padding: {
                 bottom: 0,
                 top: 0
             },
         });

    </script>


<script>
    Highcharts.chart('container', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: "Persentase Ketidaksesuaian (NCR) Program Studi {!!  $unit->nama_unit !!}",
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    credits: {
        enabled: false
    },
    series: [{
        type: 'pie',
        name: 'Ketidaksesuaian',
        data: [
            ['Mayor', {!! $mayor_persen !!}],
            ['Minor', {!! $minor_persen !!}],
            ['Sesuai', {!! $sesuai_persen !!}],

        ]
    }],
    colors: ['#f7ca18','#044f67','#00b5cc']


});
</script>


@endsection
