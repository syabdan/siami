<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta Tags -->
    <meta name="description" content="Sistem Informasi Audit Mutu Internal Universitas Islam Riau, Sistem Informasi Audit Mutu Internal (SIAMI) digunakan sebagai media evaluasi prodi/unit berbasis manajemen risiko agar diperoleh peningkatan mutu pendidikan tinggi yang berkelanjutan">
    <meta name="author" content="Syabdan Dalimunthe">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
	<meta property="og:site_name" content="siami uir" /> <!-- website name -->
	<meta property="og:site" content="http://siami.uir.ac.id" /> <!-- website link -->
	<meta property="og:title" content="siami uir"/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="Sistem Informasi Audit Mutu Internal" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
	<meta property="og:type" content="article" />

    <!-- Website Title -->
    <title>SIAMI - Sistem Informasi Audit Mutu Internal{{ asset('frontend') }}/</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet">
    <link href="{{ asset('frontend') }}/css/bootstrap.css" rel="stylesheet">
    <link href="{{ asset('frontend') }}/css/fontawesome-all.css" rel="stylesheet">
    <link href="{{ asset('frontend') }}/css/swiper.css" rel="stylesheet">
	<link href="{{ asset('frontend') }}/css/magnific-popup.css" rel="stylesheet">
	<link href="{{ asset('frontend') }}/css/styles.css" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/morris/morris.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/plugins/charts-c3/c3-chart.css')}}" rel="stylesheet"/>

	<!-- Favicon  -->
    <link rel="icon" href="{{ asset('frontend') }}/images/favicon.png">

    @yield('css')
</head>
<body data-spy="scroll" data-target=".fixed-top">

    <!-- Preloader -->
	<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->


    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top ">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Evolo</a> -->

        <!-- Image Logo -->
        <a class="navbar-brand logo-image" href="{{ url('/') }}"><img src="{{ asset('/') }}/frontend/images/siami6.png" alt="alternative"></a>

        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
            <span class="navbar-toggler-awesome fas fa-times"></span>
        </button>
        <!-- end of mobile menu toggle button -->

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#contact">Kontak</a>
                </li>
            </ul>
            <span class="nav-item social-icons">
                <span class="fa-stack">
                    <a href="#your-link">
                        <i class="fas fa-circle fa-stack-2x facebook"></i>
                        <i class="fab fa-facebook-f fa-stack-1x"></i>
                    </a>
                </span>
                <span class="fa-stack">
                    <a href="#your-link">
                        <i class="fas fa-circle fa-stack-2x twitter"></i>
                        <i class="fab fa-twitter fa-stack-1x"></i>
                    </a>
                </span>
            </span>
        </div>
    </nav> <!-- end of navbar -->
    <!-- end of navigation -->


    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row mt--5">
                    <div class="col-lg-6 ">
                        <div class="text-container">
                            <h1>Selamat Datang di Sistem <span class="turquoise">SIAMI</span> UIR</h1>
                            <p class="p-large">Sistem Informasi Audit  Mutu   Internal  (SIAMI)  digunakan sebagai media evaluasi prodi/unit berbasis manajemen risiko agar diperoleh peningkatan mutu  pendidikan  tinggi yang berkelanjutan</p>
                            <a class="btn-solid-lg page-scroll" href="{{ route('login') }}">Masuk</a>
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-6">
                        <div class="image-container">
                            <img class="img-fluid" src="{{ asset('frontend') }}/images/header-teamwork.svg" alt="alternative">
                        </div> <!-- end of image-container -->
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <!-- end of header -->

    @yield('content')



    <!-- Contact -->
    <div id="contact" class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">

                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">

                <div class="col-lg-4 col-md-4">
                  <div class="footer-info">
                    <h3>Siami UIR</h3>
                    <p>
                      Jl. Kaharuddin Nasution 113
                      28284, Pekanbaru Riau <br>
                      <strong>Phone:</strong>+62 761 674 674<br>
                      <strong>Fax:</strong> +62 761 674 834<br>
                      <strong>Email:</strong> info@uir.ac.id<br>
                    </p>

                  </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-col middle">
                        <h4>Tautan Penting</h4>
                        <ul class="list-unstyled li-space-lg">
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Website Universitas Islam Riau <a class="turquoise" href="http://uir.ac.id">uir.ac.id</a></div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Sistem Sadardiri<a class="turquoise" href="http://sadardiri.uir.ac.id">Sadardiri UIR</a></div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15958.786685554316!2d101.452555!3d0.447421!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe6f83f2deb032e8f!2sIslamic%20University%20of%20Riau!5e0!3m2!1sen!2sid!4v1609132713972!5m2!1sen!2sid" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                </div>


              </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of form-2 -->
    <!-- end of contact -->


    <!-- Footer -->
    {{-- <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-col">
                        <h4>Tentang Siami</h4>
                        <p>Sistem Informasi Audit Mutu Internal Universitas Islam Riau</p>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4">
                    <div class="footer-col middle">
                        <h4>Tautan Penting</h4>
                        <ul class="list-unstyled li-space-lg">
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Website Universitas Islam Riau <a class="turquoise" href="http://uir.ac.id">uir.ac.id</a></div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Sistem Sadardiri<a class="turquoise" href="http://sadardiri.uir.ac.id">Sadardiri UIR</a></div>
                            </li>
                        </ul>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4">
                    <div class="footer-col last">
                        <h4>Social Media</h4>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-instagram fa-stack-1x"></i>
                            </a>
                        </span>
                    </div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div>  --}}
    <!-- end of footer -->
    <!-- end of footer -->


    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <p class="p-small">Copyright © {{ date('Y') }} <a href="https://uir.ac.id">Universitas Islam Riau</a> - All rights reserved</p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright -->
    <!-- end of copyright -->


    <!-- JQUERY JS -->
	<script src="{{ URL::asset('assets/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('frontend') }}/js/popper.min.js" defer></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="{{ asset('frontend') }}/js/bootstrap.min.js" defer></script> <!-- Bootstrap framework -->
    <script src="{{ asset('frontend') }}/js/jquery.easing.min.js" defer></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="{{ asset('frontend') }}/js/swiper.min.js" defer></script> <!-- Swiper for image and text sliders -->
    <script src="{{ asset('frontend') }}/js/jquery.magnific-popup.js" defer></script> <!-- Magnific Popup for lightboxes -->
    <script src="{{ asset('frontend') }}/js/validator.min.js" defer></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="{{ asset('frontend') }}/js/scripts.js" defer></script> <!-- Custom scripts -->


    <script src="{{ URL::asset('assets/plugins/morris/raphael-min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/morris/morris.js') }}"></script>
    <!-- C3.JS CHART JS -->
    <script src="{{ URL::asset('assets/plugins/charts-c3/d3.v5.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/charts-c3/c3-chart.js') }}"></script>

    <script src="{{ URL::to('/') }}/assets/plugins/highchart/highcharts.js" ></script>
    <script src="{{ URL::to('/') }}/assets/plugins/highchart/highcharts-more.js" ></script>
    <script src="{{ URL::to('/') }}/assets/plugins/highchart/exporting.js" ></script>
    <script src="{{ URL::to('/') }}/assets/plugins/highchart/export-data.js" ></script>
    <script src="{{ URL::to('/') }}/assets/plugins/highchart/accessibility.js" ></script>

    @yield('js')
</body>
</html>
