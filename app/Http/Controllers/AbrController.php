<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Jawaban;
use App\Models\Pertanyaan;
use App\Models\Periode;
use App\Models\Standar;
use App\Models\Dosen;
use App\Models\Unit;
use App\Models\User;
use App\Models\Dokumen;
use App\Models\PenugasanAuditor;
use Auth;
use PDF;
use DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class AbrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($periode=null)
    {
        $periode==null ? $periode=cekPeriode()->id : $periode=$periode;
        $jawaban            = $this->jawaban($periode);
        $djawaban           = $jawaban->get();
        $pilihUnitAuditee   = Unit::select("units.id", \DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_prodi"))->whereNotNull('parent_id')->pluck('nama_prodi', 'id');
        $pilihPeriode       = Periode::select('nama_periode','id')->pluck('nama_periode','id');
        $periodeTerpilih = Periode::select('nama_periode','id')->where('id', $periode)->first();

        // dd($djawaban);
        if($djawaban->isEmpty()){
            if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2){
                $unit      = with (new User)->select('unit_id as id')->where('id', \Auth::user()->id)->first();
                $data      = array();
                $auditee   = array();
                $auditors  = array();
                return view('backend.abr.index', compact('djawaban','data','auditee','auditors', 'unit','pilihUnitAuditee','pilihPeriode','periodeTerpilih'));
            }
            return view('errors.unpublish');
        }

        $data               = $jawaban->first();
        $auditee            = $this->auditee( $data->unit_id);

        $auditors           = $this->auditor( $data->unit_id);


        return view('backend.abr.index', compact('djawaban','data','auditee','auditors','pilihUnitAuditee','pilihPeriode','periodeTerpilih'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tindakan($id)
    {
        $data   = Jawaban::whereId($id)->first();

        return view('backend.abr.tindakan', compact('data'));
    }

    public function modal_unggah()
    {
        return view('backend.abr.unggah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function unggah(Request $request)
    {
        // if ($file = $request->file('pic')) {

                // $dataFile = savegdrive($file);
                // $dataFile = savelocal($file);

                $periode = cekPeriode();

                $update =  Dokumen::updateOrCreate(
                    ['unit_id'    => \Auth::user()->unit_id, 'user_id'  => \Auth::user()->id, 'periode_id' => $periode['id']], [
                    // 'file'        => $dataFile,
                    'urlfile'        => $request->urlfile,
                    'jenis'       => 'ABR'
                ]);

        // }else{
        //     $respon = array('status'=>false, 'pesan' => ['msg' => 'Mohon upload dokumen dengan benar !']);
        //     return response()->json($respon);
        // }


        if ($update) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
        }

        return response()->json($respon);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data   = Jawaban::find($id);

        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'aspect'              => 'required',
            'impact'              => 'required',
            'likelihood'          => 'required',
            'severity'            => 'required',
            'mitigation'          => 'required',
       ],
       [
           'aspect.required'        => 'Harap mengisi form ASPECT / (PENYEBAB RISK).',
           'impact.required'        => 'Harap mengisi form IMPACT / (AKIBAT RISK).',
           'likelihood.required'    => 'Harap mengisi form Likelihood.',
           'severity.required'      => 'Harap mengisi form Severity.',
           'mitigation.required'    => 'Harap mengisi form RISK MITIGATION.',
      ]);

        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        }else{
            if($request->likelihood == 'H' && $request->severity == 'H'){
                $assesment = 'H';
            }else
            if($request->likelihood == 'H' && $request->severity == 'M'){
                $assesment = 'H';
            }else
            if($request->likelihood == 'H' && $request->severity == 'L'){
                $assesment = 'M';
            }else
            if($request->likelihood == 'M' && $request->severity == 'H'){
                $assesment = 'H';
            }else
            if($request->likelihood == 'L' && $request->severity == 'H'){
                $assessment = 'M';
            }else
            if($request->likelihood == 'M' && $request->severity == 'M'){
                $assesment = 'M';
            }else
            if($request->likelihood == 'M' && $request->severity == 'L'){
                $assesment = 'L';
            }else
            if($request->likelihood == 'L' && $request->severity == 'M'){
                $assesment = 'L';
            }else
            if($request->likelihood == 'L' && $request->severity == 'L'){
                $assesment = 'L';
            }

            $data->assesment = $assesment;
            $data->update();

            $update = $data->update($request->except(['unit_id']));

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }

        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cetak(Request $request, $periode=null)
    {

        $jawaban        = $this->jawaban($periode);
        $djawaban       = $jawaban->where('publish', 1)->get();

        $data           = $jawaban->first();

        $auditee        = $this->auditee($data->unit_id);

        $auditors       = $this->auditor($data->unit_id,$periode);

        $namaketua      = PenugasanAuditor::select(DB::raw("CONCAT(dosens.gelar_depan,' ',dosens.nama_dosen,', ',dosens.gelar_belakang) as nama_dosen"),'nidn')
                    ->join('users', 'users.id', 'penugasan_auditors.user_id')
                    ->join('dosens', 'dosens.id', 'users.dosen_id')
                    ->join('periodes', 'periodes.id', 'penugasan_auditors.periode_id');
                    // Cek Periode
                    if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2){
                        $namaketua->where('penugasan_auditors.periode_id', \Auth::user()->periode_id);
                    }else{
                        if($periode==null){
                            $namaketua->where('periodes.status', 1);
                        }else{
                            $namaketua->where('penugasan_auditors.periode_id', $periode);
                        }
                    }
                    $namaketua->where('penugasan_auditors.unit_audit_id',  $data->unit_id)
                    ->where('penugasan_auditors.posisi', 'Ketua');

        $ketua  = $namaketua->first();
        // return view('backend.abr.report', compact('djawaban','data','auditee','auditors','ketua'));


        view()->share(['djawaban' => $djawaban ,'data' => $data ,'auditee' => $auditee ,'auditors' => $auditors,'ketua' =>$ketua]);


        	// pass view file
            $pdf = PDF::loadView('backend.abr.report')->setPaper('a4', 'landscape');
            // download pdf
            return $pdf->stream('abr_'.$auditee->nama_unit.'.pdf');



        // $pdf            = PDF::loadView('backend.abr.report', compact('djawaban','data','auditee','auditors','ketua'))->setPaper('a4', 'landscape');
        // return $pdf->stream('ABR.pdf', array('Attachment'=>0));
    }

    private function auditor($idUnit,$periode=null){
        $auditors = PenugasanAuditor::select(DB::raw("CONCAT(dosens.gelar_depan,' ',dosens.nama_dosen,', ',dosens.gelar_belakang) as nama_dosen"))
                    ->join('users', 'users.id', 'penugasan_auditors.user_id')
                    ->join('dosens', 'dosens.id', 'users.dosen_id')
                    ->join('periodes', 'periodes.id', 'penugasan_auditors.periode_id');
                    // Cek Periode
                    if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2){
                        $auditors->where('penugasan_auditors.periode_id', \Auth::user()->periode_id);
                    }else{
                        if($periode==null){
                            $auditors->where('periodes.status', 1);
                        }else{
                            $auditors->where('penugasan_auditors.periode_id', $periode);
                        }
                    }
                    $auditors->where('penugasan_auditors.unit_audit_id',  $idUnit)
                    ->orderBy('posisi','DESC');

        return $auditors->get();
    }

    private function auditee($idUnit){
        $auditee  = Dosen::select(DB::raw("CONCAT(dosens.gelar_depan,' ',dosens.nama_dosen,', ',dosens.gelar_belakang) as nama_dosen"),'nidn')
                    ->join('users', 'users.dosen_id', 'dosens.id')
                    ->where('users.unit_id', $idUnit)
                    ->where('users.permission_id',4)
                    ->first();
        return $auditee;
    }

    private function jawaban($periode=null){
        $jawaban        = Jawaban::select('jawabans.id as id','temuan','uraian','nama_unit','aspect','impact','likelihood','severity','assesment','mitigation','user_id','auditor_id','unit_id','periode_id','nama_periode')
                            ->join('pertanyaans', 'pertanyaans.id', 'jawabans.pertanyaan_id')
                            ->join('standars', 'standars.id', 'pertanyaans.standar_id')
                            ->join('units', 'units.id', 'jawabans.unit_id')
                            ->join('periodes', 'periodes.id', 'jawabans.periode_id');
                            // Cek Periode
                            if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2){
                                $jawaban->where('periode_id', \Auth::user()->periode_id);
                            }else{
                                if($periode==null){
                                    $jawaban->where('periodes.status', 1);
                                }else{
                                    $jawaban->where('periode_id', $periode);
                                }
                            }
                            $jawaban->where('temuan', '!=', 'Sesuai');

                            if(\Auth::user()->permission_id == 4){
                                $jawaban->where('user_id', Auth::user()->id);
                            }
                           else{
                                $jawaban->where('unit_id', Auth::user()->unit_id);
                            }

                            $jawaban->where('publish', 1)->orderBy('pertanyaans.id','asc');

        return $jawaban;
    }


    public function ditandatangani ($periodeId)
    {
        $data = Dokumen::where('periode_id', $periodeId)->where('unit_id',\Auth::user()->unit_id)->first();

        // return viewgdrive($data->file);
        return viewlocal($data->file);
    }

}
