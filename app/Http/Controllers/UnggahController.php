<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Dokumen;
use App\Models\Unit;
use App\Models\Periode;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;

class UnggahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pilihUnitAuditee   = Unit::select("units.id", \DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_prodi"))->whereNotNull('parent_id')->pluck('nama_prodi', 'id');
        $pilihPeriode       = Periode::select('nama_periode','id')->pluck('nama_periode','id');

        return view('backend.unggah.index', compact('pilihUnitAuditee','pilihPeriode'));
    }

    public function data(Request $request)
    {
        $data = Dokumen::with(['periode', 'unit'])
                ->orderBy('id', 'DESC');
                if(Auth::user()->permission_id == 3 || Auth::user()->permission_id == 4){
                    $data->where('unit_id', \Auth::user()->unit_id);
                }
                $data->where('jenis','<>', 'ABR');
        return DataTables::of($data->get())

            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-outline-warning btn-xs ubah mr-2"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-outline-danger btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })
            ->addColumn('prodi', function (Dokumen $data) {
                return $data->unit->nama_unit.' - '.$data->unit->jenjang_pendidikan;
            })
            ->addColumn('periode', function (Dokumen $data) {
                return $data->periode->nama_periode;
            })
            ->addColumn('file', function (Dokumen $data) {
                // return '<a href="'.url('unggah/ambil_file/'.$data->file).'" target="_blank" class="btn btn-outline-primary btn-sm"> <i class="fa fa-file"></i>'.$data->file.'</a></span>';
                return ($data->urlfile) ? '<a href="'.$data->urlfile.'" target="_blank" class="btn btn-outline-primary btn-sm"> <i class="fa fa-file"></i>Lihat Dokumen</a></span>' : '<span>Belum ada dokumen </span></span>';
            })
            ->addIndexColumn()
            ->rawColumns(['actions','periode','file','prodi'])
            ->make(true);
    }

    public function modal_unggah()
    {
        return view('backend.unggah.unggah');
    }

    public function unggah(Request $request)
    {

        // if ($file = $request->file('pic')) {

                // $dataFile = savegdrive($file);

                // $dataFile = savelocal($file);

                $periode = cekPeriode();

                $update =  Dokumen::updateOrCreate(
                    ['unit_id'      => \Auth::user()->unit_id, 'user_id'  => \Auth::user()->id, 'periode_id' => $periode['id'], 'jenis' => $request->jenis], [
                    // 'file'          => $dataFile,
                    'urlfile'       => $request->urlfile,
                    'jenis'         => $request->jenis,
                    'unit_id'       => \Auth::user()->unit_id,
                    'user_id'       => \Auth::user()->id,
                    'periode_id'    => $periode['id'],
                ]);

        // }else{
        //     $respon = array('status'=>false, 'pesan' => ['msg' => 'Mohon upload dokumen dengan benar !']);
        //     return response()->json($respon);
        // }


        if ($update) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
        }

        return response()->json($respon);
    }


    public function ambil_file($file)
    {
        // return viewgdrive($file);
        return viewlocal($file);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = Dokumen::find($id);
        return view('backend.unggah.ubah', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'jenis'              => 'required',
        ],
        [
            'jenis.required'     => 'Harap mengisi form jenis.'
       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data   = Dokumen::find($id);
            $update = $data->update($request->all());

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        $data = Dokumen::find($id);
        return view('backend.unggah.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = Dokumen::find($id);

        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
