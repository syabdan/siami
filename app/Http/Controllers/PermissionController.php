<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\User;
use App\Models\Menu;
use App\Models\Unit;
use App\Models\Nilai;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Hash;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('backend.permission.index');
    }

    public function data(Request $request)
    {
        $data = Permission::orderBy('id', 'DESC')->get();
        return DataTables::of($data)

            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-outline-warning btn-xs ubah mr-2" title="ubah" ><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-outline-danger btn-xs hapus" title="hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })
            ->addIndexColumn()
            ->rawColumns(['actions'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data           = Permission::all();
        $menus          = Menu::all();

        return view('backend.permission.tambah', compact('data','menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $validator = Validator::make($request->all(), [
        //     'permission_name'       => 'required',
        //     'menu'                  => 'required',
        // ]);
        // if ($validator->fails()) {
        //     $respon = array('status'=>false, 'pesan' => $validator->messages());
        // } else {
            $stuff = $request->menu;
            $data_menus =  implode(", ", $stuff);
            $data   = Permission::create(['permission_name' => $request->permission_name, 'data_menus' => $data_menus]);
        if ($data) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
        }
    // }
    return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = Permission::find($id);
        $menus          = Menu::all();

        return view('backend.permission.ubah', compact('data','menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'permission_name'       => 'required',
            'menu'                  => 'required',
       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data   = Permission::find($id);
            $stuff = $request->menu;
            $data_menus =  implode(", ", $stuff);
            $update = $data->update(['permission_name' => $request->permission_name, 'data_menus' => $data_menus]);

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $data = Permission::find($id);
        return view('backend.permission.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = Permission::find($id);

        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
