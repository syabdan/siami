<?php

namespace App\Http\Controllers;

use App\Models\PenugasanAuditor;
use App\Models\Permission;
use App\Models\User;
use App\Models\Menu;
use App\Models\Unit;
use App\Models\Dosen;
use App\Models\Periode;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;

class PenugasanAuditorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.penugasan.index');
    }

    public function data(Request $request)
    {
        $data = PenugasanAuditor::orderBy('id', 'DESC')->with('user','unit','periode')->get();
        return DataTables::of($data)
            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-outline-warning btn-xs ubah mr-2"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-outline-danger btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })

            ->addColumn('nama_auditor', function($data){
                $auditor  = Dosen::select(DB::raw("CONCAT(dosens.gelar_depan,' ',dosens.nama_dosen,', ',dosens.gelar_belakang) as nama_dosen"),"dosens.id")->join('users','users.dosen_id','dosens.id')->where('users.id',$data->user_id)->first();
                // $auditor  = ($data->user->dosen->gelar_depan) ?? null.' '.$data->user->dosen->nama_dosen.', '.($data->user->dosen->gelar_belakang) ?? null;
                return $auditor->nama_dosen;
            })
            ->addColumn('unit_auditee', function($data){
                $unit_auditee = $data->unit->nama_unit.' - '.$data->unit->jenjang_pendidikan;
                return $unit_auditee;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','nama_auditor','unit_auditee'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $auditor        = Dosen::select( DB::raw("CONCAT(dosens.gelar_depan,' ',dosens.nama_dosen,', ',dosens.gelar_belakang) as dosen"),"users.id as id")->join('users','users.dosen_id','dosens.id')->where('permission_id',3)->pluck('dosen','id');
        $unit           = Unit::select("id", DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_prodi"))->whereNotNull('parent_id')->pluck('nama_prodi', 'id');
        $periode        = Periode::select("id" , "nama_periode")->pluck('nama_periode', 'id');

        return view('backend.penugasan.tambah', compact('auditor','unit','periode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id'           => 'required',
            'unit_audit_id'     => 'required',
            'periode_id'        => 'required',
            'posisi'            => 'required',
        ],
        [
            'user_id.required'          => 'Harap mengisi form Nama Auditor.',
            'unit_audit_id.required'    => 'Harap mengisi form Unit Auditee.',
            'periode_id.required'       => 'Harap mengisi form Periode.',
            'posisi.required'           => 'Harap mengisi form Posisi.',
        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
        $data   = PenugasanAuditor::create($request->all());
        if ($data) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
        }
    }
    return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = PenugasanAuditor::find($id);
        $auditor        = User::join('dosens','dosens.id', 'users.dosen_id')->select("users.id as id", DB::raw("CONCAT(dosens.gelar_depan,' ',dosens.nama_dosen,', ',dosens.gelar_belakang) as nama_dosen"))->where('permission_id', 3)->pluck('nama_dosen', 'id');
        $unit           = Unit::select("id", DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_prodi"))->whereNotNull('parent_id')->pluck('nama_prodi', 'id');
        $periode        = Periode::select("id" , "nama_periode")->pluck('nama_periode', 'id');

        return view('backend.penugasan.ubah', compact('data','auditor','unit','periode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'user_id'           => 'required',
            'unit_audit_id'     => 'required',
            'periode_id'        => 'required',
            'posisi'            => 'required',
        ],
        [
            'user_id.required'          => 'Harap mengisi form Nama Auditor.',
            'unit_audit_id.required'    => 'Harap mengisi form Unit Auditee.',
            'periode_id.required'       => 'Harap mengisi form Periode.',
            'posisi.required'           => 'Harap mengisi form Posisi.',
        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data   = PenugasanAuditor::find($id);
            $update = $data->update($request->all());

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $data = PenugasanAuditor::find($id);
        return view('backend.penugasan.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = PenugasanAuditor::find($id);

        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
