<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\User;
use App\Models\Menu;
use App\Models\Unit;
use App\Models\Dosen;
use App\Models\Periode;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Hash;
use Intervention\Image\Facades\Image;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.users.index');
    }

    public function data(Request $request)
    {
        $data = User::select(['id','nama_user', 'email','permission_id'])
                ->orderBy('id', 'DESC')->get();
        return DataTables::of($data)
            ->editColumn('level',function(User $data) {
                return $data->permissionsGroup->permission_name;
            })

            ->addColumn('actions',function($data) {
                $actions = '<a user-id="'.$data->id.'" class="btn btn-outline-warning btn-xs ubah mr-2" title="ubah"><i class="fa fa-pencil"></i></a>';
                if($data->id != Auth::user()->id){
                    $actions .= '<a  user-id="'.$data->id.'" class="btn btn-outline-danger btn-xs hapus" title="hapus"><i class="fa fa-trash-o"></i></a>';

                }

                return $actions;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','level'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data           = User::all();
        $dosen          = Dosen::select("id", DB::raw("CONCAT(dosens.gelar_depan,' ',dosens.nama_dosen,', ',dosens.gelar_belakang) as nama_dosen"))->pluck('nama_dosen','id');
        $unit           = Unit::select("id", DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_prodi"))->whereNotNull('parent_id')->pluck('nama_prodi', 'id');
        $permission     = Permission::all()->pluck('permission_name', 'id');
        // $periode        = Periode::all()->pluck('nama_periode','id');
        return view('backend.users.tambah', compact('data', 'dosen', 'unit','permission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email'             => 'required|string|email|max:255|unique:users',
            'password'          => 'min:6',
			'pic' 		        => 'file|image|mimes:jpeg,png,jpg|max:2048',

        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data = new User;


            // dd($unit->parent->nama_unit);

            if($request->permission_id ==3 || $request->permission_id == 4){
                $unit  = Unit::where('id', $request->unit_id)->with('parent')->first();

                // Update Data Dosen
                $dosen = Dosen::updateOrCreate(['nidn' => $request->nidn], [
                    'nama_dosen'           => $request->nama_dosen,
                    'gelar_depan'          => $request->gelar_depan ?? '',
                    'gelar_belakang'       => $request->gelar_belakang ?? '',
                    'fakultas'             => $unit->parent->nama_unit,
                    'prodi'                => $unit->nama_unit,
                ]);

                $data->dosen_id             = $dosen->id;
            }

            if ($file = $request->file('pic')) {
                $fullName=$file->getClientOriginalName();
                $name=explode('.',$fullName)[0];

                $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
                $replaceImage=strtolower($originalImage);
                $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path().'/backend/assets/img/user/';

                $file->move($destinationPath, $gambarName);
                $data->pic = $gambarName;
            }
            if($request->nama_user){
                $namaUser = $request->nama_user;
            }else{
                $namaUser = $request->gelar_depan.' '.$request->nama_dosen.', '.$request->gelar_belakang;
            }

            $data->nama_user            = $namaUser;
            $data->email                = $request->email;
            $data->hp                   = $request->hp;
            $data->permission_id        = $request->permission_id;
            $data->unit_id              = $request->unit_id;
            $data->tgl_lulus_auditor    = $request->tgl_lulus_auditor;
            $data->password             = bcrypt($request->password);

            $user =  $data->save() ? 1 : 0;
            if ($user) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = User::find($id);
        $dosen          = Dosen::where('id', $data->dosen_id)->first();

        $unit           = Unit::select("id", DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_prodi"))->whereNotNull('parent_id')->pluck('nama_prodi', 'id');
        $permission     = Permission::all()->pluck('permission_name', 'id');

        return view('backend.users.ubah', compact('data', 'dosen', 'unit','permission'));
    }

    public function editProfile($id)
    {
        $data           = User::find($id);
        $dosen          = Dosen::where('id', $data->dosen_id)->first();

        $unit           = Unit::select("id", DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_prodi"))->whereNotNull('parent_id')->pluck('nama_prodi', 'id');
        $permission     = Permission::all()->pluck('permission_name', 'id');

        return view('backend.users.profile', compact('data', 'dosen', 'unit','permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_profile(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_user'         => 'required|string|max:255',
            // 'email'             => 'required|string|email|max:255|unique:users,email,'.$id
        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $user = User::find($id);
            if ($request->filled('password')) {
                $update = $user->update($request->except('password'));

                $user->password = bcrypt($request->password);
                $user->update();

            }
                $update = $user->update($request->except('password','pic'));
                if ($file = $request->file('pic')) {
                    $fullName=$file->getClientOriginalName();
                    $name=explode('.',$fullName)[0];

                    $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
                    $replaceImage=strtolower($originalImage);
                    $gambarName = time().'_'.$replaceImage.'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path().'/backend/assets/img/user/';

                    $file->move($destinationPath, $gambarName);
                    $user->pic = $gambarName;
                    $user->update();
                }



            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    public function update_unit_auditor(Request $request, $id)
    {
        $user = User::find($id);
        $update = $user->update($request->all());
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'nama_user'         => 'required|string|max:255',
            // 'email'             => 'required|string|email|max:255|unique:users,email,'.$id
        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $user = User::find($id);
            if ($request->filled('password')) {
                $update = $user->update($request->except('password'));

                $user->password = bcrypt($request->password);
                $user->update();

            } else {

                    if($user->permission_id ==3 || $user->permission_id == 4){
                        $unit  = Unit::where('id', $user->unit_id)->with('parent')->first();

                        // Update Data Dosen
                        $dosen = Dosen::updateOrCreate(['nidn' => $request->nidn], [
                            'nama_dosen'           => $request->nama_dosen,
                            'gelar_depan'          => $request->gelar_depan,
                            'gelar_belakang'       => $request->gelar_belakang,
                            // 'fakultas'             => $unit->parent->nama_unit,
                            // 'prodi'                => $unit->nama_unit,
                        ]);

                        $user->dosen_id = $dosen->id;

                    }


                // $update = $user->update($request->except('password','pic', 'dosen_id'));
                if ($file = $request->file('pic')) {
                    $fullName=$file->getClientOriginalName();
                    $name=explode('.',$fullName)[0];

                    $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
                    $replaceImage=strtolower($originalImage);
                    $gambarName = time().'_'.$replaceImage.'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path().'/backend/assets/img/user/';

                    $file->move($destinationPath, $gambarName);
                    $user->pic = $gambarName;
                    // $user->update();
                }


            }
            $namaUser = $request->nama_user;

            if($user->permission_id == 3){
                $namaUser = $request->gelar_depan.' '.$request->nama_dosen.', '.$request->gelar_belakang;
            }


            $user->nama_user            = $namaUser;
            $user->email                = $request->email;
            $user->hp                   = $request->hp;
            $user->permission_id        = $request->permission_id;
            $user->unit_id              = $request->unit_id;
            $user->tgl_lulus_auditor    = $request->tgl_lulus_auditor;

            $update = $user->update() ? 1 : 0;


            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $user = User::find($id);
        return view('backend.users.hapus', ['user' => $user]);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        if ($user->id == \Auth::id()) {
            return response()->json(array('status'=>false, 'pesan' => ['msg' => 'Maaf, tidak bisa menghapus akun sendiri. Silahkan Hubungi Administrator.']));
        }
        if ($user->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }

    public function reset_auditee(Request $request)
    {
        if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2){
            \DB::update('update users set unit_id = NULL where id = ?', [\Auth::user()->id]);
        }
        return redirect()->back();
    }
}
