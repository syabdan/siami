<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Jawaban;
use App\Models\Pertanyaan;
use App\Models\Periode;
use App\Models\Standar;
use App\Models\Dosen;
use App\Models\User;
use App\Models\Unit;
use App\Models\PenugasanAuditor;
use Auth;
use PDF;
use DB;

class NcrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function unit(){
        if(\Auth::user()->permission_id == 3){
            $unitu      = with (new User)->select('penugasan_auditors.unit_audit_id as id')->join('penugasan_auditors','penugasan_auditors.user_id','users.id')->where('users.id', \Auth::user()->id)->first();
            $unituser   = $unitu->id ?? null;

        }else
        if(\Auth::user()->permission_id == 4){
            $unitu = with (new User)->select('unit_id as id')->where('id', \Auth::user()->id)->first();
            $unituser = $unitu->id ?? null;
        }else{
            $unituser = null;

        }

        return $unituser;
    }

    public function standar($id,$periode=null)
    {
        $periode==null ? $periode=cekPeriode()->id : $periode=$periode;

        $jawaban            = $this->ambilJawaban($id,$periode);
        $standar            = Standar::whereId($id)->first();
        // $publish        = with(new Jawaban)->cekPublish($id);

        $djawaban               = $jawaban->get();
        $data                   = $jawaban->first();
        $pilihUnitAuditee       = Unit::select("units.id", \DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_prodi"))->whereNotNull('parent_id')->pluck('nama_prodi', 'id');
        $pilihPeriode           = Periode::select('nama_periode','id')->pluck('nama_periode','id');
        $periodeTerpilih = Periode::select('nama_periode','id')->where('id', $periode)->first();

        if($djawaban->isEmpty()){
            if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2){
                $unit      = with (new User)->select('unit_id as id')->where('id', \Auth::user()->id)->first();
                return view('backend.ncr.index', compact('id','djawaban', 'standar', 'unit','pilihUnitAuditee','pilihPeriode','data', 'periodeTerpilih'));
            }
            return view('errors.unpublish');
        }


        return view('backend.ncr.index', compact('id','djawaban', 'standar','pilihUnitAuditee','pilihPeriode','data', 'periodeTerpilih'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tindakan($id)
    {
        $data   = Jawaban::whereId($id)->first();

        return view('backend.ncr.tindakan', compact('data'));
    }

    public function penyebab($id)
    {
        $data   = Jawaban::whereId($id)->first();

        return view('backend.ncr.penyebab', compact('data'));
    }

    public function catatan_verifikasi($id)
    {
        $data   = Jawaban::whereId($id)->first();

        return view('backend.ncr.catatan-verifikasi', compact('data'));
    }

    public function pencegahan($id)
    {
        $data   = Jawaban::whereId($id)->first();

        return view('backend.ncr.pencegahan', compact('data'));
    }

    public function prodikoreksi($id)
    {
        $data   = Jawaban::whereId($id)->first();

        return view('backend.ncr.prodikoreksi', compact('data'));
    }

    public function reupload($id)
    {
        $data   = Jawaban::whereId($id)->first();

        return view('backend.ncr.reupload', compact('data'));
    }

    public function verifikasi($id)
    {
        $data   = Jawaban::whereId($id)->first();

        return view('backend.ncr.verifikasi', compact('data'));
    }

    public function nilai($id)
    {
        $data   = Jawaban::whereId($id)->first();

        return view('backend.ncr.nilai', compact('data'));
    }

    public function unggah($id)
    {
        $data   = Jawaban::whereId($id)->first();

        return view('backend.ncr.unggah', compact('data'));
    }

    public function unggah_file(Request $request, $id)
    {
        $data = Jawaban::find($id);

        $data->urlfile_koreksi = $request->urlfile_koreksi;
        $data->update();

        if ($data) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diunggah']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diunggah']);
        }

        return response()->json($respon);

        // if ($file = $request->file('file_koreksi')) {


        //     #Save to gdrive
        //     // $fullName=$file->getClientOriginalName();
        //     // $name=explode('.',$fullName)[0];
        //     // $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
        //     // $replaceImage=strtolower($originalImage);
        //     // $gambarName = time().'_'.$replaceImage.'.'.$file->getClientOriginalExtension();
        //     // $destinationPath =  storage_path().'/backend/files/ncr';
        //     // $file->move($destinationPath, $gambarName);

        //     // $dataFile = savegdrive($file);
        //     #Save to gdrive


        //     #Save to local storage
        //     // $dataFile = savelocal($file);
        //     // $data->file_koreksi = $dataFile;
        //     // $data->update();
        //     #Save to local storage



        //     if ($data) {
        //         $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diunggah']);
        //     } else {
        //         $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diunggah']);
        //     }

        //     return response()->json($respon);
        // }else{
        //     $respon = array('status'=>false, 'pesan' => ['msg' => 'Oopps.. Anda belum memilih file. Pilih File Terlebih Dahulu !']);
        //     return response()->json($respon);

        // }


    }

    public function ambil_file($file)
    {
        // $data = Jawaban::where('file_jawaban',$file)->first();
        // return response()->file(storage_path('/backend/files/instrumen/'.$data->file_jawaban));

        // return viewgdrive($file);
        return viewlocal($file);

    }

    public function ambil_file_konfirmasi($file)
    {
        // $data = Jawaban::where('file_konfirmasi',$file)->first();
        // return response()->file(storage_path('/backend/files/ncr/'.$data->file_konfirmasi));

        // return viewgdrive($file);
        return viewlocal($file);

    }

    public function ambil_file_ncr($file)
    {
        // $data = Jawaban::where('file_koreksi',$file)->first();
        // return response()->file(storage_path('/backend/files/ncr/'.$data->file_koreksi));

        // return viewgdrive($file);
        return viewlocal($file);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data   = Jawaban::find($id);
        $update = $data->update($request->except(['unit_id']));

        #sementara upload dokumen tidak dipakai
        // if ($file = $request->file('file_jawaban')) {
        //     $data2   = Jawaban::find($id);
        //     // $dataFile = savegdrive($file);
        //     $dataFile = savelocal($file);
        //     $data2->file_jawaban = $dataFile;
        //     $data2->update();
        // }
        #sementara upload dokumen tidak dipakai

        if ($file = $request->file('file_konfirmasi')) {
            $data2   = Jawaban::find($id);

            // $fullName=$file->getClientOriginalName();
            // $name=explode('.',$fullName)[0];

            // $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            // $replaceImage=strtolower($originalImage);
            // $gambarName = time().'_'.$replaceImage.'.'.$file->getClientOriginalExtension();
            // $destinationPath =  storage_path().'/backend/files/ncr';
            // $file->move($destinationPath, $gambarName);

            // $dataFile = savegdrive($file);
            $dataFile = savelocal($file);

            $data2->file_konfirmasi = $dataFile;
            $data2->update();

            // if ($data) {
            //     $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diunggah']);
            // } else {
            //     $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diunggah']);
            // }

            // return response()->json($respon);
        }


        if($request->nilai_auditor !== null){
            $data3   = Jawaban::find($id);
            // cari temuan
            $data3->temuan = $this->ambilTemuan($request->nilai_auditor);

            $data3->update();
        }

        if ($update) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
        }

        return response()->json($respon);
    }

    private function ambilTemuan($nilai_auditor){
        if($nilai_auditor == 3){
            $temuan = 'Minor';
        }

        if($nilai_auditor == 4){
            $temuan = 'Sesuai';
        }

        if($nilai_auditor == "0" || $nilai_auditor == 1 || $nilai_auditor == 2){
            $temuan = 'Mayor';
        }

        return $temuan;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function ambilJawaban($id,$periode){
        $jawaban = Jawaban::select('jawabans.id as id','temuan','nilai','nilai_auditor','jawaban', 'catatan_penyebab', 'catatan_koreksi', 'catatan_pencegahan', 'catatan_verifikasi', 'tgl_tindakan_koreksi', 'tgl_ttd_auditor', 'tgl_verifikasi','standar_id', 'nama_unit','uraian','user_id','auditor_id','unit_id','file_jawaban','file_koreksi','pertanyaan_id','urlfile','urlfile_koreksi','urlfile_konfirmasi')
                            ->join('pertanyaans', 'pertanyaans.id', 'jawabans.pertanyaan_id')
                            ->join('standars', 'standars.id', 'pertanyaans.standar_id')
                            ->join('units', 'units.id', 'jawabans.unit_id')
                            ->join('periodes', 'periodes.id', 'jawabans.periode_id');
                            // Cek Periode
                            if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2){
                                $jawaban->where('periode_id', \Auth::user()->periode_id);
                            }else{
                                if($periode==null){
                                    $jawaban->where('periodes.status', 1);
                                }else{
                                    $jawaban->where('periode_id', $periode);
                                }
                            }
                            $jawaban->where('standar_id', $id);
                             //Cek User ID
                            $jawaban->where('unit_id', \Auth::user()->unit_id)->where('publish', 1);


                            $jawaban->orderBy('pertanyaans.id','asc');

                            // dd($jawaban->get());
                            return $jawaban;
    }

    public function cetak(Request $request, $id, $periode=null)
    {
        $jawaban     = $this->ambilJawaban($id,$periode)->where('temuan', '!=', 'Sesuai');
        $ambilj      = $this->ambilJawaban($id,$periode)->get();

        $i      = 1;
        $sub    = array();
        foreach ($ambilj as $val) {
            if($val->temuan != 'Sesuai'){
                $sub[] = $i;
            }
            $i++;
        }

        $datas       = $jawaban->get();
        $id_user     = $jawaban->first();

        // $auditee    = Dosen::select(DB::raw("CONCAT(dosens.gelar_depan,' ',dosens.nama_dosen,', ',dosens.gelar_belakang) as nama_dosen"))
        //             ->join('users', 'users.dosen_id', 'dosens.id')
        //             ->where('users.id', $id_user->user_id)->first();

        $dauditors = PenugasanAuditor::select(DB::raw("CONCAT(dosens.gelar_depan,' ',dosens.nama_dosen,', ',dosens.gelar_belakang) as nama_dosen"))
                    ->join('users', 'users.id', 'penugasan_auditors.user_id')
                    ->join('dosens', 'dosens.id', 'users.dosen_id')
                    ->join('periodes', 'periodes.id', 'penugasan_auditors.periode_id');
                    // Cek Periode
                    if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2){
                        $dauditors->where('penugasan_auditors.periode_id', \Auth::user()->periode_id);
                    }else{
                        if($periode==null){
                            $dauditors->where('periodes.status', 1);
                        }else{
                            $dauditors->where('penugasan_auditors.periode_id', $periode);
                        }
                    }
                    $dauditors->where('penugasan_auditors.unit_audit_id',  \Auth::user()->unit_id)
                    ->orderBy('posisi','DESC');
        $auditors  = $dauditors->get();

        // return view('backend.ncr.report', compact('datas','auditee','auditors'));
        view()->share(['datas' => $datas,'auditors' => $auditors, 'sub' => $sub]);


        $pdf = PDF::loadView('backend.ncr.report');
        // download pdf
        return $pdf->stream('NCR_standar_'.$id.' .pdf');

        // $pdf        = PDF::loadView('backend.ncr.report', compact('datas','auditee','auditors'));
        // return $pdf->stream('NCR_standar'.$id.' .pdf', array('Attachment'=>0));
    }
}
