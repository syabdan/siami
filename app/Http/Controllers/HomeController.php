<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jawaban;
use App\Models\Unit;
use App\Models\Pertanyaan;
use App\Models\User;
use App\Models\Periode;
use Yajra\DataTables\DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        $data_proses                = new Jawaban;
        $units                      = Unit::select('id', \DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_unit"), 'jenjang_pendidikan')->whereNotNull('parent_id')->orderBy('id', 'asc');
        if(\Auth::user()->permission_id == 3 || \Auth::user()->permission_id == 4){
            $pertanyaan             = Pertanyaan::whereIn('s_unit',['Semua', \Auth::user()->unit->jenjang_pendidikan ?? null])->where('s_aktif',1)->count();
        }else{
            $pertanyaan             = Pertanyaan::where('s_unit','Semua')->where('s_aktif',1)->count();
        }

        $unitss                     = $units->get();
        $unit_tampil                = $units->paginate(9);


        $abr_array                  = $data_proses->abr_array(\Auth::user()->unit_id); // abr array per unit

        foreach($unitss as $unit){
            $pertanyaan_unit        = Pertanyaan::where('s_unit',$unit->jenjang_pendidikan)->where('s_aktif',1)->count();
            $chart_ncr[$unit->id]   = $data_proses->chart_ncr($unit->id);
            $chart_abr[$unit->id]   = $data_proses->abr_array($unit->id);

            $total_pertanyaan       = ($pertanyaan+$pertanyaan_unit);


        // Untuk dashboard prodi
            // Persen Mayor
            $jumlah_mayor                   = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periodes.status', 1)->where('temuan','Mayor')->where('unit_id', $unit->id)->count();
            $mayor_persen[$unit->id]        = number_format($jumlah_mayor / $pertanyaan * 100, 2);
            // Persen Minor
            $jumlah_minor                   = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periodes.status', 1)->where('temuan','Minor')->where('unit_id', $unit->id)->count();
            $minor_persen[$unit->id]        = number_format($jumlah_minor / $pertanyaan * 100, 2);
            // Persen Sesuai
            $jumlah_sesuai                  = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periodes.status', 1)->where('temuan','Sesuai')->where('unit_id', $unit->id)->count();
            $sesuai_persen[$unit->id]       = number_format($jumlah_sesuai / $pertanyaan * 100, 2);
        // #Untuk dashboard prodi


            // Persen Assessment High
            $jumlah_h                       = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periodes.status', 1)->where('assesment','H')->where('unit_id', $unit->id)->count();
            // $h_persen[$unit->id]                = number_format($jumlah_h / $pertanyaan * 100, 2);
            // Persen Assessment Medium
            $jumlah_m                       = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periodes.status', 1)->where('assesment','M')->where('unit_id', $unit->id)->count();
            // $m_persen[$unit->id]                = number_format($jumlah_m / $pertanyaan * 100, 2);
            // Persen Assessment Low
            $jumlah_l                       = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periodes.status', 1)->where('assesment','L')->where('unit_id', $unit->id)->count();
            // $l_persen[$unit->id]                = number_format($jumlah_l / $pertanyaan * 100, 2);

        }

        if(\Auth::user()->permission_id == 4 || \Auth::user()->permission_id == 3){
            $chart_ncr_per_standar      = $data_proses->chart_ncr_per_standar(\Auth::user()->id) ?? null;

        }else{
            $chart_ncr_per_standar = null;
        }

        if(\Auth::user()->permission_id == 1 || \Auth::user()->permission_id == 2){
            $chart_ncr_per_prodi        = $data_proses->chart_ncr_per_prodi();

        }else{
            $chart_ncr_per_prodi = null;
        }

        if(\Auth::user()->permission_id == 3){
            $unitu      = with (new User)->select('penugasan_auditors.unit_audit_id as id')->join('penugasan_auditors','penugasan_auditors.user_id','users.id')->where('users.id', \Auth::user()->id)->first();
            $unituser   = $unitu->id ?? null;

            // Pilih Auditee Untuk Auditor
            $pilihUnitAuditee      = Unit::select("units.id", \DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_prodi"))
            ->join('penugasan_auditors', 'penugasan_auditors.unit_audit_id', 'units.id')
            ->join('periodes', 'periodes.id', 'penugasan_auditors.periode_id')
            ->where('periodes.status', 1)
            ->where('user_id', \Auth::user()->id)
            ->pluck('nama_prodi', 'id') ?? null;
            // #End Pilih Auditee Untuk Auditor


        }else if(\Auth::user()->permission_id == 4){
            $unitu      = with (new User)->select('unit_id as id')->where('id', \Auth::user()->id)->first();
            $unituser   = $unitu->id ?? null;
            $pilihUnitAuditee      = null;
        }else{
            $unituser   = null;
            $pilihUnitAuditee      = null;
        }

        // dd($pilihUnitAuditee);

        $pilihPeriode           = Periode::select('nama_periode','id')->pluck('nama_periode','id');
        // $periodeTerpilih        = Periode::select('nama_periode','id')->where('id', $periode)->first();
        return view('backend.dashboard',[
            'pilihPeriode'          => $pilihPeriode,
            // 'periodeTerpilih'       => $periodeTerpilih,
            'units'                 => $unit_tampil,
            'units2'                => $unitss,
            'unit'                  => $unituser,
            'pilihUnitAuditee'      => $pilihUnitAuditee,
            'mayor_persen'          => $mayor_persen,
            'minor_persen'          => $minor_persen,
            'sesuai_persen'         => $sesuai_persen,

            'jumlah_prodi'          => $unitss->count(),
            'jumlah_instrumen'      => $pertanyaan,

            'chart_ncr'             => $chart_ncr,
            'chart_abr'             => $chart_abr,
            'abr_array'             => $abr_array,

            'chart_ncr_per_standar'           => $chart_ncr_per_standar,
            'chart_ncr_per_prodi'             => $chart_ncr_per_prodi,

        ]);
    }

    public function data_monitoring(Request $request, $periode=null)
    {
        $periode==null ? $periode=cekPeriode()->id : $periode=$periode;
        $unit           = Unit::select("id", \DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_unit"),'jenjang_pendidikan')->whereNotNull('parent_id')->orderBy('id', 'ASC')->get();
        $data_proses    = new Jawaban;
        $pertanyaan     = Pertanyaan::where('s_unit','Semua')->where('s_aktif',1)->count();

        return DataTables::of($unit)

            ->addColumn('pengisian',function($unit) use($data_proses,$pertanyaan, $periode){
                $pertanyaan_unit                = Pertanyaan::where('s_unit',$unit->jenjang_pendidikan)->where('s_aktif',1)->count();

                $total_pertanyaan               = ($pertanyaan+$pertanyaan_unit);
                $jumlah_isi                     = $data_proses->cek_progres_pengisian($unit->id, $periode);
                $isi_persen                     = number_format($jumlah_isi / $total_pertanyaan * 100, 2);
                return $isi_persen;
            })

            ->addColumn('unggah_proses',function($unit) use($data_proses,$pertanyaan, $periode){
                $pertanyaan_all                 = Pertanyaan::where('s_unit','Semua')->where('s_aktif',1)->where('s_unggah',1)->count();
                $pertanyaan_unit                = Pertanyaan::where('s_unit',$unit->jenjang_pendidikan)->where('s_aktif',1)->where('s_unggah',1)->count();

                $total_pertanyaan               = ($pertanyaan_all+$pertanyaan_unit);
                $jumlah_unggah                  = $data_proses->cek_progres_unggah($unit->id, $periode);
                $unggah_persen                  = number_format($jumlah_unggah / $total_pertanyaan * 100, 2);
                return $unggah_persen;
            })

            ->addColumn('mayor',function($unit) use($data_proses,$pertanyaan, $periode){
                $pertanyaan_unit                = Pertanyaan::where('s_unit',$unit->jenjang_pendidikan)->where('s_aktif',1)->count();

                $total_pertanyaan               = ($pertanyaan+$pertanyaan_unit) ;
                $jumlah_mayor                   = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periode_id', $periode)->where('temuan', 'Mayor')->where('unit_id', $unit->id)->where('publish',1)->count();
                $mayor_persen                   = number_format($jumlah_mayor / $total_pertanyaan * 100, 2);

                return $jumlah_mayor." (".$mayor_persen." %)";
            })

            ->addColumn('minor',function($unit) use($data_proses,$pertanyaan, $periode){
                $pertanyaan_unit                = Pertanyaan::where('s_unit',$unit->jenjang_pendidikan)->where('s_aktif',1)->count();
                $total_pertanyaan               = ($pertanyaan+$pertanyaan_unit) ;
                $jumlah_minor                   = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periode_id', $periode)->where('temuan','Minor')->where('unit_id', $unit->id)->where('publish',1)->count();
                $minor_persen                   = number_format($jumlah_minor / $total_pertanyaan * 100, 2);

                return $jumlah_minor." (".$minor_persen." %)";
            })

            ->addColumn('sesuai',function($unit) use($data_proses,$pertanyaan, $periode){
                $pertanyaan_unit                = Pertanyaan::where('s_unit',$unit->jenjang_pendidikan)->where('s_aktif',1)->count();
                $total_pertanyaan               = ($pertanyaan+$pertanyaan_unit) ;
                $jumlah_sesuai                  = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periode_id', $periode)->where('temuan','Sesuai')->where('unit_id', $unit->id)->where('publish',1)->count();
                $sesuai_persen                  = number_format($jumlah_sesuai / $total_pertanyaan * 100, 2);

                return $jumlah_sesuai." (".$sesuai_persen." %)";
            })

            ->addColumn('high',function($unit) use($data_proses,$pertanyaan, $periode){
                $pertanyaan_unit                = Pertanyaan::where('s_unit',$unit->jenjang_pendidikan)->where('s_aktif',1)->count();
                $total_pertanyaan               = ($pertanyaan+$pertanyaan_unit) ;
                $jumlah_h                       = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periode_id', $periode)->where('assesment','H')->where('unit_id', $unit->id)->where('publish',1)->count();
                $h_persen                       = number_format($jumlah_h / $total_pertanyaan * 100, 2);
                return $h_persen;
            })

            ->addColumn('medium',function($unit) use($data_proses,$pertanyaan, $periode){
                $pertanyaan_unit                = Pertanyaan::where('s_unit',$unit->jenjang_pendidikan)->where('s_aktif',1)->count();
                $total_pertanyaan               = ($pertanyaan+$pertanyaan_unit) ;
                $jumlah_m                       = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periode_id', $periode)->where('assesment','M')->where('unit_id', $unit->id)->where('publish',1)->count();
                $m_persen                       = number_format($jumlah_m / $total_pertanyaan * 100, 2);
                return $m_persen;
            })

            ->addColumn('low',function($unit) use($data_proses,$pertanyaan, $periode){
                $pertanyaan_unit                = Pertanyaan::where('s_unit',$unit->jenjang_pendidikan)->where('s_aktif',1)->count();
                $total_pertanyaan               = ($pertanyaan+$pertanyaan_unit) ;
                $jumlah_l                       = $data_proses->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periode_id', $periode)->where('assesment','L')->where('unit_id', $unit->id)->where('publish',1)->count();
                $l_persen                       = number_format($jumlah_l / $total_pertanyaan * 100, 2);
                return $l_persen;
            })

            ->addIndexColumn()
            ->rawColumns(['mayor','minor','sesuai', 'high', 'medium', 'low','nama_unit','unggah_proses','pengisian'])
            ->make(true);
    }

    public function data_monitoring_prodi(Request $request)
    {
        $data_proses    = new Jawaban;
        $pertanyaan     = Pertanyaan::where('s_unit','Semua')->where('s_aktif',1)->count();
        $periode        = Periode::select('id','nama_periode')->orderBy('id','DESC')->get();
        $unit           = Unit::select('id','jenjang_pendidikan')->where('id',\Auth::user()->unit_id)->first();

        if($unit){
            return DataTables::of($periode)

            ->addColumn('pengisian',function($periode) use($unit,$data_proses,$pertanyaan){
                $pertanyaan_unit                = Pertanyaan::where('s_unit',$unit->jenjang_pendidikan)->where('s_aktif',1)->count();

                $total_pertanyaan               = ($pertanyaan+$pertanyaan_unit);
                $jumlah_isi                     = $data_proses->cek_progres_pengisian($unit->id, $periode->id);
                $isi_persen                     = number_format($jumlah_isi / $total_pertanyaan * 100, 2);
                return $isi_persen;
            })

            ->addColumn('unggah_proses',function($periode) use($unit,$data_proses,$pertanyaan){
                $pertanyaan_all                 = Pertanyaan::where('s_unit','Semua')->where('s_aktif',1)->where('s_unggah',1)->count();
                $pertanyaan_unit                = Pertanyaan::where('s_unit',$unit->jenjang_pendidikan)->where('s_aktif',1)->where('s_unggah',1)->count();

                $total_pertanyaan               = ($pertanyaan_all+$pertanyaan_unit);
                $jumlah_unggah                  = $data_proses->cek_progres_unggah($unit->id, $periode->id);
                $unggah_persen                  = number_format($jumlah_unggah / $total_pertanyaan * 100, 2);
                return $unggah_persen;
            })


            ->addIndexColumn()
            ->rawColumns(['unggah_proses','pengisian','nama_periode'])
            ->make(true);
        }else{
            return response()->json();
        }
    }


}
