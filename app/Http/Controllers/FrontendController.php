<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Unit;
use App\Models\User;
use App\Models\Jawaban;
use App\Models\Pertanyaan;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Unit::whereNotNull('parent_id')->get();
        return view('frontend.dashboard',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lihat($id_unit)
    {
        $users                 = User::where('unit_id', $id_unit)->orderBy('unit_id')->first() ?? null;
        $unit                  = Unit::select("id", \DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_unit"),'jenjang_pendidikan')->where('id', $id_unit)->orderBy('id', 'ASC')->first();

        $data_proses                = new Jawaban;
        $chart_ncr_per_standar      = $data_proses->chart_ncr_per_standar($users->id ?? null);
        $abr_array                  = $data_proses->abr_array($id_unit ?? null);

        $pertanyaan                 = Pertanyaan::whereIn('s_unit',['Semua', $users->unit->jenjang_pendidikan ?? null])->where('s_aktif',1)->count();

        // Untuk dashboard prodi
            // Persen Mayor
            $jumlah_mayor                   = $data_proses->where('temuan','Mayor')->where('unit_id', $id_unit)->count();
            $mayor_persen                   = number_format($jumlah_mayor / $pertanyaan * 100, 2);
            // Persen Minor
            $jumlah_minor                   = $data_proses->where('temuan','Minor')->where('unit_id', $id_unit)->count();
            $minor_persen                   = number_format($jumlah_minor / $pertanyaan * 100, 2);
            // Persen Sesuai
            $jumlah_sesuai                  = $data_proses->where('temuan','Sesuai')->where('unit_id', $id_unit)->count();
            $sesuai_persen                  = number_format($jumlah_sesuai / $pertanyaan * 100, 2);
        // #Untuk dashboard prodi


        // dd($mayor_persen);
        $units                      = Unit::select('id', \DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_unit"), 'jenjang_pendidikan')->whereNotNull('parent_id')->orderBy('nama_unit', 'asc')->count();


        return view('frontend.unit.index'
        ,[
            'mayor_persen'              => $mayor_persen,
            'minor_persen'              => $minor_persen,
            'sesuai_persen'             => $sesuai_persen,
            'abr_array'                 => $abr_array,
            'chart_ncr_per_standar'     => $chart_ncr_per_standar,
            'jumlah_prodi'              => $units,
            'jumlah_instrumen'          => $pertanyaan,
            'unit'                      => $unit
        ]
        );
    }


}
