<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Dosen;
use App\Models\Unit;
use App\Models\PenugasanAuditor;
use PDF;

class BeritaAcaraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function cetak(Request $request)
    {

        $data           = PenugasanAuditor::select('users.nama_user as nama_user','posisi','nama_periode')
                            ->join('users', 'users.id', 'penugasan_auditors.user_id')
                            ->join('periodes','periodes.id','penugasan_auditors.periode_id')
                            ->where('periodes.status', 1)
                            ->where('penugasan_auditors.unit_audit_id',  \Auth::user()->unit_id);

        $user           = $data->get();
        $ketua          = $data->where('posisi', 'Ketua')->first();
        $dekan          = $request->dekan;
        $siklus         = $request->siklus;
        $auditee        = User::select('nama_user','dosen_id')->where('users.unit_id', \Auth::user()->unit_id)->where('permission_id', 4)->with('dosen')->first();
        $unit           = Unit::where('id',\Auth::user()->unit_id)->first();


        // return view('backend.auditor.berita_acara', compact('user','dekan','siklus','auditee','unit','ketua'));
        $pdf            = PDF::loadView('backend.auditor.berita_acara', compact('user','dekan','siklus','auditee','unit','ketua'));

        return $pdf->stream('Berita Acara AMI.pdf');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
