<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\User;
use App\Models\Menu;
use App\Models\Unit;
use App\Models\Dosen;
use App\Models\Periode;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Hash;
use Illuminate\Support\Facades\Http;


class PeriodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.periode.index');
    }

    public function data(Request $request)
    {
        $data = Periode::orderBy('id', 'DESC')->get();
        return DataTables::of($data)

            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-outline-warning btn-xs ubah mr-2"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-outline-danger btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })
            ->addColumn('status',function($data) {
                $data->status == 1 ? $return = '<span class="label label-sm label-success">Aktif</span>' : $return = '<span class="label label-sm label-danger">Tidak Aktif</span>';
                return $return;
            })
            // ->addColumn('jenis',function($data) {
            //     if($data->jenis == 1){
            //         $return = '<span class="label label-sm label-menu label-success">Awal TA</span>';
            //     }else if($data->jenis == 2){
            //         $return = '<span class="label label-sm label-menu label-warning">Akhir TA</span>';
            //     }else if($data->jenis == 3){
            //         $return = '<span class="label label-sm label-menu label-default">Asessor</span>';
            //     }
            //     else{
            //         $return = '<span class="label label-sm label-menu label-primary">Dekan</span>';
            //     }
            //     return $return;
            // })
            ->addIndexColumn()
            ->rawColumns(['actions','status'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data           = Periode::all();
        $status         = array(1 => 'Aktif', 0 => 'Tidak Aktif');
        $jenis          = array(1 => 'Awal Tahun Ajaran', 2 => 'Akhir Tahun Ajaran', 3 => 'Asessor', 4 => 'Dekan');

        return view('backend.periode.tambah', compact('data', 'status', 'jenis'));
    }

    public function pesan()
    {

        return view('backend.periode.pesan');
    }

    public function kirim_pesan(Request $request)
    {
        $isi_sms = strip_tags($request->pesan);
        $to = $request->to;


        if($to == 'Auditor'){
            $auditor = User::join('penugasan_auditors','penugasan_auditors.user_id','users.id')->where('permission_id',3)->get();
            foreach($auditor as $data){

                $no_hp      = $data->hp;

                $response = $this->link_sms($no_hp,$isi_sms);
                // $data = $response->json();

            }

        }
        if($to == 'Prodi'){
            $prodi = User::where('permission_id',4)->get();
            foreach($prodi as $data){

                $no_hp      = $data->hp;

                $response = $this->link_sms($no_hp,$isi_sms);
                // dd($response);

            }

        }
        if ($response) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Pesan Berhasil dikirim']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Pesan gagal dikirim']);
        }
        return response()->json($respon);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    private function link_sms($no_hp,$isi_sms)
    {
        return Http::get('http://sms.webuir.com/sadiri.php?user=sadiri&pass=uirunggul2020&no='.$no_hp.'&isi='.$isi_sms);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nama_periode'              => 'required',
            'tanggal_awal'              => 'required',
            'tanggal_akhir'             => 'required',
            'batas_isi_prodi'           => 'required',
       ],
       [
           'nama_periode.required'     => 'Harap mengisi form nama periode.',
           'tanggal_awal.required'     => 'Harap mengisi form tanggal awal.',
           'tanggal_akhir.required'    => 'Harap mengisi form tanggal akhir.',
           'batas_isi_prodi.required'  => 'Harap mengisi form batas pengisian instrumen.',
      ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {

            $ambil_id       = Periode::orderBy('id','desc')->first();
            if(!empty($ambil_id)){
            $id = $ambil_id->id;
                $update     = Periode::where('id', $id)->update(['status' => 0]);
            }

            $data   = Periode::create($request->all());
            if ($data) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
            }
        }

        return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = Periode::find($id);
        // $status         = array(1 => 'Aktif', 0 => 'Tidak Aktif');
        // $jenis          = array(1 => 'Awal Tahun Ajaran', 2 => 'Akhir Tahun Ajaran', 3 => 'Asessor', 4 => 'Dekan');

        return view('backend.periode.ubah', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'nama_periode'              => 'required',
            'tanggal_awal'              => 'required',
            'tanggal_akhir'             => 'required',
            'batas_isi_prodi'           => 'required',
        ],
        [
            'nama_periode.required'     => 'Harap mengisi form nama periode.',
            'tanggal_awal.required'     => 'Harap mengisi form tanggal awal.',
            'tanggal_akhir.required'    => 'Harap mengisi form tanggal akhir.',
            'batas_isi_prodi.required'  => 'Harap mengisi form batas pengisian instrumen.',
       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data   = Periode::find($id);
            $update = $data->update($request->all());

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $data = Periode::find($id);
        return view('backend.periode.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = Periode::find($id);

        if ($data->delete()) {
            $d = Periode::count();
            if($d === 1) DB::table('periodes')->update(['status' => 1]);
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
