<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Menu;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $url_admin = '';

    public function __construct()
    {

        $menu =  Menu::orderBy('posisi','asc')->whereNull('parent_menu_id')->where('tampil',1)->orderBy('posisi', 'asc')->get();

        $current	        = explode(".", \Route::currentRouteName());
        $submenu            = Menu::where('link', $current[0])->orderBy('id', 'desc')->first();
        $halaman            = $submenu === null ? null:$submenu;

        $parentMenu         = Menu::where('id', ($submenu->parent_menu_id) ?? NULL)->orderBy('id', 'asc')->first();
        $parent             = $parentMenu === null ? 'Dashboard':$parentMenu;


        View::share([
            'url_admin'     => $this->url_admin,
            'menu'          => $menu,
            'halaman'       => $halaman,
            'parent'        => $parent,
        ]);

    }
}
