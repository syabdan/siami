<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Jawaban;
use App\Models\Pertanyaan;
use App\Models\Periode;
use App\Models\Standar;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class JawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function standar($id)
    {

        $pertanyaan     = Pertanyaan::whereIn('s_unit',['Semua', \Auth::user()->unit->jenjang_pendidikan])->where('s_aktif',1)->get();
        $standar        = Standar::whereId($id)->first();
        $publish        = with(new Jawaban)->cekPublish($id);

        return view('backend.instrumen.index', compact('id','pertanyaan','standar', 'publish'));
    }

    public function data(Request $request)
    {
        $data = Pertanyaan::whereIn('s_unit',['Semua', \Auth::user()->unit->jenjang_pendidikan])->where('s_aktif',1)->with(['instrumen'])->orderBy('id', 'DESC')->get();
        return DataTables::of($data)

            ->addColumn('actions',function($data) {
                $actions = '<a href="#edit-'.$data->id.'" data-id="'.$data->id.'"  class="btn btn-tbl-edit btn-xs ubah"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-tbl-delete btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })
            ->addColumn('status_unggah',function($data) {
                $data->status_unggah == 1 ? $return = '<span class="label label-sm label-success">Aktif</span>' : $return = '<span class="label label-sm label-danger">Tidak Aktif</span>';
                return $return;
            })
            ->addColumn('isi_pertanyaan',function($data) {
                return "<div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>$data->isi_pertanyaan</div>";
            })
            ->addIndexColumn()
            ->rawColumns(['actions','status_unggah','isi_pertanyaan'])
            ->make(true);
    }

    public function publish(Request $request)
    {
        // dd($request->instrumen_id);
        for($i=0;$i<count($request->id_pertanyaan);$i++){
            $update = Jawaban::updateOrCreate(['pertanyaan_id' => $request->id_pertanyaan[$i],'user_id' => Auth::user()->id], [
                'publish'           => 1,
            ]);
        }

        if ($update) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
        }

        return response()->json($respon);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data           = Pertanyaan::whereIn('s_unit',['Semua', \Auth::user()->unit->jenjang_pendidikan])->where('s_aktif',1)->get();
        $instrumen      = Instrumen::pluck('nama_instrumen','id');
        $unggah         = array(0 => 'Tidak Aktif', 1 => 'Aktif');
        $ambil_lagi     = array(NULL => 'Tidak', 1 => 'Ya');
        $data_jawaban   = DataDiri::where('user_id', Auth::user()->id)->get();
        return view('backend.pertanyaan.tambah', compact('data','instrumen','unggah','data_jawaban', 'ambil_lagi'));
    }

    // public function disable()
    // {
    //     $data_jawaban   = Jawaban::where('user_id', Auth::user()->id)->get();
    //     return response()->json($data_jawaban);
    // }


    public function ambil_pilihan(Request $request)
    {
        $data   = Jawaban::join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periodes.status', 1)->where('user_id', Auth::user()->id)->get();
        return response()->json($data);
    }

    public function ambil_file ($file)
    {
        // $data = Jawaban::join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periodes.status', 1)->where('file_jawaban',$file)->first();
        // return response()->file(storage_path('/backend/files/instrumen/'.$data->file_jawaban));
        // return viewgdrive($file);
        return viewlocal($file);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function simpan_jawaban(Request $request)
    {
        $periode       = cekPeriode();;

        if(empty($periode)){
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Maaf, Data Periode Tidak Ada Yang Aktif, Silahkan hubungi LPM UIR']);
            return response()->json($respon);

        }

        $pertanyaan = Pertanyaan::whereIn('id', $request->pertanyaan_id)->get();
        foreach($pertanyaan as $p){
            if(!is_null($request->jawaban_nilai_.$p->id)){

                $isi    = 'jawaban_isi_'.$p->id;
                $nilai  = 'jawaban_nilai_'.$p->id;
                $url    = 'urlfile_'.$p->id;

                // cari temuan
                if($request->$nilai == 3){
                    $temuan = 'Minor';
                }elseif($request->$nilai == 4){
                    $temuan = 'Sesuai';
                }else{
                    $temuan = 'Mayor';
                }

                $update = Jawaban::updateOrCreate(['pertanyaan_id' => $p->id,'user_id' => \Auth::user()->id, 'periode_id' => $periode['id']], [
                    'unit_id'           => Auth::user()->unit_id,
                    'jawaban'           => $request->$isi,
                    'uraian'            => $p->isi_pertanyaan,
                    'nilai'             => $request->$nilai,
                    'pertanyaan_id'     => $p->id,
                    'periode_id'        => $periode['id'],
                    'temuan'            => $temuan,
                    'urlfile'           => $request->$url,
                ]);
            }

            #upload dokumen sementara ini tiidak dipakai
            // if(!is_null($request->dokumen_.$p->id)){
            //     $doc    = 'dokumen_'.$p->id;
            //     if ($file = $request->file($doc)) {

            //             // $dataFile = savegdrive($file);
            //             $dataFile = savelocal($file);

            //             Jawaban::updateOrCreate(
            //                 ['pertanyaan_id'      => $p->id, 'user_id'  => \Auth::user()->id, 'periode_id' => $periode['id']], [
            //                 'file_jawaban'        => $dataFile,
            //             ]);
            //         // }
            //     }
            // }
            #upload dokumen sementara ini tiidak dipakai

        }


        // dd($coba);
        return response()->json($update);
    }


    public function publis(Request $request)
    {
        // dd($request->pertanyaan_id);
        $periode       = cekPeriode();

        for($i=0;$i<count($request->pertanyaan_id);$i++){
            $update = Jawaban::updateOrCreate(['pertanyaan_id' => $request->pertanyaan_id[$i],'user_id' => \Auth::user()->id, 'periode_id' => $periode['id']], [
                'publish'           => 1,
            ]);
        }

        if ($update) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
        }

        return response()->json($respon);

    }

    public function nonaktif($id)
    {
        $data_jawaban   = Jawaban::select('standars.id','publish')
                            ->join('pertanyaans','pertanyaans.id', 'jawabans.pertanyaan_id')
                            ->join('standars','standars.id', 'pertanyaans.standar_id')
                            ->join('periodes', 'periodes.id', 'jawabans.periode_id')
                            ->where('periodes.status', 1)
                            ->where('standar_id',$id)
                            ->where('user_id', Auth::user()->id)->get();
        return response()->json($data_jawaban);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = Pertanyaan::find($id);
        $instrumen      = Instrumen::pluck('nama_instrumen','id');
        $unggah         = array(0 => 'Tidak Aktif', 1 => 'Aktif');
        $ambil_lagi     = array(NULL => 'Tidak', 1 => 'Ya');
        $capaian        = Capaian::where('pertanyaan_id', $id)->first() ?? null;

        // $list = $capaian->pilihan;
        // for($i=0;$i<count($list);$i++){
        //         $list[$i]['isi_pilihan'];
        // }
        // dd($list);
        return view('backend.pertanyaan.ubah', compact('data', 'instrumen', 'unggah', 'ambil_lagi', 'capaian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'instrumen_id'               => 'required',
            'isi_pertanyaan'             => 'required',
       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data = Pertanyaan::find($id);

                $update = $data->update($request->all());

                $capaian = Capaian::find($request->capaian_id);
                        $pilihan =  [[
                                        'nilai'             => 1,
                                        'isi_pilihan'       => $request->pilihan1,
                                        ],
                                        [
                                        'nilai'             => 2,
                                        'isi_pilihan'       => $request->pilihan2,
                                        ],
                                        [
                                        'nilai'             => 3,
                                        'isi_pilihan'       => $request->pilihan3,
                                        ],
                                        [
                                        'nilai'             => 4,
                                        'isi_pilihan'       => $request->pilihan4,
                                        ],
                                    ];
                    $capaian->pilihan           = $pilihan;
                    $capaian->update();


            if ($capaian) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $data = Pertanyaan::find($id);
        return view('backend.pertanyaan.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = Pertanyaan::find($id);
        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
