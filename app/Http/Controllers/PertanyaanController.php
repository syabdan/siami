<?php

namespace App\Http\Controllers;

use App\Models\Pertanyaan;

use App\Http\Controllers\Controller;
use App\Models\Standar;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Hash;
use Illuminate\Support\Facades\Http;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pertanyaan.index');
    }

    public function data(Request $request)
    {
        $data = Pertanyaan::orderBy('id', 'DESC')->with('standar')->get();
        return DataTables::of($data)
            ->addColumn('isi_pertanyaan',function($data) {
                return "<div style='border:1px solid #ccc; padding:10px; max-height:100px; max-width:500px; overflow:scroll;'>$data->isi_pertanyaan</div>";
            })
            ->addColumn('catatan',function($data) {
                return "<div style='border:1px solid #ccc; padding:10px; max-height:100px;max-width:500px;overflow:scroll;'>$data->catatan</div>";
            })
            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-outline-warning btn-xs ubah mr-2"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-outline-danger btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })
            ->addColumn('s_aktif',function($data) {
                $data->s_aktif == 1 ? $return = '<span class="label label-sm label-success">Aktif</span>' : $return = '<span class="label label-sm label-danger">Tidak Aktif</span>';
                return $return;
            })
            ->addColumn('s_unggah',function($data) {
                $data->s_unggah == 1 ? $return = '<span class="label label-sm label-success">Aktif</span>' : $return = '<span class="label label-sm label-danger">Tidak Aktif</span>';
                return $return;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','s_aktif', 's_unggah','isi_pertanyaan','catatan'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data           = Pertanyaan::all();
        $status         = array(1 => 'Aktif', 0 => 'Tidak Aktif');
        $jenis          = array('Semua' => 'Semua', 'D3' => 'D3', 'S1' => 'S1', 'S2' => 'S2', 'S3' => 'S3');
        $standar        = Standar::pluck('keterangan','id');
        return view('backend.pertanyaan.tambah', compact('data', 'status', 'jenis','standar'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'standar_id'              => 'required',
            'isi_pertanyaan'          => 'required',
        ],
        [
            'standar_id.required'     => 'Harap mengisi form Standar.',
            'isi_pertanyaan.required' => 'Harap mengisi form pertanyaan.',
       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {

            $pilihan =  [
                [
                'nilai'             => 0,
                'pilihan'       => $request->pilihan0,
                ],[
                'nilai'             => 1,
                'pilihan'       => $request->pilihan1,
                ],
                [
                'nilai'             => 2,
                'pilihan'       => $request->pilihan2,
                ],
                [
                'nilai'             => 3,
                'pilihan'       => $request->pilihan3,
                ],
                [
                'nilai'             => 4,
                'pilihan'       => $request->pilihan4,
                ],
            ];

            $data   = New Pertanyaan;
            $data->standar_id           = $request->standar_id;
            $data->s_unggah             = $request->s_unggah;
            $data->s_aktif              = $request->s_aktif;
            $data->s_unit               = $request->s_unit;
            $data->catatan              = $request->catatan;
            $data->isi_pertanyaan       = $request->isi_pertanyaan;
            $data->pilihan              = $pilihan;
            $data->save();

            if ($data) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
            }
        }

        return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = Pertanyaan::find($id);
        $status         = array(1 => 'Aktif', 0 => 'Tidak Aktif');
        $jenis          = array('Semua' => 'Semua', 'D3' => 'D3', 'S1' => 'S1', 'S2' => 'S2', 'S3' => 'S3');
        $standar        = Standar::pluck('keterangan','id');
        return view('backend.pertanyaan.ubah', compact('data', 'status', 'jenis','standar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'standar_id'              => 'required',
            'isi_pertanyaan'          => 'required',
        ],
        [
            'standar_id.required'     => 'Harap mengisi form Standar.',
            'isi_pertanyaan.required' => 'Harap mengisi form pertanyaan.',
       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data   = Pertanyaan::find($id);
            $pilihan =  [
                [
                'nilai'             => 0,
                'pilihan'       => $request->pilihan0,
                ],[
                'nilai'             => 1,
                'pilihan'       => $request->pilihan1,
                ],
                [
                'nilai'             => 2,
                'pilihan'       => $request->pilihan2,
                ],
                [
                'nilai'             => 3,
                'pilihan'       => $request->pilihan3,
                ],
                [
                'nilai'             => 4,
                'pilihan'       => $request->pilihan4,
                ],
            ];
            $data->standar_id           = $request->standar_id;
            $data->s_unggah             = $request->s_unggah;
            $data->s_aktif              = $request->s_aktif;
            $data->s_unit               = $request->s_unit;
            $data->catatan              = $request->catatan;
            $data->isi_pertanyaan       = $request->isi_pertanyaan;
            $data->pilihan              = $pilihan;
            $data->update();
            // $update = $data->update($request->all());

            if ($data) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $data = Pertanyaan::find($id);
        return view('backend.pertanyaan.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = Pertanyaan::find($id);

        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
