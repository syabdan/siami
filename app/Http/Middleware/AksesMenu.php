<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Route;
use App\Models\Menu;
use App\Models\Permission;

class AksesMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user			= \Auth::user();
        $current	=	explode(".", Route::currentRouteName());

        $submenu	=	Menu::where('link', $current[0])->orderBy('id','asc')->first();

        // dd($submenu);

        if($submenu !== NULL){
                // if($submenu->tampil){
                    $data_sections_arr = explode(",", \Auth::user()->permissionsGroup->data_menus);

                    if(in_array($submenu->id,$data_sections_arr)){
                        return $next($request);
                    }else{
                        return redirect('dashboard');
                    }
                // }else{
                //     return $next($request);
                // }

        }else{
            return redirect('/');
            // return response()->json(array('status' => false, 'pesan' => ['msg' => 'Tidak Ada akses']));
        }
    }
}
