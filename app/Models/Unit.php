<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Unit;
use App\Models\Dosen;

class Unit extends Model
{
    use HasFactory;

    protected $fillable = [
        'id','nama_unit', 'jenjang_pendidikan','parent_id', 'warna'
    ];

    public function dosen()
    {
        return $this->belongsTo(Dosen::class, 'dosen_id');
    }

    public function child()
    {
        return $this->hasMany(Unit::class, 'parent_id')->with('parent');
    }

    public function parent()
    {
        return $this->belongsTo(Unit::class, 'parent_id');
    }

}
