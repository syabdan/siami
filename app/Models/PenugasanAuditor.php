<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Unit;
use App\Models\Periode;
use App\Models\User;

class PenugasanAuditor extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'periode_id','unit_audit_id', 'posisi'
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_audit_id');
    }

    public function periode()
    {
        return $this->belongsTo(Periode::class, 'periode_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
