<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $fillable = [
        'posisi', 'nama','link', 'icon','status','tampil','parent_menu_id','label'
    ];

    public function subMenus()
    {
        return $this->hasMany(static::class, 'parent_menu_id')->orderby('posisi', 'asc');
    }
}
