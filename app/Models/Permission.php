<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Permission extends Model
{
    use HasFactory;

    protected $fillable = [
        'permission_name', 'alias','detail', 'view_status','add_status','edit_status','delete_status','data_menus', 'status'
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'permission_id');
    }
}
