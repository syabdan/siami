<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Periode extends Model
{
    use HasFactory;

    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'nama_periode',
        'tanggal_awal',
        'tanggal_akhir',
        'batas_isi_prodi',
        'status'
    ];
}
