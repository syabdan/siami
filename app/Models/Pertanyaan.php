<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Standar;

class Pertanyaan extends Model
{
    use HasFactory;
    protected $casts = ['pilihan'=> 'array'];

    protected $fillable = [
        'standar_id', 'isi_pertanyaan', 's_unggah', 's_aktif', 's_unit', 'catatan','pilihan'
    ];

    public function standar()
    {
        return $this->belongsTo(Standar::class, 'standar_id');
    }
}
