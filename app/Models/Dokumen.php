<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Periode;
use App\Models\Unit;

class Dokumen extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'unit_id','periode_id', 'file', 'jenis', 'urlfile'
    ];

    public function periode()
    {
        return $this->belongsTo(Periode::class, 'periode_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }
}
