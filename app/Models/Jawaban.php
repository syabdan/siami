<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Pertanyaan;
use App\Models\Dosen;
use App\Models\Unit;
use App\Models\Periode;
use App\Models\Standar;
use App\Models\Dokumen;

class Jawaban extends Model
{
    use HasFactory;

    protected $fillable = [
        'pertanyaan_id',
        'user_id','auditor_id',
        'unit_id',
        'periode_id',
        'jawaban',
        'urlfile',
        'file_jawaban',
        'file_koreksi',
        'urlfile_koreksi',
        'file_konfirmasi',
        'urlfile_konfirmasi',
        'nilai',
        'nilai_auditor',
        'temuan',
        'catatan_penyebab',
        'catatan_koreksi',
        'catatan_pencegahan',
        'tgl_tindakan_koreksi',
        'tgl_ttd_auditor',
        'tgl_verifikasi',
        'aspect',
        'impact',
        'likelihood',
        'severity',
        'mitigation',
        'uraian',
        'publish',
        'catatan_verifikasi'
    ];

    // public static  function boot()
    // {
    //     parent::boot();
    //     static::creating(function ($model){
    //         \Auth::user()->permission_id == 4 ? ($model->user_id = \Auth::user()->id) : 0;
    //         \Auth::user()->permission_id == 3 ? ($model->auditor_id = \Auth::user()->id) : 0;
    //         $model->unit_id = \Auth::user()->unit_id;
    //     });

    //     static::updating(function ($model){
    //         \Auth::user()->permission_id == 4 ? ($model->user_id = \Auth::user()->id) : 0;
    //         \Auth::user()->permission_id == 3 ? ($model->auditor_id = \Auth::user()->id) : 0;
    //     });

    // }

    public function pertanyaan()
    {
        return $this->belongsTo(Pertanyaan::class, 'pertanyaan_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function auditor()
    {
        return $this->belongsTo(User::class, 'auditor_id');
    }

    public function periode()
    {
        return $this->belongsTo(Periode::class, 'periode_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }


    public function cekPublish($id)
    {
        // dd($id);
        $users           = User::where('unit_id', \Auth::user()->unit_id)->orderBy('unit_id')->first() ?? null;

        $periode        = cekPeriode();


        if(!empty($id)){
            $sts = \DB::table('pertanyaans')->where('pertanyaans.standar_id', $id)->whereIn('pertanyaans.s_unit',['Semua', $users->unit->jenjang_pendidikan ?? null]);
                $sts->whereNotIn('pertanyaans.id', function($q) use($id, $periode){
                    $q  ->select('pertanyaan_id')->from('jawabans')
                        ->where('jawabans.user_id', \Auth::user()->id)
                        ->where('jawabans.periode_id', $periode['id'])
                        ->whereNotNull('jawaban');
                    });

                    // dd($sts->get());
            return $sts->count();
        }
    }

    public function chart_ncr($idunit){
        $category   = array();
        $rata_nilai_prodi = array();
        $rata_nilai_auditor = array();

        // check unit
        $adaUnit = Jawaban::where('unit_id', $idunit)->count();
        if($adaUnit > 0){
            foreach ($this->instrumen_item_array($idunit) as $key => $value) {
                $category[]     = $key;
                $rata_nilai_prodi[]   = (int) $value;
            }
            foreach ($this->instrumen_item_array_auditor($idunit) as $key => $value) {
                $rata_nilai_auditor[]   = (int) $value;
            }


            return ['category' => $category, 'rata_nilai_prodi' => $rata_nilai_prodi, 'rata_nilai_auditor' => $rata_nilai_auditor];
        }else{
            return ['category' => [], 'rata_nilai_prodi' => [], 'rata_nilai_auditor' => []];
        }


    }

    private function instrumen_item_array_auditor($idunit)
    {
        $query = Pertanyaan::groupBy('pertanyaans.standar_id')
                ->join('standars','standars.id','pertanyaans.standar_id')
                ->join('jawabans','jawabans.pertanyaan_id','pertanyaans.id')
                ->join('periodes', 'periodes.id', 'jawabans.periode_id')
                ->where('periodes.status', 1)
                ->select(array('pertanyaans.standar_id',\DB::Raw('CONCAT(standars.nama_standar,": ",standars.keterangan) as standar'), \DB::Raw('avg(nilai_auditor) as rata_nilai')))
                ->orderBy('standars.id')
                ->where('jawabans.unit_id', $idunit)
                ->get();

        if($query){
            $data =[];
            foreach($query as $row){
                $data[$row->standar] = $row->rata_nilai;
            }
        }
        // dd($data);
        return $data;
    }

    private function instrumen_item_array($idunit)
    {
        $query = Pertanyaan::groupBy('pertanyaans.standar_id')
                ->join('standars','standars.id','pertanyaans.standar_id')
                ->join('jawabans','jawabans.pertanyaan_id','pertanyaans.id')
                ->join('periodes', 'periodes.id', 'jawabans.periode_id')
                ->where('periodes.status', 1)
                ->select(array('pertanyaans.standar_id',\DB::Raw('CONCAT(standars.nama_standar,": ",standars.keterangan) as standar'), \DB::Raw('avg(nilai) as rata_nilai')))
                ->orderBy('standars.id')
                ->where('jawabans.unit_id', $idunit)
                ->get();
        // dd($query);
        if($query){
            $data =[];
            foreach($query as $row){
                $data[$row->standar] = $row->rata_nilai;
            }
        }
        // dd($data);
        return $data;
    }

    private function abr_count($idunit=NULL, $jenis){
        $query      = Jawaban::select('id','unit_id','assesment','publish')
                    ->where('unit_id', $idunit)
                    ->where('assesment', $jenis)
                    ->where('publish', 1)
                    ->where('periode_id', cekPeriode()->id)
                    ->count();
        return $query;
    }

    public function abr_array($unitId)
    {

        $pertanyaan                 = Pertanyaan::whereIn('s_unit',['Semua', \Auth::user()->unit->jenjang_pendidikan ?? null])->where('s_aktif',1)->count();

        $data=array();
        $data=[
            ['value' => number_format($this->abr_count($unitId ?? null, 'H') / $pertanyaan * 100, 2), 'label' => 'High'],
            ['value' => number_format($this->abr_count($unitId ?? null, 'M') / $pertanyaan * 100, 2), 'label' => 'Medium'],
            ['value' => number_format($this->abr_count($unitId ?? null, 'L') / $pertanyaan * 100, 2), 'label' => 'Low'],
        ];
        // dd($data);
        return json_encode($data);
    }

    public function chart_ncr_per_standar($user_id){
        $category       = array();
        $mayor          = array();
        $minor          = array();
        $sesuai         = array();


            // if(\Auth::user()->permission_id == 3){
            //     $unit = with (new User)->select('penugasan_auditors.unit_audit_id as id')->join('penugasan_auditors','penugasan_auditors.user_id','users.id')->where('users.id', \Auth::user()->id)->first();
            // }
            // if(\Auth::user()->permission_id == 4){
            //     $unit = with (new User)->select('unit_id as id')->where('id', \Auth::user()->id)->first();
            // }

            $unit = with (new User)->select('unit_id as unit_id')->where('users.id', $user_id)->first();

            foreach (with(new Jawaban)->mayor_per_standar_array($unit->unit_id ?? null) as $key => $value) {
                $mayor[]        = (int) $value;
            }

            foreach (with(new Jawaban)->minor_per_standar_array($unit->unit_id ?? null) as $key => $value) {
                $minor[]        = (int) $value;
            }

            foreach (with(new Jawaban)->sesuai_per_standar_array($unit->unit_id ?? null) as $key => $value) {
                $sesuai[]        = (int) $value;
            }

            $data_kategori = Standar::select(array(\DB::Raw('CONCAT(standars.nama_standar,": ",standars.keterangan) as standar')))->get();
            // dd($data_kategori);
            foreach($data_kategori as $row){
                $category[] = $row->standar;

            }


        	return ['category' => $category, 'mayor' => $mayor, 'minor' => $minor, 'sesuai' => $sesuai];

    }

    private function mayor_per_standar_array($idunit)
    {
        $query = Pertanyaan::groupBy('pertanyaans.standar_id')
                ->join('standars','standars.id','pertanyaans.standar_id')
                ->join('jawabans','jawabans.pertanyaan_id','pertanyaans.id')
                ->join('periodes', 'periodes.id', 'jawabans.periode_id')
                ->where('periodes.status', 1)
                ->select(array(\DB::Raw('CONCAT(standars.nama_standar,": ",standars.keterangan) as standar'), \DB::Raw('count(temuan) as rata'), 'standars.id as id', 'jawabans.temuan as temuan'))
                ->orderBy('standars.id')
                ->where('jawabans.unit_id', $idunit)
                ->where('jawabans.temuan', 'Mayor')
                ->get();
        // dd($query);

            $standars = Standar::select('id')->get();
            $data =[];
            $no     = 1;
            foreach ($standars as $standar) {
                foreach($query as $row){
                    if($standar->id == $row->id){
                        $data[$row->standar] = $row->rata;
                        $no = $standar->id;
                        $no++;

                        break;
                    }
                }
                if($no == $standar->id){
                    $data[$no] = 0;
                    $no++;
                }

            }

        return $data;
    }

    private function minor_per_standar_array($idunit)
    {
        $query = Pertanyaan::groupBy('pertanyaans.standar_id')
                ->join('standars','standars.id','pertanyaans.standar_id')
                ->join('jawabans','jawabans.pertanyaan_id','pertanyaans.id')
                ->join('periodes', 'periodes.id', 'jawabans.periode_id')
                ->where('periodes.status', 1)
                ->select(array(\DB::Raw('CONCAT(standars.nama_standar,": ",standars.keterangan) as standar'), \DB::Raw('count(temuan) as rata'), 'standars.id as id'))
                ->orderBy('standars.id')
                ->where('jawabans.unit_id', $idunit)
                ->where('jawabans.temuan', 'Minor')
                ->get();
        // dd($query);

        $standars = Standar::select('id')->get();
        $data   = [];
        $no     = 1;
        foreach ($standars as $standar) {
            foreach($query as $row){
                if($standar->id == $row->id){
                    $data[$row->standar] = $row->rata;
                    $no = $standar->id;
                    $no++;

                    break;
                }
            }
            if($no == $standar->id){
                $data[$no] = 0;
                $no++;
            }

        }
        // dd($data);
        return $data;
    }

    private function sesuai_per_standar_array($idunit)
    {
        $query = Pertanyaan::groupBy('pertanyaans.standar_id')
                ->join('standars','standars.id','pertanyaans.standar_id')
                ->join('jawabans','jawabans.pertanyaan_id','pertanyaans.id')
                ->join('periodes', 'periodes.id', 'jawabans.periode_id')
                ->where('periodes.status', 1)
                ->select(array(\DB::Raw('CONCAT(standars.nama_standar,": ",standars.keterangan) as standar'), \DB::Raw('count(temuan) as rata'), 'standars.id as id'))
                ->orderBy('standars.id')
                ->where('jawabans.unit_id', $idunit)
                ->where('jawabans.temuan', 'Sesuai')
                ->get();
        // dd($query);
        $standars = Standar::select('id')->get();
        $data   = [];
        $no     = 1;
        foreach ($standars as $standar) {
            foreach($query as $row){
                if($standar->id == $row->id){
                    $data[$row->standar] = $row->rata;
                    $no = $standar->id;
                    $no++;

                    break;
                }
            }
            if($no == $standar->id){
                $data[$no] = 0;
                $no++;
            }

        }
        // dd($data);
        return $data;
    }



    public function chart_ncr_per_prodi(){
        $category       = array();
        $mayor          = array();
        $minor          = array();
        $sesuai         = array();

        $lihatjawaban   = Jawaban::all()->count();
        // dd($lihatjawaban);
        if($lihatjawaban > 0){
            foreach (with(new Jawaban)->ncr_per_prodi_array('Mayor') as $key => $value) {
                $category[]     = $key;
                $mayor[]        = (int) $value;
            }

            foreach (with(new Jawaban)->ncr_per_prodi_array('Minor') as $key => $value) {
                $minor[]        = (int) $value;
            }

            foreach (with(new Jawaban)->ncr_per_prodi_array('Sesuai') as $key => $value) {
                $sesuai[]        = (int) $value;
            }

        	return ['category' => $category, 'mayor' => $mayor, 'minor' => $minor, 'sesuai' => $sesuai];

        }else{
            return ['category' => [], 'mayor' => [], 'minor' => [], 'sesuai' => []];
        }



    }

    private function ncr_per_prodi_array($temuan){
        $query = Jawaban::groupBy('jawabans.unit_id')
                ->join('pertanyaans','pertanyaans.id','jawabans.pertanyaan_id')
                ->join('units','units.id','jawabans.unit_id')
                ->join('periodes', 'periodes.id', 'jawabans.periode_id')
                ->where('periodes.status', 1)
                ->select(array(\DB::Raw('CONCAT(units.nama_unit," - ",units.jenjang_pendidikan) as unit'), \DB::Raw('count(temuan) as rata')))
                ->orderBy('units.id')
                ->where('jawabans.temuan', $temuan)
                ->get();
        // dd($query);
        if($query){
            $data =[];
            foreach($query as $row){
                $data[$row->unit] = $row->rata;
            }
        }

        return $data;
    }

    public function cek_progres_unggah($unit_id, $periode){
        $data           = Jawaban::join('pertanyaans','pertanyaans.id','jawabans.pertanyaan_id')
                            ->whereNotNull('file_jawaban')
                            ->join('periodes', 'periodes.id', 'jawabans.periode_id')->where('periode_id', $periode)
                            ->where('jawabans.unit_id', $unit_id)->get()->unique('pertanyaan_id');

        return $data->count();
    }

    public function cek_progres_pengisian($unit_id, $periode){
        $data           = Jawaban::join('pertanyaans','pertanyaans.id','jawabans.pertanyaan_id')
                                    ->where('periode_id', $periode)
                                    ->where('jawabans.unit_id', $unit_id)
                                    ->where('publish',1)
                                    ->get()
                                    ->unique('pertanyaan_id');

        return $data->count();
    }

}
