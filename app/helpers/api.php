<?php

function api_dosen(){
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', 'http://datajson.webuir.com/SiPresma/dosen.php', [
        'form_params' => [
            'user' => 'root',
            'key' => 'QWerui7834JHHoP9',
        ]
    ]);
    // echo $res->getStatusCode();
    return $res->getBody();

}

    function api_prodi(){
        $client = new GuzzleHttp\Client();
        $res = $client->request('POST', 'http://datajson.webuir.com/MahasiswaAsing/prodi.php', [
            'form_params' => [
                'user' => 'root',
                'pass' => 'QWerui7834JHHoP9',
            ]
        ]);
        // echo $res->getStatusCode();
        return $res->getBody();
    }

    function api_fakultas(){
        $client = new GuzzleHttp\Client();
        $res = $client->request('POST', 'http://datajson.webuir.com/MahasiswaAsing/fakultas.php', [
            'form_params' => [
                'user' => 'root',
                'pass' => 'QWerui7834JHHoP9',
            ]
        ]);
        // echo $res->getStatusCode();
        return $res->getBody();
    }

function api_mahasiswa_by_npm($npm){
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', 'http://datajson.webuir.com/SiPresma/mhsnim.php', [
        'form_params' => [
            'user' => 'root',
            'key' => 'QWerui7834JHHoP9',
            'npm' => $npm
        ]
    ]);
    // echo $res->getStatusCode();
    return $res->getBody();
}

function api_login($npm,$pass){
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', 'http://datajson.webuir.com/SiPresma/login.php', [
        'form_params' => [
            'user' => 'root',
            'key' => 'QWerui7834JHHoP9',
            'npm' => $npm,
            'pass' => $pass
        ]
    ]);
    // echo $res->getStatusCode();
    return $res->getBody();

}

function api_mahasiswa_limit($limitawal,$limitakhir){
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', 'http://datajson.webuir.com/SiPresma/mhs.php', [
        'form_params' => [
            'user' => 'root',
            'key' => 'QWerui7834JHHoP9',
            'limitawal' => $limitawal,
            'limitakhir' => $limitakhir
        ]
    ]);
    // echo $res->getStatusCode();
    return $res->getBody();

}

function api_mahasiswa_all($limitawal,$limitakhir){
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', 'http://datajson.webuir.com/SiPresma/mhsnew.php', [
        'form_params' => [
            'user' => 'root',
            'key' => 'QWerui7834JHHoP9',
            'limitawal' => $limitawal,
            'limitakhir' => $limitakhir
        ]
    ]);
    // echo $res->getStatusCode();
    return $res->getBody();
}

// API S2
function api_mahasiswa_by_npm_s2($npm){
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', 'http://datajson.webuir.com/SiPresmaS2/mhsnim.php', [
        'form_params' => [
            'user' => 'root',
            'key' => 'QWerui7834JHHoP9',
            'npm' => $npm
        ]
    ]);
    // echo $res->getStatusCode();
    return $res->getBody();
}

function api_login_s2($npm,$pass){
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', 'http://datajson.webuir.com/SiPresmaS2/login.php', [
        'form_params' => [
            'user' => 'root',
            'key' => 'QWerui7834JHHoP9',
            'npm' => $npm,
            'pass' => $pass
        ]
    ]);
    // echo $res->getStatusCode();
    return $res->getBody();
}

function api_mahasiswa_limit_s2($limitawal,$limitakhir){
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', 'http://datajson.webuir.com/SiPresmaS2/mhs.php', [
        'form_params' => [
            'user' => 'root',
            'key' => 'QWerui7834JHHoP9',
            'limitawal' => $limitawal,
            'limitakhir' => $limitakhir
        ]
    ]);
    // echo $res->getStatusCode();
    return $res->getBody();

}

function api_mahasiswa_all_s2($limitawal,$limitakhir){
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', 'http://datajson.webuir.com/SiPresmaS2/mhsnew.php', [
        'form_params' => [
            'user' => 'root',
            'key' => 'QWerui7834JHHoP9',
            'limitawal' => $limitawal,
            'limitakhir' => $limitakhir
        ]
    ]);
    // echo $res->getStatusCode();
    return $res->getBody();

}
// #enf API S2

function api_total_mahasiswa_aktif_lokal(){
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', 'http://datajson.webuir.com/Total/mhsaktifprodi.php', [
        'form_params' => [
            'user' => 'root',
            'pass' => 'QWerui7834JHHoP9',
        ]
    ]);
    // echo $res->getStatusCode();
    return $res->getBody();
}

function api_total_mahasiswa_aktif_asing(){
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', 'http://datajson.webuir.com/Total/mhsaktifprodi_asing.php', [
        'form_params' => [
            'user' => 'root',
            'pass' => 'QWerui7834JHHoP9',
        ]
    ]);
    // echo $res->getStatusCode();
    return $res->getBody();
}

?>
