<?php
use App\Models\Unit;
use App\Models\Periode;
use App\Models\Dokumen;
use Illuminate\Support\Facades\Storage;

function auditee(){
    $auditee = Unit::select("id", \DB::raw("CONCAT(units.nama_unit,' - ',units.jenjang_pendidikan) as nama_prodi"))->whereNotNull('parent_id')->where('id', \Auth::user()->unit_id)->first();
    return $auditee->nama_prodi ?? null;
}

function periode(){
    $periode = Periode::select("id","nama_periode")->where('id', \Auth::user()->periode_id)->first();
    return $periode->nama_periode ?? null;
}

function cekPeriodeProdi(){
    $periode        = Periode::where('tanggal_awal', '<=', date('Y-m-d'))
                            ->where('batas_isi_prodi','>=', date('Y-m-d'))
                            ->where('status',1)
                            ->count();


    return $periode;
}

function cekPeriode(){
    $periode = Periode::orderBy('id','desc')->where('status', 1)->first();

    return $periode;
}

function cekDokumen(){
    $data = Dokumen::where('periode_id', cekPeriode()->id)->where('unit_id',\Auth::user()->unit_id)->first();

    return $data;
}

function tanggal_indonesia($tgl, $tampil_hari=true){
	$nama_hari=array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
	$nama_bulan=array(1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun=substr($tgl,0,4);
	$bulan=$nama_bulan[(int)substr($tgl,5,2)];
	$tanggal=substr($tgl,8,2);

	$text="";

	if($tampil_hari){
		$urutan_hari=date('w', mktime(0,0,0, substr($tgl,5,2), $tanggal, $tahun));

		$hari=$nama_hari[$urutan_hari];
		$text .= $hari.", ";
	}

	$text .=$tanggal." ".$bulan." ".$tahun;

	return $text;
}

// function getBulan($tgl){
// 	$nama_bulan=array(1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

// 	$bulan=$nama_bulan[(int)substr($tgl,5,2)];

// 	return $bulan;
// }

function tanggal_indonesia2($tgl, $tampil_hari=true){
	$nama_hari=array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
	$nama_bulan=array(1=>"Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des");

	$tahun=substr($tgl,0,4);
	$bulan=$nama_bulan[(int)substr($tgl,5,2)];
	$tanggal=substr($tgl,8,2);

	$text="";

	if($tampil_hari){
		$urutan_hari=date('w', mktime(0,0,0, substr($tgl,5,2), $tanggal, $tahun));

		$hari=$nama_hari[$urutan_hari];
		$text .= $hari.", ";
	}

	$text .=$tanggal." ".$bulan." ".$tahun;

	return $text;
}

function bulan($tgl){

	$nama_bulan=array(1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    $bulan=$nama_bulan[(int)substr($tgl,5,2)];

    return $bulan;
}
function getBulanRomawi ($bln){
	switch($bln){
		case 1:
			return "I";
		break;
		case 2:
			return "II";
		break;
		case 3:
			return "III";
		break;
		case 4:
			return "IV";
		break;
		case 5:
			return "V";
		break;
		case 6:
			return "VI";
		break;
		case 7:
			return "VII";
		break;
		case 8:
			return "VIII";
		break;
		case 9:
			return "IX";
		break;
		case 10:
			return "X";
		break;
		case 11:
			return "XI";
		break;
		case 12:
			return "XII";
		break;
	}
}

function hariIni(){
    $hari = date ("D");

	switch($hari){
		case 'Sun':
			$hari_ini = "Minggu";
		break;

		case 'Mon':
			$hari_ini = "Senin";
		break;

		case 'Tue':
			$hari_ini = "Selasa";
		break;

		case 'Wed':
			$hari_ini = "Rabu";
		break;

		case 'Thu':
			$hari_ini = "Kamis";
		break;

		case 'Fri':
			$hari_ini = "Jumat";
		break;

		case 'Sat':
			$hari_ini = "Sabtu";
		break;

		default:
			$hari_ini = "-";
		break;
	}

	return $hari_ini;
}

function terbilang ($nilai){
	$nilai = abs($nilai);
	$huruf = array("", " satu", " dua", " tiga", " empat", " lima", " enam", " tujuh", " delapan", " sembilan", " sepuluh", " sebelas");
	$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = terbilang($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = terbilang($nilai/10)." puluh". terbilang($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . terbilang($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = terbilang($nilai/100) . " ratus" . terbilang($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . terbilang($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = terbilang($nilai/1000) . " ribu" . terbilang($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = terbilang($nilai/1000000) . " juta" . terbilang($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = terbilang($nilai/1000000000) . " milyar" . terbilang(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = terbilang($nilai/1000000000000) . " trilyun" . terbilang(fmod($nilai,1000000000000));
		}
		return trim($temp);
}

function siswa_waktu ($tgl_awal, $tgl_akhir, $status_eng=NULL){
	$diff  = date_diff( date_create($tgl_akhir), date_create($tgl_awal) );

	$data =  $diff->format('%Y tahun %M bulan %D hari');
	$hasil = array();

	$hasil = ['tahun' => $diff->format('%y'), 'bulan' => $diff->format('%m'), 'hari' => $diff->format('%d')];
	// dd($hasil);
	return $hasil;
}

function sisa_hari ($tglBerakhir){
	// mengatur time zone untuk WIB.
	date_default_timezone_set("Asia/Jakarta");

	$tahun	=	substr($tglBerakhir,0,4);
	$bulan	=	substr($tglBerakhir,5,2);
	$hari	=	substr($tglBerakhir,8,2);

	// mencari mktime untuk tanggal 1 Januari 2011 00:00:00 WIB
	$selisih1 =  mktime(0, 0, 0, $bulan, $hari, $tahun);

	// mencari mktime untuk current time
	$selisih2 = mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"));

	// mencari selisih detik antara kedua waktu
	$delta = $selisih1 - $selisih2;

	// proses mencari jumlah hari
	$a = floor($delta / 86400);

	// proses mencari jumlah jam
	$sisa = $delta % 86400;
	$b  = floor($sisa / 3600);

	// proses mencari jumlah menit
	$sisa = $sisa % 3600;
	$c = floor($sisa / 60);

	// proses mencari jumlah detik
	$sisa = $sisa % 60;
	$d = floor($sisa / 1);

	// echo "Waktu saat ini: ".date("d-m-Y H:i:s")."<br>";
	return $a;
}

function setTanggal(){
	for($i=1; $i<=31; $i++){
		$nilai = strlen($i) == 1 ? '0' . $i:$i;
		$tgl[$nilai] = $i;
	}
	 return $tgl;
}

function setBulan(){
	return array("01"=>"Januari","02"=>"Februari","03"=>"Maret","04"=>"April","05"=>"Mei","06"=>"Juni","07"=>"July","08"=>"Agustus","09"=>"September","10"=>"Oktober","11"=>"November","12"=>"Desember");
}

function setTahun($start=1980, $end = NULL){
	$to =$end == NULL ? date("Y"):$end;
	for ($a=$to;$a>=$start;$a--)
	{
		$tahun[$a] =$a;
	}
	return $tahun;
}

function comboTanggal($v="", $selected){
$selected = !empty($selected) ? $selected : date("d");
$cTgl = "<select name=tanggal".$v.">";
$cTgl .= "<option value='".$selected."' selected='".$selected."' >".$selected."</option>";
	for($tgl=1; $tgl<=31; $tgl++){
	$tgl_leng=strlen($tgl);
		if ($tgl_leng==1) $i="0".$tgl;
		else $i=$tgl;
	$cTgl .= "<option value=".$i.">".$i."</option>";
	}
	$cTgl .="</select>";
	return $cTgl;
}

function comboBulan($v="", $selected){
	$selected = !empty($selected) ? $selected : date("m");
	$bln=array(1=>"Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
	$cBulan = "<select name=bulan".$v.">";
	$cBulan .= "<option value='".$selected."' selected='".$selected."' >".$this->getBulan($selected)."</option>";
		for($bulan=1; $bulan<=12; $bulan++){
		$bln_leng = strlen($bulan);
			if($bln_leng==1) $a="0".$bulan;
			else $a = $bulan;
			$cBulan.="<option value=".$a.">".$bln[$bulan]."</option>";
		}
	$cBulan.= "</select>";
return $cBulan;
}

function comboTahun($v="",$selected){
$now=date("Y");
$selected = !empty($selected) ? $selected : date("Y");
$cTahun ="<select name=tahun".$v.">";
$cTahun .= "<option value='".$selected."' selected='".$selected."' >".$selected."</option>";
	for($thn=2000; $thn<=$now; $thn++){
		$cTahun.= "<option value=".$thn.">".$thn."</option>";
	}
$cTahun.= "</select>";
return $cTahun;
}

function autolink ($str){
	$str = preg_replace("/([[:space:]])((f|ht)tps?:\/\/[a-z0-9~#%@\&:=?+\/\.,_-]+[a-z0-9~#%@\&=?+\/_.;-]+)/", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $str); //http
	$str = preg_replace("/([[:space:]])(www\.[a-z0-9~#%@\&:=?+\/\.,_-]+[a-z0-9~#%@\&=?+\/_.;-]+)/", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $str); // www.
	$str = preg_replace("/([[:space:]])([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})/","\\1<a href=\"mailto:\\2\">\\2</a>", $str); // mail
	$str = preg_replace("/^((f|ht)tp:\/\/[a-z0-9~#%@\&:=?+\/\.,_-]+[a-z0-9~#%@\&=?+\/_.;-]+)/", "<a href=\"\\1\" target=\"_blank\">\\1</a>", $str); //http
	$str = preg_replace("/^(www\.[a-z0-9~#%@\&:=?+\/\.,_-]+[a-z0-9~#%@\&=?+\/_.;-]+)/", "<a href=\"http://\\1\" target=\"_blank\">\\1</a>", $str); // www.
	$str = preg_replace("/^([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})/","<a href=\"mailto:\\1\">\\1</a>", $str); // mail
	return $str;
}

function potong($teks,$ambil = 200){
	$isi = substr($teks,0, $ambil);
	if(strlen($teks)>$ambil){
	$isi = substr($teks,0,strrpos($isi," "));
	}
	return $isi = strip_tags($isi);
}

function specialChar($del){
	$ereg	= "/[{}~`!@#$%^&?*()+=\/'|,]/";
	//$del	= stripslashes(str_replace(' ','-',$del));
	$del	= preg_replace($ereg,' ',$del);
	$del	= trim(preg_replace("/[[:blank:]]+/",'-',$del));
	//$del	= str_replace('?','',$del);

	return strtolower($del);
}

function permalink($link){
	$p_link = substr($link,0,90);
	if(strlen($p_link) > 50){
		$p_link = substr($p_link,0,strrpos($p_link," "));
	}
	$p_link	= preg_replace("/[^A-Za-z0-9 ^-]/", '', $p_link);;
	$p_link	= str_replace('.','-',$this->specialChar($p_link));
	return trim($p_link,'-');
}


function curPageURL() {
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
	$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
	$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function getIp() {
	//Just get the headers if we can or else use the SERVER global
	if ( function_exists( 'apache_request_headers' ) ) {
		$headers = apache_request_headers();
	} else {
		$headers = $_SERVER;
	}
	//Get the forwarded IP if it exists
	if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
		$the_ip = $headers['X-Forwarded-For'];
	} elseif ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 )
	) {
		$the_ip = $headers['HTTP_X_FORWARDED_FOR'];
	} else {

		$the_ip = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
	}
	return $the_ip;
}

function fileUpload($root){
	$path = $root.'/app/files';
	$results = scandir($path);
	$string = array();
	foreach ($results as $result){
		if ($result === '.' or $result === '..') continue;
		$string[] = $result;
	}
	return $string;
}

function hapusFile($path, $namaFile){
	$path = $path .'/';
	if(!empty($namaFile)){
		if(file_exists($path . $namaFile)) unlink($path . $namaFile);
		$tipe = array("kecil", "thumb", "empatEnam", "sedang", "wide", "slide", "banner");
		foreach ($tipe as $gbr){
			if(file_exists($path . $gbr .'-'. $namaFile)) unlink($path . $gbr .'-'. $namaFile);
		}
	}
}

function hapusFiletunggal($path, $namaFile){
	$path = $path .'/';
	if(!empty($namaFile)){
		if(file_exists($path . $namaFile)) unlink($path . $namaFile);
	}
}

function downloadgdrive(String $filename){

    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));

    $file = $contents
        ->where('type', '=', 'file')
        ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
        ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
        ->first(); // there can be duplicate file names!

    //return $file; // array with file info
    // dd($file['path']);
    $rawData = Storage::cloud()->get($file['path']);


    return response($rawData, 200)
        ->header('ContentType', $file['mimetype'])
        ->header('Content-Disposition', "attachment; filename=$filename");

}

function viewgdrive(String $filename){

    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));

    $file = $contents->where('type', '=', 'file')->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))->first(); // there can be duplicate file names!

    //return $file; // array with file info

    $rawData = Storage::cloud()->url($file['path']);

    return redirect('https://docs.google.com/viewer?url='.$rawData);

}

function deletegdrive(String $filename){

    // First we need to create a file to delete
    // Storage::cloud()->makeDirectory('Test Dir');

    // Now find that file and use its ID (path) to delete it
    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::cloud()->listContents($dir, $recursive));

    $file = $contents
        ->where('type', '=', 'file')
        ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
        ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
        ->first(); // there can be duplicate file names!

    $del= Storage::cloud()->delete($file['path']);

    return  $del;

}

function savegdrive($file){

    $fullName=$file->getClientOriginalName();

    $name=explode('.',$fullName)[0];
    $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
    $replaceImage=strtolower($originalImage);

    $gambarName = time().'_'.$replaceImage.'.'.$file->getClientOriginalExtension();

    $filePath = $file->getPathName();
    $path=Storage::cloud()->put($gambarName, fopen($filePath, 'r+'));

    return $gambarName;
}

function savelocal($file){
    $fullName=$file->getClientOriginalName();

    $name=explode('.',$fullName)[0];
    $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
    $replaceImage=strtolower($originalImage);

    $gambarName = time().'_'.$replaceImage.'.'.$file->getClientOriginalExtension();

    $filePath = $file->getPathName();
    $destinationPath = storage_path().'/dokumen/SIAMI/';
    $file->move($destinationPath, $gambarName);

    return $gambarName;
}

function viewlocal($file){
    return response()->file(storage_path('/dokumen/SIAMI/'.$file));
}
?>
